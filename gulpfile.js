var gulp = require('gulp');
var sass = require('gulp-sass');
var browser = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var merge = require('event-stream').merge;

gulp.task('sass', function () {
    gulp.src('./assets/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./assets/css/'))
        .pipe(browser.stream());

        var files = [
            { src: './node_modules/bxslider/dist/jquery.bxslider.css', dist: './assets/css/' },
        ];

        for (const key in files) {
            gulp.src(files[key].src)
            .pipe(gulp.dest(files[key].dist))
        }
});

gulp.task('js', function () {
    var files = [
        { src: './node_modules/jquery/dist/jquery.js', dist: './assets/js/' },
        { src: './node_modules/popper.js/dist/umd/popper.js', dist: './assets/js/' },
        { src: './node_modules/bootstrap/dist/js/bootstrap.js', dist: './assets/js/' },
        { src: './node_modules/bxslider/dist/jquery.bxslider.js', dist: './assets/js/' }
    ];

    for (const key in files) {
        gulp.src(files[key].src)
            .pipe(sourcemaps.init())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(files[key].dist));
    }
});

gulp.task('serve', function () {
    browser.init({
        server: './'
    });

    gulp.watch('./assets/scss/**/*.scss', ['sass']);
    gulp.watch('./assets/scss/**/*.scss', ['js']);

    gulp.watch('./*.html').on('change', browser.reload);
    gulp.watch('./assets/css/*.css').on('change', browser.reload);
    gulp.watch('./assets/js/*.js').on('change', browser.reload);
});

gulp.task('default', ['serve']);