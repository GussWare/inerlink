/**
 * bxSlider v4.2.12
 * Copyright 2013-2015 Steven Wanderski
 * Written while drinking Belgian ales and listening to jazz
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */

;(function($) {

  var defaults = {

    // GENERAL
    mode: 'horizontal',
    slideSelector: '',
    infiniteLoop: true,
    hideControlOnEnd: false,
    speed: 500,
    easing: null,
    slideMargin: 0,
    startSlide: 0,
    randomStart: false,
    captions: false,
    ticker: false,
    tickerHover: false,
    adaptiveHeight: false,
    adaptiveHeightSpeed: 500,
    video: false,
    useCSS: true,
    preloadImages: 'visible',
    responsive: true,
    slideZIndex: 50,
    wrapperClass: 'bx-wrapper',

    // TOUCH
    touchEnabled: true,
    swipeThreshold: 50,
    oneToOneTouch: true,
    preventDefaultSwipeX: true,
    preventDefaultSwipeY: false,

    // ACCESSIBILITY
    ariaLive: true,
    ariaHidden: true,

    // KEYBOARD
    keyboardEnabled: false,

    // PAGER
    pager: true,
    pagerType: 'full',
    pagerShortSeparator: ' / ',
    pagerSelector: null,
    buildPager: null,
    pagerCustom: null,

    // CONTROLS
    controls: true,
    nextText: 'Next',
    prevText: 'Prev',
    nextSelector: null,
    prevSelector: null,
    autoControls: false,
    startText: 'Start',
    stopText: 'Stop',
    autoControlsCombine: false,
    autoControlsSelector: null,

    // AUTO
    auto: false,
    pause: 4000,
    autoStart: true,
    autoDirection: 'next',
    stopAutoOnClick: false,
    autoHover: false,
    autoDelay: 0,
    autoSlideForOnePage: false,

    // CAROUSEL
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 0,
    slideWidth: 0,
    shrinkItems: false,

    // CALLBACKS
    onSliderLoad: function() { return true; },
    onSlideBefore: function() { return true; },
    onSlideAfter: function() { return true; },
    onSlideNext: function() { return true; },
    onSlidePrev: function() { return true; },
    onSliderResize: function() { return true; },
	onAutoChange: function() { return true; } //calls when auto slides starts and stops
  };

  $.fn.bxSlider = function(options) {

    if (this.length === 0) {
      return this;
    }

    // support multiple elements
    if (this.length > 1) {
      this.each(function() {
        $(this).bxSlider(options);
      });
      return this;
    }

    // create a namespace to be used throughout the plugin
    var slider = {},
    // set a reference to our slider element
    el = this,
    // get the original window dimens (thanks a lot IE)
    windowWidth = $(window).width(),
    windowHeight = $(window).height();

    // Return if slider is already initialized
    if ($(el).data('bxSlider')) { return; }

    /**
     * ===================================================================================
     * = PRIVATE FUNCTIONS
     * ===================================================================================
     */

    /**
     * Initializes namespace settings to be used throughout plugin
     */
    var init = function() {
      // Return if slider is already initialized
      if ($(el).data('bxSlider')) { return; }
      // merge user-supplied options with the defaults
      slider.settings = $.extend({}, defaults, options);
      // parse slideWidth setting
      slider.settings.slideWidth = parseInt(slider.settings.slideWidth);
      // store the original children
      slider.children = el.children(slider.settings.slideSelector);
      // check if actual number of slides is less than minSlides / maxSlides
      if (slider.children.length < slider.settings.minSlides) { slider.settings.minSlides = slider.children.length; }
      if (slider.children.length < slider.settings.maxSlides) { slider.settings.maxSlides = slider.children.length; }
      // if random start, set the startSlide setting to random number
      if (slider.settings.randomStart) { slider.settings.startSlide = Math.floor(Math.random() * slider.children.length); }
      // store active slide information
      slider.active = { index: slider.settings.startSlide };
      // store if the slider is in carousel mode (displaying / moving multiple slides)
      slider.carousel = slider.settings.minSlides > 1 || slider.settings.maxSlides > 1 ? true : false;
      // if carousel, force preloadImages = 'all'
      if (slider.carousel) { slider.settings.preloadImages = 'all'; }
      // calculate the min / max width thresholds based on min / max number of slides
      // used to setup and update carousel slides dimensions
      slider.minThreshold = (slider.settings.minSlides * slider.settings.slideWidth) + ((slider.settings.minSlides - 1) * slider.settings.slideMargin);
      slider.maxThreshold = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
      // store the current state of the slider (if currently animating, working is true)
      slider.working = false;
      // initialize the controls object
      slider.controls = {};
      // initialize an auto interval
      slider.interval = null;
      // determine which property to use for transitions
      slider.animProp = slider.settings.mode === 'vertical' ? 'top' : 'left';
      // determine if hardware acceleration can be used
      slider.usingCSS = slider.settings.useCSS && slider.settings.mode !== 'fade' && (function() {
        // create our test div element
        var div = document.createElement('div'),
        // css transition properties
        props = ['WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
        // test for each property
        for (var i = 0; i < props.length; i++) {
          if (div.style[props[i]] !== undefined) {
            slider.cssPrefix = props[i].replace('Perspective', '').toLowerCase();
            slider.animProp = '-' + slider.cssPrefix + '-transform';
            return true;
          }
        }
        return false;
      }());
      // if vertical mode always make maxSlides and minSlides equal
      if (slider.settings.mode === 'vertical') { slider.settings.maxSlides = slider.settings.minSlides; }
      // save original style data
      el.data('origStyle', el.attr('style'));
      el.children(slider.settings.slideSelector).each(function() {
        $(this).data('origStyle', $(this).attr('style'));
      });

      // perform all DOM / CSS modifications
      setup();
    };

    /**
     * Performs all DOM and CSS modifications
     */
    var setup = function() {
      var preloadSelector = slider.children.eq(slider.settings.startSlide); // set the default preload selector (visible)

      // wrap el in a wrapper
      el.wrap('<div class="' + slider.settings.wrapperClass + '"><div class="bx-viewport"></div></div>');
      // store a namespace reference to .bx-viewport
      slider.viewport = el.parent();

      // add aria-live if the setting is enabled and ticker mode is disabled
      if (slider.settings.ariaLive && !slider.settings.ticker) {
        slider.viewport.attr('aria-live', 'polite');
      }
      // add a loading div to display while images are loading
      slider.loader = $('<div class="bx-loading" />');
      slider.viewport.prepend(slider.loader);
      // set el to a massive width, to hold any needed slides
      // also strip any margin and padding from el
      el.css({
        width: slider.settings.mode === 'horizontal' ? (slider.children.length * 1000 + 215) + '%' : 'auto',
        position: 'relative'
      });
      // if using CSS, add the easing property
      if (slider.usingCSS && slider.settings.easing) {
        el.css('-' + slider.cssPrefix + '-transition-timing-function', slider.settings.easing);
      // if not using CSS and no easing value was supplied, use the default JS animation easing (swing)
      } else if (!slider.settings.easing) {
        slider.settings.easing = 'swing';
      }
      // make modifications to the viewport (.bx-viewport)
      slider.viewport.css({
        width: '100%',
        overflow: 'hidden',
        position: 'relative'
      });
      slider.viewport.parent().css({
        maxWidth: getViewportMaxWidth()
      });
      // apply css to all slider children
      slider.children.css({
        // the float attribute is a reserved word in compressors like YUI compressor and need to be quoted #48
        'float': slider.settings.mode === 'horizontal' ? 'left' : 'none',
        listStyle: 'none',
        position: 'relative'
      });
      // apply the calculated width after the float is applied to prevent scrollbar interference
      slider.children.css('width', getSlideWidth());
      // if slideMargin is supplied, add the css
      if (slider.settings.mode === 'horizontal' && slider.settings.slideMargin > 0) { slider.children.css('marginRight', slider.settings.slideMargin); }
      if (slider.settings.mode === 'vertical' && slider.settings.slideMargin > 0) { slider.children.css('marginBottom', slider.settings.slideMargin); }
      // if "fade" mode, add positioning and z-index CSS
      if (slider.settings.mode === 'fade') {
        slider.children.css({
          position: 'absolute',
          zIndex: 0,
          display: 'none'
        });
        // prepare the z-index on the showing element
        slider.children.eq(slider.settings.startSlide).css({zIndex: slider.settings.slideZIndex, display: 'block'});
      }
      // create an element to contain all slider controls (pager, start / stop, etc)
      slider.controls.el = $('<div class="bx-controls" />');
      // if captions are requested, add them
      if (slider.settings.captions) { appendCaptions(); }
      // check if startSlide is last slide
      slider.active.last = slider.settings.startSlide === getPagerQty() - 1;
      // if video is true, set up the fitVids plugin
      if (slider.settings.video) { el.fitVids(); }
      if (slider.settings.preloadImages === 'all' || slider.settings.ticker) { preloadSelector = slider.children; }
      // only check for control addition if not in "ticker" mode
      if (!slider.settings.ticker) {
        // if controls are requested, add them
        if (slider.settings.controls) { appendControls(); }
        // if auto is true, and auto controls are requested, add them
        if (slider.settings.auto && slider.settings.autoControls) { appendControlsAuto(); }
        // if pager is requested, add it
        if (slider.settings.pager) { appendPager(); }
        // if any control option is requested, add the controls wrapper
        if (slider.settings.controls || slider.settings.autoControls || slider.settings.pager) { slider.viewport.after(slider.controls.el); }
      // if ticker mode, do not allow a pager
      } else {
        slider.settings.pager = false;
      }
      loadElements(preloadSelector, start);
    };

    var loadElements = function(selector, callback) {
      var total = selector.find('img:not([src=""]), iframe').length,
      count = 0;
      if (total === 0) {
        callback();
        return;
      }
      selector.find('img:not([src=""]), iframe').each(function() {
        $(this).one('load error', function() {
          if (++count === total) { callback(); }
        }).each(function() {
          if (this.complete || this.src == '') { $(this).trigger('load'); }
        });
      });
    };

    /**
     * Start the slider
     */
    var start = function() {
      // if infinite loop, prepare additional slides
      if (slider.settings.infiniteLoop && slider.settings.mode !== 'fade' && !slider.settings.ticker) {
        var slice    = slider.settings.mode === 'vertical' ? slider.settings.minSlides : slider.settings.maxSlides,
        sliceAppend  = slider.children.slice(0, slice).clone(true).addClass('bx-clone'),
        slicePrepend = slider.children.slice(-slice).clone(true).addClass('bx-clone');
        if (slider.settings.ariaHidden) {
          sliceAppend.attr('aria-hidden', true);
          slicePrepend.attr('aria-hidden', true);
        }
        el.append(sliceAppend).prepend(slicePrepend);
      }
      // remove the loading DOM element
      slider.loader.remove();
      // set the left / top position of "el"
      setSlidePosition();
      // if "vertical" mode, always use adaptiveHeight to prevent odd behavior
      if (slider.settings.mode === 'vertical') { slider.settings.adaptiveHeight = true; }
      // set the viewport height
      slider.viewport.height(getViewportHeight());
      // make sure everything is positioned just right (same as a window resize)
      el.redrawSlider();
      // onSliderLoad callback
      slider.settings.onSliderLoad.call(el, slider.active.index);
      // slider has been fully initialized
      slider.initialized = true;
      // bind the resize call to the window
      if (slider.settings.responsive) { $(window).bind('resize', resizeWindow); }
      // if auto is true and has more than 1 page, start the show
      if (slider.settings.auto && slider.settings.autoStart && (getPagerQty() > 1 || slider.settings.autoSlideForOnePage)) { initAuto(); }
      // if ticker is true, start the ticker
      if (slider.settings.ticker) { initTicker(); }
      // if pager is requested, make the appropriate pager link active
      if (slider.settings.pager) { updatePagerActive(slider.settings.startSlide); }
      // check for any updates to the controls (like hideControlOnEnd updates)
      if (slider.settings.controls) { updateDirectionControls(); }
      // if touchEnabled is true, setup the touch events
      if (slider.settings.touchEnabled && !slider.settings.ticker) { initTouch(); }
      // if keyboardEnabled is true, setup the keyboard events
      if (slider.settings.keyboardEnabled && !slider.settings.ticker) {
        $(document).keydown(keyPress);
      }
    };

    /**
     * Returns the calculated height of the viewport, used to determine either adaptiveHeight or the maxHeight value
     */
    var getViewportHeight = function() {
      var height = 0;
      // first determine which children (slides) should be used in our height calculation
      var children = $();
      // if mode is not "vertical" and adaptiveHeight is false, include all children
      if (slider.settings.mode !== 'vertical' && !slider.settings.adaptiveHeight) {
        children = slider.children;
      } else {
        // if not carousel, return the single active child
        if (!slider.carousel) {
          children = slider.children.eq(slider.active.index);
        // if carousel, return a slice of children
        } else {
          // get the individual slide index
          var currentIndex = slider.settings.moveSlides === 1 ? slider.active.index : slider.active.index * getMoveBy();
          // add the current slide to the children
          children = slider.children.eq(currentIndex);
          // cycle through the remaining "showing" slides
          for (i = 1; i <= slider.settings.maxSlides - 1; i++) {
            // if looped back to the start
            if (currentIndex + i >= slider.children.length) {
              children = children.add(slider.children.eq(i - 1));
            } else {
              children = children.add(slider.children.eq(currentIndex + i));
            }
          }
        }
      }
      // if "vertical" mode, calculate the sum of the heights of the children
      if (slider.settings.mode === 'vertical') {
        children.each(function(index) {
          height += $(this).outerHeight();
        });
        // add user-supplied margins
        if (slider.settings.slideMargin > 0) {
          height += slider.settings.slideMargin * (slider.settings.minSlides - 1);
        }
      // if not "vertical" mode, calculate the max height of the children
      } else {
        height = Math.max.apply(Math, children.map(function() {
          return $(this).outerHeight(false);
        }).get());
      }

      if (slider.viewport.css('box-sizing') === 'border-box') {
        height += parseFloat(slider.viewport.css('padding-top')) + parseFloat(slider.viewport.css('padding-bottom')) +
              parseFloat(slider.viewport.css('border-top-width')) + parseFloat(slider.viewport.css('border-bottom-width'));
      } else if (slider.viewport.css('box-sizing') === 'padding-box') {
        height += parseFloat(slider.viewport.css('padding-top')) + parseFloat(slider.viewport.css('padding-bottom'));
      }

      return height;
    };

    /**
     * Returns the calculated width to be used for the outer wrapper / viewport
     */
    var getViewportMaxWidth = function() {
      var width = '100%';
      if (slider.settings.slideWidth > 0) {
        if (slider.settings.mode === 'horizontal') {
          width = (slider.settings.maxSlides * slider.settings.slideWidth) + ((slider.settings.maxSlides - 1) * slider.settings.slideMargin);
        } else {
          width = slider.settings.slideWidth;
        }
      }
      return width;
    };

    /**
     * Returns the calculated width to be applied to each slide
     */
    var getSlideWidth = function() {
      var newElWidth = slider.settings.slideWidth, // start with any user-supplied slide width
      wrapWidth      = slider.viewport.width();    // get the current viewport width
      // if slide width was not supplied, or is larger than the viewport use the viewport width
      if (slider.settings.slideWidth === 0 ||
        (slider.settings.slideWidth > wrapWidth && !slider.carousel) ||
        slider.settings.mode === 'vertical') {
        newElWidth = wrapWidth;
      // if carousel, use the thresholds to determine the width
      } else if (slider.settings.maxSlides > 1 && slider.settings.mode === 'horizontal') {
        if (wrapWidth > slider.maxThreshold) {
          return newElWidth;
        } else if (wrapWidth < slider.minThreshold) {
          newElWidth = (wrapWidth - (slider.settings.slideMargin * (slider.settings.minSlides - 1))) / slider.settings.minSlides;
        } else if (slider.settings.shrinkItems) {
          newElWidth = Math.floor((wrapWidth + slider.settings.slideMargin) / (Math.ceil((wrapWidth + slider.settings.slideMargin) / (newElWidth + slider.settings.slideMargin))) - slider.settings.slideMargin);
        }
      }
      return newElWidth;
    };

    /**
     * Returns the number of slides currently visible in the viewport (includes partially visible slides)
     */
    var getNumberSlidesShowing = function() {
      var slidesShowing = 1,
      childWidth = null;
      if (slider.settings.mode === 'horizontal' && slider.settings.slideWidth > 0) {
        // if viewport is smaller than minThreshold, return minSlides
        if (slider.viewport.width() < slider.minThreshold) {
          slidesShowing = slider.settings.minSlides;
        // if viewport is larger than maxThreshold, return maxSlides
        } else if (slider.viewport.width() > slider.maxThreshold) {
          slidesShowing = slider.settings.maxSlides;
        // if viewport is between min / max thresholds, divide viewport width by first child width
        } else {
          childWidth = slider.children.first().width() + slider.settings.slideMargin;
          slidesShowing = Math.floor((slider.viewport.width() +
            slider.settings.slideMargin) / childWidth) || 1;
        }
      // if "vertical" mode, slides showing will always be minSlides
      } else if (slider.settings.mode === 'vertical') {
        slidesShowing = slider.settings.minSlides;
      }
      return slidesShowing;
    };

    /**
     * Returns the number of pages (one full viewport of slides is one "page")
     */
    var getPagerQty = function() {
      var pagerQty = 0,
      breakPoint = 0,
      counter = 0;
      // if moveSlides is specified by the user
      if (slider.settings.moveSlides > 0) {
        if (slider.settings.infiniteLoop) {
          pagerQty = Math.ceil(slider.children.length / getMoveBy());
        } else {
          // when breakpoint goes above children length, counter is the number of pages
          while (breakPoint < slider.children.length) {
            ++pagerQty;
            breakPoint = counter + getNumberSlidesShowing();
            counter += slider.settings.moveSlides <= getNumberSlidesShowing() ? slider.settings.moveSlides : getNumberSlidesShowing();
          }
		  return counter;
        }
      // if moveSlides is 0 (auto) divide children length by sides showing, then round up
      } else {
        pagerQty = Math.ceil(slider.children.length / getNumberSlidesShowing());
      }
      return pagerQty;
    };

    /**
     * Returns the number of individual slides by which to shift the slider
     */
    var getMoveBy = function() {
      // if moveSlides was set by the user and moveSlides is less than number of slides showing
      if (slider.settings.moveSlides > 0 && slider.settings.moveSlides <= getNumberSlidesShowing()) {
        return slider.settings.moveSlides;
      }
      // if moveSlides is 0 (auto)
      return getNumberSlidesShowing();
    };

    /**
     * Sets the slider's (el) left or top position
     */
    var setSlidePosition = function() {
      var position, lastChild, lastShowingIndex;
      // if last slide, not infinite loop, and number of children is larger than specified maxSlides
      if (slider.children.length > slider.settings.maxSlides && slider.active.last && !slider.settings.infiniteLoop) {
        if (slider.settings.mode === 'horizontal') {
          // get the last child's position
          lastChild = slider.children.last();
          position = lastChild.position();
          // set the left position
          setPositionProperty(-(position.left - (slider.viewport.width() - lastChild.outerWidth())), 'reset', 0);
        } else if (slider.settings.mode === 'vertical') {
          // get the last showing index's position
          lastShowingIndex = slider.children.length - slider.settings.minSlides;
          position = slider.children.eq(lastShowingIndex).position();
          // set the top position
          setPositionProperty(-position.top, 'reset', 0);
        }
      // if not last slide
      } else {
        // get the position of the first showing slide
        position = slider.children.eq(slider.active.index * getMoveBy()).position();
        // check for last slide
        if (slider.active.index === getPagerQty() - 1) { slider.active.last = true; }
        // set the respective position
        if (position !== undefined) {
          if (slider.settings.mode === 'horizontal') { setPositionProperty(-position.left, 'reset', 0); }
          else if (slider.settings.mode === 'vertical') { setPositionProperty(-position.top, 'reset', 0); }
        }
      }
    };

    /**
     * Sets the el's animating property position (which in turn will sometimes animate el).
     * If using CSS, sets the transform property. If not using CSS, sets the top / left property.
     *
     * @param value (int)
     *  - the animating property's value
     *
     * @param type (string) 'slide', 'reset', 'ticker'
     *  - the type of instance for which the function is being
     *
     * @param duration (int)
     *  - the amount of time (in ms) the transition should occupy
     *
     * @param params (array) optional
     *  - an optional parameter containing any variables that need to be passed in
     */
    var setPositionProperty = function(value, type, duration, params) {
      var animateObj, propValue;
      // use CSS transform
      if (slider.usingCSS) {
        // determine the translate3d value
        propValue = slider.settings.mode === 'vertical' ? 'translate3d(0, ' + value + 'px, 0)' : 'translate3d(' + value + 'px, 0, 0)';
        // add the CSS transition-duration
        el.css('-' + slider.cssPrefix + '-transition-duration', duration / 1000 + 's');
        if (type === 'slide') {
          // set the property value
          el.css(slider.animProp, propValue);
          if (duration !== 0) {
            // bind a callback method - executes when CSS transition completes
            el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(e) {
              //make sure it's the correct one
              if (!$(e.target).is(el)) { return; }
              // unbind the callback
              el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
              updateAfterSlideTransition();
            });
          } else { //duration = 0
            updateAfterSlideTransition();
          }
        } else if (type === 'reset') {
          el.css(slider.animProp, propValue);
        } else if (type === 'ticker') {
          // make the transition use 'linear'
          el.css('-' + slider.cssPrefix + '-transition-timing-function', 'linear');
          el.css(slider.animProp, propValue);
          if (duration !== 0) {
            el.bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function(e) {
              //make sure it's the correct one
              if (!$(e.target).is(el)) { return; }
              // unbind the callback
              el.unbind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd');
              // reset the position
              setPositionProperty(params.resetValue, 'reset', 0);
              // start the loop again
              tickerLoop();
            });
          } else { //duration = 0
            setPositionProperty(params.resetValue, 'reset', 0);
            tickerLoop();
          }
        }
      // use JS animate
      } else {
        animateObj = {};
        animateObj[slider.animProp] = value;
        if (type === 'slide') {
          el.animate(animateObj, duration, slider.settings.easing, function() {
            updateAfterSlideTransition();
          });
        } else if (type === 'reset') {
          el.css(slider.animProp, value);
        } else if (type === 'ticker') {
          el.animate(animateObj, duration, 'linear', function() {
            setPositionProperty(params.resetValue, 'reset', 0);
            // run the recursive loop after animation
            tickerLoop();
          });
        }
      }
    };

    /**
     * Populates the pager with proper amount of pages
     */
    var populatePager = function() {
      var pagerHtml = '',
      linkContent = '',
      pagerQty = getPagerQty();
      // loop through each pager item
      for (var i = 0; i < pagerQty; i++) {
        linkContent = '';
        // if a buildPager function is supplied, use it to get pager link value, else use index + 1
        if (slider.settings.buildPager && $.isFunction(slider.settings.buildPager) || slider.settings.pagerCustom) {
          linkContent = slider.settings.buildPager(i);
          slider.pagerEl.addClass('bx-custom-pager');
        } else {
          linkContent = i + 1;
          slider.pagerEl.addClass('bx-default-pager');
        }
        // var linkContent = slider.settings.buildPager && $.isFunction(slider.settings.buildPager) ? slider.settings.buildPager(i) : i + 1;
        // add the markup to the string
        pagerHtml += '<div class="bx-pager-item"><a href="" data-slide-index="' + i + '" class="bx-pager-link">' + linkContent + '</a></div>';
      }
      // populate the pager element with pager links
      slider.pagerEl.html(pagerHtml);
    };

    /**
     * Appends the pager to the controls element
     */
    var appendPager = function() {
      if (!slider.settings.pagerCustom) {
        // create the pager DOM element
        slider.pagerEl = $('<div class="bx-pager" />');
        // if a pager selector was supplied, populate it with the pager
        if (slider.settings.pagerSelector) {
          $(slider.settings.pagerSelector).html(slider.pagerEl);
        // if no pager selector was supplied, add it after the wrapper
        } else {
          slider.controls.el.addClass('bx-has-pager').append(slider.pagerEl);
        }
        // populate the pager
        populatePager();
      } else {
        slider.pagerEl = $(slider.settings.pagerCustom);
      }
      // assign the pager click binding
      slider.pagerEl.on('click touchend', 'a', clickPagerBind);
    };

    /**
     * Appends prev / next controls to the controls element
     */
    var appendControls = function() {
      slider.controls.next = $('<a class="bx-next" href="">' + slider.settings.nextText + '</a>');
      slider.controls.prev = $('<a class="bx-prev" href="">' + slider.settings.prevText + '</a>');
      // bind click actions to the controls
      slider.controls.next.bind('click touchend', clickNextBind);
      slider.controls.prev.bind('click touchend', clickPrevBind);
      // if nextSelector was supplied, populate it
      if (slider.settings.nextSelector) {
        $(slider.settings.nextSelector).append(slider.controls.next);
      }
      // if prevSelector was supplied, populate it
      if (slider.settings.prevSelector) {
        $(slider.settings.prevSelector).append(slider.controls.prev);
      }
      // if no custom selectors were supplied
      if (!slider.settings.nextSelector && !slider.settings.prevSelector) {
        // add the controls to the DOM
        slider.controls.directionEl = $('<div class="bx-controls-direction" />');
        // add the control elements to the directionEl
        slider.controls.directionEl.append(slider.controls.prev).append(slider.controls.next);
        // slider.viewport.append(slider.controls.directionEl);
        slider.controls.el.addClass('bx-has-controls-direction').append(slider.controls.directionEl);
      }
    };

    /**
     * Appends start / stop auto controls to the controls element
     */
    var appendControlsAuto = function() {
      slider.controls.start = $('<div class="bx-controls-auto-item"><a class="bx-start" href="">' + slider.settings.startText + '</a></div>');
      slider.controls.stop = $('<div class="bx-controls-auto-item"><a class="bx-stop" href="">' + slider.settings.stopText + '</a></div>');
      // add the controls to the DOM
      slider.controls.autoEl = $('<div class="bx-controls-auto" />');
      // bind click actions to the controls
      slider.controls.autoEl.on('click', '.bx-start', clickStartBind);
      slider.controls.autoEl.on('click', '.bx-stop', clickStopBind);
      // if autoControlsCombine, insert only the "start" control
      if (slider.settings.autoControlsCombine) {
        slider.controls.autoEl.append(slider.controls.start);
      // if autoControlsCombine is false, insert both controls
      } else {
        slider.controls.autoEl.append(slider.controls.start).append(slider.controls.stop);
      }
      // if auto controls selector was supplied, populate it with the controls
      if (slider.settings.autoControlsSelector) {
        $(slider.settings.autoControlsSelector).html(slider.controls.autoEl);
      // if auto controls selector was not supplied, add it after the wrapper
      } else {
        slider.controls.el.addClass('bx-has-controls-auto').append(slider.controls.autoEl);
      }
      // update the auto controls
      updateAutoControls(slider.settings.autoStart ? 'stop' : 'start');
    };

    /**
     * Appends image captions to the DOM
     */
    var appendCaptions = function() {
      // cycle through each child
      slider.children.each(function(index) {
        // get the image title attribute
        var title = $(this).find('img:first').attr('title');
        // append the caption
        if (title !== undefined && ('' + title).length) {
          $(this).append('<div class="bx-caption"><span>' + title + '</span></div>');
        }
      });
    };

    /**
     * Click next binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickNextBind = function(e) {
      e.preventDefault();
      if (slider.controls.el.hasClass('disabled')) { return; }
      // if auto show is running, stop it
      if (slider.settings.auto && slider.settings.stopAutoOnClick) { el.stopAuto(); }
      el.goToNextSlide();
    };

    /**
     * Click prev binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickPrevBind = function(e) {
      e.preventDefault();
      if (slider.controls.el.hasClass('disabled')) { return; }
      // if auto show is running, stop it
      if (slider.settings.auto && slider.settings.stopAutoOnClick) { el.stopAuto(); }
      el.goToPrevSlide();
    };

    /**
     * Click start binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickStartBind = function(e) {
      el.startAuto();
      e.preventDefault();
    };

    /**
     * Click stop binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickStopBind = function(e) {
      el.stopAuto();
      e.preventDefault();
    };

    /**
     * Click pager binding
     *
     * @param e (event)
     *  - DOM event object
     */
    var clickPagerBind = function(e) {
      var pagerLink, pagerIndex;
      e.preventDefault();
      if (slider.controls.el.hasClass('disabled')) {
        return;
      }
      // if auto show is running, stop it
      if (slider.settings.auto  && slider.settings.stopAutoOnClick) { el.stopAuto(); }
      pagerLink = $(e.currentTarget);
      if (pagerLink.attr('data-slide-index') !== undefined) {
        pagerIndex = parseInt(pagerLink.attr('data-slide-index'));
        // if clicked pager link is not active, continue with the goToSlide call
        if (pagerIndex !== slider.active.index) { el.goToSlide(pagerIndex); }
      }
    };

    /**
     * Updates the pager links with an active class
     *
     * @param slideIndex (int)
     *  - index of slide to make active
     */
    var updatePagerActive = function(slideIndex) {
      // if "short" pager type
      var len = slider.children.length; // nb of children
      if (slider.settings.pagerType === 'short') {
        if (slider.settings.maxSlides > 1) {
          len = Math.ceil(slider.children.length / slider.settings.maxSlides);
        }
        slider.pagerEl.html((slideIndex + 1) + slider.settings.pagerShortSeparator + len);
        return;
      }
      // remove all pager active classes
      slider.pagerEl.find('a').removeClass('active');
      // apply the active class for all pagers
      slider.pagerEl.each(function(i, el) { $(el).find('a').eq(slideIndex).addClass('active'); });
    };

    /**
     * Performs needed actions after a slide transition
     */
    var updateAfterSlideTransition = function() {
      // if infinite loop is true
      if (slider.settings.infiniteLoop) {
        var position = '';
        // first slide
        if (slider.active.index === 0) {
          // set the new position
          position = slider.children.eq(0).position();
        // carousel, last slide
        } else if (slider.active.index === getPagerQty() - 1 && slider.carousel) {
          position = slider.children.eq((getPagerQty() - 1) * getMoveBy()).position();
        // last slide
        } else if (slider.active.index === slider.children.length - 1) {
          position = slider.children.eq(slider.children.length - 1).position();
        }
        if (position) {
          if (slider.settings.mode === 'horizontal') { setPositionProperty(-position.left, 'reset', 0); }
          else if (slider.settings.mode === 'vertical') { setPositionProperty(-position.top, 'reset', 0); }
        }
      }
      // declare that the transition is complete
      slider.working = false;
      // onSlideAfter callback
      slider.settings.onSlideAfter.call(el, slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);
    };

    /**
     * Updates the auto controls state (either active, or combined switch)
     *
     * @param state (string) "start", "stop"
     *  - the new state of the auto show
     */
    var updateAutoControls = function(state) {
      // if autoControlsCombine is true, replace the current control with the new state
      if (slider.settings.autoControlsCombine) {
        slider.controls.autoEl.html(slider.controls[state]);
      // if autoControlsCombine is false, apply the "active" class to the appropriate control
      } else {
        slider.controls.autoEl.find('a').removeClass('active');
        slider.controls.autoEl.find('a:not(.bx-' + state + ')').addClass('active');
      }
    };

    /**
     * Updates the direction controls (checks if either should be hidden)
     */
    var updateDirectionControls = function() {
      if (getPagerQty() === 1) {
        slider.controls.prev.addClass('disabled');
        slider.controls.next.addClass('disabled');
      } else if (!slider.settings.infiniteLoop && slider.settings.hideControlOnEnd) {
        // if first slide
        if (slider.active.index === 0) {
          slider.controls.prev.addClass('disabled');
          slider.controls.next.removeClass('disabled');
        // if last slide
        } else if (slider.active.index === getPagerQty() - 1) {
          slider.controls.next.addClass('disabled');
          slider.controls.prev.removeClass('disabled');
        // if any slide in the middle
        } else {
          slider.controls.prev.removeClass('disabled');
          slider.controls.next.removeClass('disabled');
        }
      }
    };
	/* auto start and stop functions */
	var windowFocusHandler = function() { el.startAuto(); };
	var windowBlurHandler = function() { el.stopAuto(); };
    /**
     * Initializes the auto process
     */
    var initAuto = function() {
      // if autoDelay was supplied, launch the auto show using a setTimeout() call
      if (slider.settings.autoDelay > 0) {
        var timeout = setTimeout(el.startAuto, slider.settings.autoDelay);
      // if autoDelay was not supplied, start the auto show normally
      } else {
        el.startAuto();

        //add focus and blur events to ensure its running if timeout gets paused
        $(window).focus(windowFocusHandler).blur(windowBlurHandler);
      }
      // if autoHover is requested
      if (slider.settings.autoHover) {
        // on el hover
        el.hover(function() {
          // if the auto show is currently playing (has an active interval)
          if (slider.interval) {
            // stop the auto show and pass true argument which will prevent control update
            el.stopAuto(true);
            // create a new autoPaused value which will be used by the relative "mouseout" event
            slider.autoPaused = true;
          }
        }, function() {
          // if the autoPaused value was created be the prior "mouseover" event
          if (slider.autoPaused) {
            // start the auto show and pass true argument which will prevent control update
            el.startAuto(true);
            // reset the autoPaused value
            slider.autoPaused = null;
          }
        });
      }
    };

    /**
     * Initializes the ticker process
     */
    var initTicker = function() {
      var startPosition = 0,
      position, transform, value, idx, ratio, property, newSpeed, totalDimens;
      // if autoDirection is "next", append a clone of the entire slider
      if (slider.settings.autoDirection === 'next') {
        el.append(slider.children.clone().addClass('bx-clone'));
      // if autoDirection is "prev", prepend a clone of the entire slider, and set the left position
      } else {
        el.prepend(slider.children.clone().addClass('bx-clone'));
        position = slider.children.first().position();
        startPosition = slider.settings.mode === 'horizontal' ? -position.left : -position.top;
      }
      setPositionProperty(startPosition, 'reset', 0);
      // do not allow controls in ticker mode
      slider.settings.pager = false;
      slider.settings.controls = false;
      slider.settings.autoControls = false;
      // if autoHover is requested
      if (slider.settings.tickerHover) {
        if (slider.usingCSS) {
          idx = slider.settings.mode === 'horizontal' ? 4 : 5;
          slider.viewport.hover(function() {
            transform = el.css('-' + slider.cssPrefix + '-transform');
            value = parseFloat(transform.split(',')[idx]);
            setPositionProperty(value, 'reset', 0);
          }, function() {
            totalDimens = 0;
            slider.children.each(function(index) {
              totalDimens += slider.settings.mode === 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
            });
            // calculate the speed ratio (used to determine the new speed to finish the paused animation)
            ratio = slider.settings.speed / totalDimens;
            // determine which property to use
            property = slider.settings.mode === 'horizontal' ? 'left' : 'top';
            // calculate the new speed
            newSpeed = ratio * (totalDimens - (Math.abs(parseInt(value))));
            tickerLoop(newSpeed);
          });
        } else {
          // on el hover
          slider.viewport.hover(function() {
            el.stop();
          }, function() {
            // calculate the total width of children (used to calculate the speed ratio)
            totalDimens = 0;
            slider.children.each(function(index) {
              totalDimens += slider.settings.mode === 'horizontal' ? $(this).outerWidth(true) : $(this).outerHeight(true);
            });
            // calculate the speed ratio (used to determine the new speed to finish the paused animation)
            ratio = slider.settings.speed / totalDimens;
            // determine which property to use
            property = slider.settings.mode === 'horizontal' ? 'left' : 'top';
            // calculate the new speed
            newSpeed = ratio * (totalDimens - (Math.abs(parseInt(el.css(property)))));
            tickerLoop(newSpeed);
          });
        }
      }
      // start the ticker loop
      tickerLoop();
    };

    /**
     * Runs a continuous loop, news ticker-style
     */
    var tickerLoop = function(resumeSpeed) {
      var speed = resumeSpeed ? resumeSpeed : slider.settings.speed,
      position = {left: 0, top: 0},
      reset = {left: 0, top: 0},
      animateProperty, resetValue, params;

      // if "next" animate left position to last child, then reset left to 0
      if (slider.settings.autoDirection === 'next') {
        position = el.find('.bx-clone').first().position();
      // if "prev" animate left position to 0, then reset left to first non-clone child
      } else {
        reset = slider.children.first().position();
      }
      animateProperty = slider.settings.mode === 'horizontal' ? -position.left : -position.top;
      resetValue = slider.settings.mode === 'horizontal' ? -reset.left : -reset.top;
      params = {resetValue: resetValue};
      setPositionProperty(animateProperty, 'ticker', speed, params);
    };

    /**
     * Check if el is on screen
     */
    var isOnScreen = function(el) {
      var win = $(window),
      viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
      },
      bounds = el.offset();

      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();
      bounds.right = bounds.left + el.outerWidth();
      bounds.bottom = bounds.top + el.outerHeight();

      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };

    /**
     * Initializes keyboard events
     */
    var keyPress = function(e) {
      var activeElementTag = document.activeElement.tagName.toLowerCase(),
      tagFilters = 'input|textarea',
      p = new RegExp(activeElementTag,['i']),
      result = p.exec(tagFilters);

      if (result == null && isOnScreen(el)) {
        if (e.keyCode === 39) {
          clickNextBind(e);
          return false;
        } else if (e.keyCode === 37) {
          clickPrevBind(e);
          return false;
        }
      }
    };

    /**
     * Initializes touch events
     */
    var initTouch = function() {
      // initialize object to contain all touch values
      slider.touch = {
        start: {x: 0, y: 0},
        end: {x: 0, y: 0}
      };
      slider.viewport.bind('touchstart MSPointerDown pointerdown', onTouchStart);

      //for browsers that have implemented pointer events and fire a click after
      //every pointerup regardless of whether pointerup is on same screen location as pointerdown or not
      slider.viewport.on('click', '.bxslider a', function(e) {
        if (slider.viewport.hasClass('click-disabled')) {
          e.preventDefault();
          slider.viewport.removeClass('click-disabled');
        }
      });
    };

    /**
     * Event handler for "touchstart"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchStart = function(e) {
      //disable slider controls while user is interacting with slides to avoid slider freeze that happens on touch devices when a slide swipe happens immediately after interacting with slider controls
      slider.controls.el.addClass('disabled');

      if (slider.working) {
        e.preventDefault();
        slider.controls.el.removeClass('disabled');
      } else {
        // record the original position when touch starts
        slider.touch.originalPos = el.position();
        var orig = e.originalEvent,
        touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig];
        // record the starting touch x, y coordinates
        slider.touch.start.x = touchPoints[0].pageX;
        slider.touch.start.y = touchPoints[0].pageY;

        if (slider.viewport.get(0).setPointerCapture) {
          slider.pointerId = orig.pointerId;
          slider.viewport.get(0).setPointerCapture(slider.pointerId);
        }
        // bind a "touchmove" event to the viewport
        slider.viewport.bind('touchmove MSPointerMove pointermove', onTouchMove);
        // bind a "touchend" event to the viewport
        slider.viewport.bind('touchend MSPointerUp pointerup', onTouchEnd);
        slider.viewport.bind('MSPointerCancel pointercancel', onPointerCancel);
      }
    };

    /**
     * Cancel Pointer for Windows Phone
     *
     * @param e (event)
     *  - DOM event object
     */
    var onPointerCancel = function(e) {
      /* onPointerCancel handler is needed to deal with situations when a touchend
      doesn't fire after a touchstart (this happens on windows phones only) */
      setPositionProperty(slider.touch.originalPos.left, 'reset', 0);

      //remove handlers
      slider.controls.el.removeClass('disabled');
      slider.viewport.unbind('MSPointerCancel pointercancel', onPointerCancel);
      slider.viewport.unbind('touchmove MSPointerMove pointermove', onTouchMove);
      slider.viewport.unbind('touchend MSPointerUp pointerup', onTouchEnd);
      if (slider.viewport.get(0).releasePointerCapture) {
        slider.viewport.get(0).releasePointerCapture(slider.pointerId);
      }
    };

    /**
     * Event handler for "touchmove"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchMove = function(e) {
      var orig = e.originalEvent,
      touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig],
      // if scrolling on y axis, do not prevent default
      xMovement = Math.abs(touchPoints[0].pageX - slider.touch.start.x),
      yMovement = Math.abs(touchPoints[0].pageY - slider.touch.start.y),
      value = 0,
      change = 0;

      // x axis swipe
      if ((xMovement * 3) > yMovement && slider.settings.preventDefaultSwipeX) {
        e.preventDefault();
      // y axis swipe
      } else if ((yMovement * 3) > xMovement && slider.settings.preventDefaultSwipeY) {
        e.preventDefault();
      }
      if (slider.settings.mode !== 'fade' && slider.settings.oneToOneTouch) {
        // if horizontal, drag along x axis
        if (slider.settings.mode === 'horizontal') {
          change = touchPoints[0].pageX - slider.touch.start.x;
          value = slider.touch.originalPos.left + change;
        // if vertical, drag along y axis
        } else {
          change = touchPoints[0].pageY - slider.touch.start.y;
          value = slider.touch.originalPos.top + change;
        }
        setPositionProperty(value, 'reset', 0);
      }
    };

    /**
     * Event handler for "touchend"
     *
     * @param e (event)
     *  - DOM event object
     */
    var onTouchEnd = function(e) {
      slider.viewport.unbind('touchmove MSPointerMove pointermove', onTouchMove);
      //enable slider controls as soon as user stops interacing with slides
      slider.controls.el.removeClass('disabled');
      var orig    = e.originalEvent,
      touchPoints = (typeof orig.changedTouches !== 'undefined') ? orig.changedTouches : [orig],
      value       = 0,
      distance    = 0;
      // record end x, y positions
      slider.touch.end.x = touchPoints[0].pageX;
      slider.touch.end.y = touchPoints[0].pageY;
      // if fade mode, check if absolute x distance clears the threshold
      if (slider.settings.mode === 'fade') {
        distance = Math.abs(slider.touch.start.x - slider.touch.end.x);
        if (distance >= slider.settings.swipeThreshold) {
          if (slider.touch.start.x > slider.touch.end.x) {
            el.goToNextSlide();
          } else {
            el.goToPrevSlide();
          }
          el.stopAuto();
        }
      // not fade mode
      } else {
        // calculate distance and el's animate property
        if (slider.settings.mode === 'horizontal') {
          distance = slider.touch.end.x - slider.touch.start.x;
          value = slider.touch.originalPos.left;
        } else {
          distance = slider.touch.end.y - slider.touch.start.y;
          value = slider.touch.originalPos.top;
        }
        // if not infinite loop and first / last slide, do not attempt a slide transition
        if (!slider.settings.infiniteLoop && ((slider.active.index === 0 && distance > 0) || (slider.active.last && distance < 0))) {
          setPositionProperty(value, 'reset', 200);
        } else {
          // check if distance clears threshold
          if (Math.abs(distance) >= slider.settings.swipeThreshold) {
            if (distance < 0) {
              el.goToNextSlide();
            } else {
              el.goToPrevSlide();
            }
            el.stopAuto();
          } else {
            // el.animate(property, 200);
            setPositionProperty(value, 'reset', 200);
          }
        }
      }
      slider.viewport.unbind('touchend MSPointerUp pointerup', onTouchEnd);
      if (slider.viewport.get(0).releasePointerCapture) {
        slider.viewport.get(0).releasePointerCapture(slider.pointerId);
      }
    };

    /**
     * Window resize event callback
     */
    var resizeWindow = function(e) {
      // don't do anything if slider isn't initialized.
      if (!slider.initialized) { return; }
      // Delay if slider working.
      if (slider.working) {
        window.setTimeout(resizeWindow, 10);
      } else {
        // get the new window dimens (again, thank you IE)
        var windowWidthNew = $(window).width(),
        windowHeightNew = $(window).height();
        // make sure that it is a true window resize
        // *we must check this because our dinosaur friend IE fires a window resize event when certain DOM elements
        // are resized. Can you just die already?*
        if (windowWidth !== windowWidthNew || windowHeight !== windowHeightNew) {
          // set the new window dimens
          windowWidth = windowWidthNew;
          windowHeight = windowHeightNew;
          // update all dynamic elements
          el.redrawSlider();
          // Call user resize handler
          slider.settings.onSliderResize.call(el, slider.active.index);
        }
      }
    };

    /**
     * Adds an aria-hidden=true attribute to each element
     *
     * @param startVisibleIndex (int)
     *  - the first visible element's index
     */
    var applyAriaHiddenAttributes = function(startVisibleIndex) {
      var numberOfSlidesShowing = getNumberSlidesShowing();
      // only apply attributes if the setting is enabled and not in ticker mode
      if (slider.settings.ariaHidden && !slider.settings.ticker) {
        // add aria-hidden=true to all elements
        slider.children.attr('aria-hidden', 'true');
        // get the visible elements and change to aria-hidden=false
        slider.children.slice(startVisibleIndex, startVisibleIndex + numberOfSlidesShowing).attr('aria-hidden', 'false');
      }
    };

    /**
     * Returns index according to present page range
     *
     * @param slideOndex (int)
     *  - the desired slide index
     */
    var setSlideIndex = function(slideIndex) {
      if (slideIndex < 0) {
        if (slider.settings.infiniteLoop) {
          return getPagerQty() - 1;
        }else {
          //we don't go to undefined slides
          return slider.active.index;
        }
      // if slideIndex is greater than children length, set active index to 0 (this happens during infinite loop)
      } else if (slideIndex >= getPagerQty()) {
        if (slider.settings.infiniteLoop) {
          return 0;
        } else {
          //we don't move to undefined pages
          return slider.active.index;
        }
      // set active index to requested slide
      } else {
        return slideIndex;
      }
    };

    /**
     * ===================================================================================
     * = PUBLIC FUNCTIONS
     * ===================================================================================
     */

    /**
     * Performs slide transition to the specified slide
     *
     * @param slideIndex (int)
     *  - the destination slide's index (zero-based)
     *
     * @param direction (string)
     *  - INTERNAL USE ONLY - the direction of travel ("prev" / "next")
     */
    el.goToSlide = function(slideIndex, direction) {
      // onSlideBefore, onSlideNext, onSlidePrev callbacks
      // Allow transition canceling based on returned value
      var performTransition = true,
      moveBy = 0,
      position = {left: 0, top: 0},
      lastChild = null,
      lastShowingIndex, eq, value, requestEl;
      // store the old index
      slider.oldIndex = slider.active.index;
      //set new index
      slider.active.index = setSlideIndex(slideIndex);

      // if plugin is currently in motion, ignore request
      if (slider.working || slider.active.index === slider.oldIndex) { return; }
      // declare that plugin is in motion
      slider.working = true;

      performTransition = slider.settings.onSlideBefore.call(el, slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index);

      // If transitions canceled, reset and return
      if (typeof (performTransition) !== 'undefined' && !performTransition) {
        slider.active.index = slider.oldIndex; // restore old index
        slider.working = false; // is not in motion
        return;
      }

      if (direction === 'next') {
        // Prevent canceling in future functions or lack there-of from negating previous commands to cancel
        if (!slider.settings.onSlideNext.call(el, slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index)) {
          performTransition = false;
        }
      } else if (direction === 'prev') {
        // Prevent canceling in future functions or lack there-of from negating previous commands to cancel
        if (!slider.settings.onSlidePrev.call(el, slider.children.eq(slider.active.index), slider.oldIndex, slider.active.index)) {
          performTransition = false;
        }
      }

      // check if last slide
      slider.active.last = slider.active.index >= getPagerQty() - 1;
      // update the pager with active class
      if (slider.settings.pager || slider.settings.pagerCustom) { updatePagerActive(slider.active.index); }
      // // check for direction control update
      if (slider.settings.controls) { updateDirectionControls(); }
      // if slider is set to mode: "fade"
      if (slider.settings.mode === 'fade') {
        // if adaptiveHeight is true and next height is different from current height, animate to the new height
        if (slider.settings.adaptiveHeight && slider.viewport.height() !== getViewportHeight()) {
          slider.viewport.animate({height: getViewportHeight()}, slider.settings.adaptiveHeightSpeed);
        }
        // fade out the visible child and reset its z-index value
        slider.children.filter(':visible').fadeOut(slider.settings.speed).css({zIndex: 0});
        // fade in the newly requested slide
        slider.children.eq(slider.active.index).css('zIndex', slider.settings.slideZIndex + 1).fadeIn(slider.settings.speed, function() {
          $(this).css('zIndex', slider.settings.slideZIndex);
          updateAfterSlideTransition();
        });
      // slider mode is not "fade"
      } else {
        // if adaptiveHeight is true and next height is different from current height, animate to the new height
        if (slider.settings.adaptiveHeight && slider.viewport.height() !== getViewportHeight()) {
          slider.viewport.animate({height: getViewportHeight()}, slider.settings.adaptiveHeightSpeed);
        }
        // if carousel and not infinite loop
        if (!slider.settings.infiniteLoop && slider.carousel && slider.active.last) {
          if (slider.settings.mode === 'horizontal') {
            // get the last child position
            lastChild = slider.children.eq(slider.children.length - 1);
            position = lastChild.position();
            // calculate the position of the last slide
            moveBy = slider.viewport.width() - lastChild.outerWidth();
          } else {
            // get last showing index position
            lastShowingIndex = slider.children.length - slider.settings.minSlides;
            position = slider.children.eq(lastShowingIndex).position();
          }
          // horizontal carousel, going previous while on first slide (infiniteLoop mode)
        } else if (slider.carousel && slider.active.last && direction === 'prev') {
          // get the last child position
          eq = slider.settings.moveSlides === 1 ? slider.settings.maxSlides - getMoveBy() : ((getPagerQty() - 1) * getMoveBy()) - (slider.children.length - slider.settings.maxSlides);
          lastChild = el.children('.bx-clone').eq(eq);
          position = lastChild.position();
        // if infinite loop and "Next" is clicked on the last slide
        } else if (direction === 'next' && slider.active.index === 0) {
          // get the last clone position
          position = el.find('> .bx-clone').eq(slider.settings.maxSlides).position();
          slider.active.last = false;
        // normal non-zero requests
        } else if (slideIndex >= 0) {
          //parseInt is applied to allow floats for slides/page
          requestEl = slideIndex * parseInt(getMoveBy());
          position = slider.children.eq(requestEl).position();
        }

        /* If the position doesn't exist
         * (e.g. if you destroy the slider on a next click),
         * it doesn't throw an error.
         */
        if (typeof (position) !== 'undefined') {
          value = slider.settings.mode === 'horizontal' ? -(position.left - moveBy) : -position.top;
          // plugin values to be animated
          setPositionProperty(value, 'slide', slider.settings.speed);
        }
        slider.working = false;
      }
      if (slider.settings.ariaHidden) { applyAriaHiddenAttributes(slider.active.index * getMoveBy()); }
    };

    /**
     * Transitions to the next slide in the show
     */
    el.goToNextSlide = function() {
      // if infiniteLoop is false and last page is showing, disregard call
      if (!slider.settings.infiniteLoop && slider.active.last) { return; }
	  if (slider.working == true){ return ;}
      var pagerIndex = parseInt(slider.active.index) + 1;
      el.goToSlide(pagerIndex, 'next');
    };

    /**
     * Transitions to the prev slide in the show
     */
    el.goToPrevSlide = function() {
      // if infiniteLoop is false and last page is showing, disregard call
      if (!slider.settings.infiniteLoop && slider.active.index === 0) { return; }
	  if (slider.working == true){ return ;}
      var pagerIndex = parseInt(slider.active.index) - 1;
      el.goToSlide(pagerIndex, 'prev');
    };

    /**
     * Starts the auto show
     *
     * @param preventControlUpdate (boolean)
     *  - if true, auto controls state will not be updated
     */
    el.startAuto = function(preventControlUpdate) {
      // if an interval already exists, disregard call
      if (slider.interval) { return; }
      // create an interval
      slider.interval = setInterval(function() {
        if (slider.settings.autoDirection === 'next') {
          el.goToNextSlide();
        } else {
          el.goToPrevSlide();
        }
      }, slider.settings.pause);
	  //allback for when the auto rotate status changes
	  slider.settings.onAutoChange.call(el, true);
      // if auto controls are displayed and preventControlUpdate is not true
      if (slider.settings.autoControls && preventControlUpdate !== true) { updateAutoControls('stop'); }
    };

    /**
     * Stops the auto show
     *
     * @param preventControlUpdate (boolean)
     *  - if true, auto controls state will not be updated
     */
    el.stopAuto = function(preventControlUpdate) {
      // if no interval exists, disregard call
      if (!slider.interval) { return; }
      // clear the interval
      clearInterval(slider.interval);
      slider.interval = null;
	  //allback for when the auto rotate status changes
	  slider.settings.onAutoChange.call(el, false);
      // if auto controls are displayed and preventControlUpdate is not true
      if (slider.settings.autoControls && preventControlUpdate !== true) { updateAutoControls('start'); }
    };

    /**
     * Returns current slide index (zero-based)
     */
    el.getCurrentSlide = function() {
      return slider.active.index;
    };

    /**
     * Returns current slide element
     */
    el.getCurrentSlideElement = function() {
      return slider.children.eq(slider.active.index);
    };

    /**
     * Returns a slide element
     * @param index (int)
     *  - The index (zero-based) of the element you want returned.
     */
    el.getSlideElement = function(index) {
      return slider.children.eq(index);
    };

    /**
     * Returns number of slides in show
     */
    el.getSlideCount = function() {
      return slider.children.length;
    };

    /**
     * Return slider.working variable
     */
    el.isWorking = function() {
      return slider.working;
    };

    /**
     * Update all dynamic slider elements
     */
    el.redrawSlider = function() {
      // resize all children in ratio to new screen size
      slider.children.add(el.find('.bx-clone')).outerWidth(getSlideWidth());
      // adjust the height
      slider.viewport.css('height', getViewportHeight());
      // update the slide position
      if (!slider.settings.ticker) { setSlidePosition(); }
      // if active.last was true before the screen resize, we want
      // to keep it last no matter what screen size we end on
      if (slider.active.last) { slider.active.index = getPagerQty() - 1; }
      // if the active index (page) no longer exists due to the resize, simply set the index as last
      if (slider.active.index >= getPagerQty()) { slider.active.last = true; }
      // if a pager is being displayed and a custom pager is not being used, update it
      if (slider.settings.pager && !slider.settings.pagerCustom) {
        populatePager();
        updatePagerActive(slider.active.index);
      }
      if (slider.settings.ariaHidden) { applyAriaHiddenAttributes(slider.active.index * getMoveBy()); }
    };

    /**
     * Destroy the current instance of the slider (revert everything back to original state)
     */
    el.destroySlider = function() {
      // don't do anything if slider has already been destroyed
      if (!slider.initialized) { return; }
      slider.initialized = false;
      $('.bx-clone', this).remove();
      slider.children.each(function() {
        if ($(this).data('origStyle') !== undefined) {
          $(this).attr('style', $(this).data('origStyle'));
        } else {
          $(this).removeAttr('style');
        }
      });
      if ($(this).data('origStyle') !== undefined) {
        this.attr('style', $(this).data('origStyle'));
      } else {
        $(this).removeAttr('style');
      }
      $(this).unwrap().unwrap();
      if (slider.controls.el) { slider.controls.el.remove(); }
      if (slider.controls.next) { slider.controls.next.remove(); }
      if (slider.controls.prev) { slider.controls.prev.remove(); }
      if (slider.pagerEl && slider.settings.controls && !slider.settings.pagerCustom) { slider.pagerEl.remove(); }
      $('.bx-caption', this).remove();
      if (slider.controls.autoEl) { slider.controls.autoEl.remove(); }
      clearInterval(slider.interval);
      if (slider.settings.responsive) { $(window).unbind('resize', resizeWindow); }
      if (slider.settings.keyboardEnabled) { $(document).unbind('keydown', keyPress); }
      //remove self reference in data
      $(this).removeData('bxSlider');
	  // remove global window handlers
	  $(window).off('blur', windowBlurHandler).off('focus', windowFocusHandler);
    };

    /**
     * Reload the slider (revert all DOM changes, and re-initialize)
     */
    el.reloadSlider = function(settings) {
      if (settings !== undefined) { options = settings; }
      el.destroySlider();
      init();
      //store reference to self in order to access public functions later
      $(el).data('bxSlider', this);
    };

    init();

    $(el).data('bxSlider', this);

    // returns the current jQuery object
    return this;
  };

})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJqcXVlcnkuYnhzbGlkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBieFNsaWRlciB2NC4yLjEyXG4gKiBDb3B5cmlnaHQgMjAxMy0yMDE1IFN0ZXZlbiBXYW5kZXJza2lcbiAqIFdyaXR0ZW4gd2hpbGUgZHJpbmtpbmcgQmVsZ2lhbiBhbGVzIGFuZCBsaXN0ZW5pbmcgdG8gamF6elxuICogTGljZW5zZWQgdW5kZXIgTUlUIChodHRwOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUKVxuICovXG5cbjsoZnVuY3Rpb24oJCkge1xuXG4gIHZhciBkZWZhdWx0cyA9IHtcblxuICAgIC8vIEdFTkVSQUxcbiAgICBtb2RlOiAnaG9yaXpvbnRhbCcsXG4gICAgc2xpZGVTZWxlY3RvcjogJycsXG4gICAgaW5maW5pdGVMb29wOiB0cnVlLFxuICAgIGhpZGVDb250cm9sT25FbmQ6IGZhbHNlLFxuICAgIHNwZWVkOiA1MDAsXG4gICAgZWFzaW5nOiBudWxsLFxuICAgIHNsaWRlTWFyZ2luOiAwLFxuICAgIHN0YXJ0U2xpZGU6IDAsXG4gICAgcmFuZG9tU3RhcnQ6IGZhbHNlLFxuICAgIGNhcHRpb25zOiBmYWxzZSxcbiAgICB0aWNrZXI6IGZhbHNlLFxuICAgIHRpY2tlckhvdmVyOiBmYWxzZSxcbiAgICBhZGFwdGl2ZUhlaWdodDogZmFsc2UsXG4gICAgYWRhcHRpdmVIZWlnaHRTcGVlZDogNTAwLFxuICAgIHZpZGVvOiBmYWxzZSxcbiAgICB1c2VDU1M6IHRydWUsXG4gICAgcHJlbG9hZEltYWdlczogJ3Zpc2libGUnLFxuICAgIHJlc3BvbnNpdmU6IHRydWUsXG4gICAgc2xpZGVaSW5kZXg6IDUwLFxuICAgIHdyYXBwZXJDbGFzczogJ2J4LXdyYXBwZXInLFxuXG4gICAgLy8gVE9VQ0hcbiAgICB0b3VjaEVuYWJsZWQ6IHRydWUsXG4gICAgc3dpcGVUaHJlc2hvbGQ6IDUwLFxuICAgIG9uZVRvT25lVG91Y2g6IHRydWUsXG4gICAgcHJldmVudERlZmF1bHRTd2lwZVg6IHRydWUsXG4gICAgcHJldmVudERlZmF1bHRTd2lwZVk6IGZhbHNlLFxuXG4gICAgLy8gQUNDRVNTSUJJTElUWVxuICAgIGFyaWFMaXZlOiB0cnVlLFxuICAgIGFyaWFIaWRkZW46IHRydWUsXG5cbiAgICAvLyBLRVlCT0FSRFxuICAgIGtleWJvYXJkRW5hYmxlZDogZmFsc2UsXG5cbiAgICAvLyBQQUdFUlxuICAgIHBhZ2VyOiB0cnVlLFxuICAgIHBhZ2VyVHlwZTogJ2Z1bGwnLFxuICAgIHBhZ2VyU2hvcnRTZXBhcmF0b3I6ICcgLyAnLFxuICAgIHBhZ2VyU2VsZWN0b3I6IG51bGwsXG4gICAgYnVpbGRQYWdlcjogbnVsbCxcbiAgICBwYWdlckN1c3RvbTogbnVsbCxcblxuICAgIC8vIENPTlRST0xTXG4gICAgY29udHJvbHM6IHRydWUsXG4gICAgbmV4dFRleHQ6ICdOZXh0JyxcbiAgICBwcmV2VGV4dDogJ1ByZXYnLFxuICAgIG5leHRTZWxlY3RvcjogbnVsbCxcbiAgICBwcmV2U2VsZWN0b3I6IG51bGwsXG4gICAgYXV0b0NvbnRyb2xzOiBmYWxzZSxcbiAgICBzdGFydFRleHQ6ICdTdGFydCcsXG4gICAgc3RvcFRleHQ6ICdTdG9wJyxcbiAgICBhdXRvQ29udHJvbHNDb21iaW5lOiBmYWxzZSxcbiAgICBhdXRvQ29udHJvbHNTZWxlY3RvcjogbnVsbCxcblxuICAgIC8vIEFVVE9cbiAgICBhdXRvOiBmYWxzZSxcbiAgICBwYXVzZTogNDAwMCxcbiAgICBhdXRvU3RhcnQ6IHRydWUsXG4gICAgYXV0b0RpcmVjdGlvbjogJ25leHQnLFxuICAgIHN0b3BBdXRvT25DbGljazogZmFsc2UsXG4gICAgYXV0b0hvdmVyOiBmYWxzZSxcbiAgICBhdXRvRGVsYXk6IDAsXG4gICAgYXV0b1NsaWRlRm9yT25lUGFnZTogZmFsc2UsXG5cbiAgICAvLyBDQVJPVVNFTFxuICAgIG1pblNsaWRlczogMSxcbiAgICBtYXhTbGlkZXM6IDEsXG4gICAgbW92ZVNsaWRlczogMCxcbiAgICBzbGlkZVdpZHRoOiAwLFxuICAgIHNocmlua0l0ZW1zOiBmYWxzZSxcblxuICAgIC8vIENBTExCQUNLU1xuICAgIG9uU2xpZGVyTG9hZDogZnVuY3Rpb24oKSB7IHJldHVybiB0cnVlOyB9LFxuICAgIG9uU2xpZGVCZWZvcmU6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdHJ1ZTsgfSxcbiAgICBvblNsaWRlQWZ0ZXI6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdHJ1ZTsgfSxcbiAgICBvblNsaWRlTmV4dDogZnVuY3Rpb24oKSB7IHJldHVybiB0cnVlOyB9LFxuICAgIG9uU2xpZGVQcmV2OiBmdW5jdGlvbigpIHsgcmV0dXJuIHRydWU7IH0sXG4gICAgb25TbGlkZXJSZXNpemU6IGZ1bmN0aW9uKCkgeyByZXR1cm4gdHJ1ZTsgfSxcblx0b25BdXRvQ2hhbmdlOiBmdW5jdGlvbigpIHsgcmV0dXJuIHRydWU7IH0gLy9jYWxscyB3aGVuIGF1dG8gc2xpZGVzIHN0YXJ0cyBhbmQgc3RvcHNcbiAgfTtcblxuICAkLmZuLmJ4U2xpZGVyID0gZnVuY3Rpb24ob3B0aW9ucykge1xuXG4gICAgaWYgKHRoaXMubGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvLyBzdXBwb3J0IG11bHRpcGxlIGVsZW1lbnRzXG4gICAgaWYgKHRoaXMubGVuZ3RoID4gMSkge1xuICAgICAgdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpLmJ4U2xpZGVyKG9wdGlvbnMpO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvLyBjcmVhdGUgYSBuYW1lc3BhY2UgdG8gYmUgdXNlZCB0aHJvdWdob3V0IHRoZSBwbHVnaW5cbiAgICB2YXIgc2xpZGVyID0ge30sXG4gICAgLy8gc2V0IGEgcmVmZXJlbmNlIHRvIG91ciBzbGlkZXIgZWxlbWVudFxuICAgIGVsID0gdGhpcyxcbiAgICAvLyBnZXQgdGhlIG9yaWdpbmFsIHdpbmRvdyBkaW1lbnMgKHRoYW5rcyBhIGxvdCBJRSlcbiAgICB3aW5kb3dXaWR0aCA9ICQod2luZG93KS53aWR0aCgpLFxuICAgIHdpbmRvd0hlaWdodCA9ICQod2luZG93KS5oZWlnaHQoKTtcblxuICAgIC8vIFJldHVybiBpZiBzbGlkZXIgaXMgYWxyZWFkeSBpbml0aWFsaXplZFxuICAgIGlmICgkKGVsKS5kYXRhKCdieFNsaWRlcicpKSB7IHJldHVybjsgfVxuXG4gICAgLyoqXG4gICAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAgKiA9IFBSSVZBVEUgRlVOQ1RJT05TXG4gICAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemVzIG5hbWVzcGFjZSBzZXR0aW5ncyB0byBiZSB1c2VkIHRocm91Z2hvdXQgcGx1Z2luXG4gICAgICovXG4gICAgdmFyIGluaXQgPSBmdW5jdGlvbigpIHtcbiAgICAgIC8vIFJldHVybiBpZiBzbGlkZXIgaXMgYWxyZWFkeSBpbml0aWFsaXplZFxuICAgICAgaWYgKCQoZWwpLmRhdGEoJ2J4U2xpZGVyJykpIHsgcmV0dXJuOyB9XG4gICAgICAvLyBtZXJnZSB1c2VyLXN1cHBsaWVkIG9wdGlvbnMgd2l0aCB0aGUgZGVmYXVsdHNcbiAgICAgIHNsaWRlci5zZXR0aW5ncyA9ICQuZXh0ZW5kKHt9LCBkZWZhdWx0cywgb3B0aW9ucyk7XG4gICAgICAvLyBwYXJzZSBzbGlkZVdpZHRoIHNldHRpbmdcbiAgICAgIHNsaWRlci5zZXR0aW5ncy5zbGlkZVdpZHRoID0gcGFyc2VJbnQoc2xpZGVyLnNldHRpbmdzLnNsaWRlV2lkdGgpO1xuICAgICAgLy8gc3RvcmUgdGhlIG9yaWdpbmFsIGNoaWxkcmVuXG4gICAgICBzbGlkZXIuY2hpbGRyZW4gPSBlbC5jaGlsZHJlbihzbGlkZXIuc2V0dGluZ3Muc2xpZGVTZWxlY3Rvcik7XG4gICAgICAvLyBjaGVjayBpZiBhY3R1YWwgbnVtYmVyIG9mIHNsaWRlcyBpcyBsZXNzIHRoYW4gbWluU2xpZGVzIC8gbWF4U2xpZGVzXG4gICAgICBpZiAoc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCA8IHNsaWRlci5zZXR0aW5ncy5taW5TbGlkZXMpIHsgc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcyA9IHNsaWRlci5jaGlsZHJlbi5sZW5ndGg7IH1cbiAgICAgIGlmIChzbGlkZXIuY2hpbGRyZW4ubGVuZ3RoIDwgc2xpZGVyLnNldHRpbmdzLm1heFNsaWRlcykgeyBzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzID0gc2xpZGVyLmNoaWxkcmVuLmxlbmd0aDsgfVxuICAgICAgLy8gaWYgcmFuZG9tIHN0YXJ0LCBzZXQgdGhlIHN0YXJ0U2xpZGUgc2V0dGluZyB0byByYW5kb20gbnVtYmVyXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLnJhbmRvbVN0YXJ0KSB7IHNsaWRlci5zZXR0aW5ncy5zdGFydFNsaWRlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCk7IH1cbiAgICAgIC8vIHN0b3JlIGFjdGl2ZSBzbGlkZSBpbmZvcm1hdGlvblxuICAgICAgc2xpZGVyLmFjdGl2ZSA9IHsgaW5kZXg6IHNsaWRlci5zZXR0aW5ncy5zdGFydFNsaWRlIH07XG4gICAgICAvLyBzdG9yZSBpZiB0aGUgc2xpZGVyIGlzIGluIGNhcm91c2VsIG1vZGUgKGRpc3BsYXlpbmcgLyBtb3ZpbmcgbXVsdGlwbGUgc2xpZGVzKVxuICAgICAgc2xpZGVyLmNhcm91c2VsID0gc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcyA+IDEgfHwgc2xpZGVyLnNldHRpbmdzLm1heFNsaWRlcyA+IDEgPyB0cnVlIDogZmFsc2U7XG4gICAgICAvLyBpZiBjYXJvdXNlbCwgZm9yY2UgcHJlbG9hZEltYWdlcyA9ICdhbGwnXG4gICAgICBpZiAoc2xpZGVyLmNhcm91c2VsKSB7IHNsaWRlci5zZXR0aW5ncy5wcmVsb2FkSW1hZ2VzID0gJ2FsbCc7IH1cbiAgICAgIC8vIGNhbGN1bGF0ZSB0aGUgbWluIC8gbWF4IHdpZHRoIHRocmVzaG9sZHMgYmFzZWQgb24gbWluIC8gbWF4IG51bWJlciBvZiBzbGlkZXNcbiAgICAgIC8vIHVzZWQgdG8gc2V0dXAgYW5kIHVwZGF0ZSBjYXJvdXNlbCBzbGlkZXMgZGltZW5zaW9uc1xuICAgICAgc2xpZGVyLm1pblRocmVzaG9sZCA9IChzbGlkZXIuc2V0dGluZ3MubWluU2xpZGVzICogc2xpZGVyLnNldHRpbmdzLnNsaWRlV2lkdGgpICsgKChzbGlkZXIuc2V0dGluZ3MubWluU2xpZGVzIC0gMSkgKiBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4pO1xuICAgICAgc2xpZGVyLm1heFRocmVzaG9sZCA9IChzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzICogc2xpZGVyLnNldHRpbmdzLnNsaWRlV2lkdGgpICsgKChzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzIC0gMSkgKiBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4pO1xuICAgICAgLy8gc3RvcmUgdGhlIGN1cnJlbnQgc3RhdGUgb2YgdGhlIHNsaWRlciAoaWYgY3VycmVudGx5IGFuaW1hdGluZywgd29ya2luZyBpcyB0cnVlKVxuICAgICAgc2xpZGVyLndvcmtpbmcgPSBmYWxzZTtcbiAgICAgIC8vIGluaXRpYWxpemUgdGhlIGNvbnRyb2xzIG9iamVjdFxuICAgICAgc2xpZGVyLmNvbnRyb2xzID0ge307XG4gICAgICAvLyBpbml0aWFsaXplIGFuIGF1dG8gaW50ZXJ2YWxcbiAgICAgIHNsaWRlci5pbnRlcnZhbCA9IG51bGw7XG4gICAgICAvLyBkZXRlcm1pbmUgd2hpY2ggcHJvcGVydHkgdG8gdXNlIGZvciB0cmFuc2l0aW9uc1xuICAgICAgc2xpZGVyLmFuaW1Qcm9wID0gc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICd2ZXJ0aWNhbCcgPyAndG9wJyA6ICdsZWZ0JztcbiAgICAgIC8vIGRldGVybWluZSBpZiBoYXJkd2FyZSBhY2NlbGVyYXRpb24gY2FuIGJlIHVzZWRcbiAgICAgIHNsaWRlci51c2luZ0NTUyA9IHNsaWRlci5zZXR0aW5ncy51c2VDU1MgJiYgc2xpZGVyLnNldHRpbmdzLm1vZGUgIT09ICdmYWRlJyAmJiAoZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vIGNyZWF0ZSBvdXIgdGVzdCBkaXYgZWxlbWVudFxuICAgICAgICB2YXIgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JyksXG4gICAgICAgIC8vIGNzcyB0cmFuc2l0aW9uIHByb3BlcnRpZXNcbiAgICAgICAgcHJvcHMgPSBbJ1dlYmtpdFBlcnNwZWN0aXZlJywgJ01velBlcnNwZWN0aXZlJywgJ09QZXJzcGVjdGl2ZScsICdtc1BlcnNwZWN0aXZlJ107XG4gICAgICAgIC8vIHRlc3QgZm9yIGVhY2ggcHJvcGVydHlcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIGlmIChkaXYuc3R5bGVbcHJvcHNbaV1dICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHNsaWRlci5jc3NQcmVmaXggPSBwcm9wc1tpXS5yZXBsYWNlKCdQZXJzcGVjdGl2ZScsICcnKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgc2xpZGVyLmFuaW1Qcm9wID0gJy0nICsgc2xpZGVyLmNzc1ByZWZpeCArICctdHJhbnNmb3JtJztcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9KCkpO1xuICAgICAgLy8gaWYgdmVydGljYWwgbW9kZSBhbHdheXMgbWFrZSBtYXhTbGlkZXMgYW5kIG1pblNsaWRlcyBlcXVhbFxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAndmVydGljYWwnKSB7IHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXMgPSBzbGlkZXIuc2V0dGluZ3MubWluU2xpZGVzOyB9XG4gICAgICAvLyBzYXZlIG9yaWdpbmFsIHN0eWxlIGRhdGFcbiAgICAgIGVsLmRhdGEoJ29yaWdTdHlsZScsIGVsLmF0dHIoJ3N0eWxlJykpO1xuICAgICAgZWwuY2hpbGRyZW4oc2xpZGVyLnNldHRpbmdzLnNsaWRlU2VsZWN0b3IpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICQodGhpcykuZGF0YSgnb3JpZ1N0eWxlJywgJCh0aGlzKS5hdHRyKCdzdHlsZScpKTtcbiAgICAgIH0pO1xuXG4gICAgICAvLyBwZXJmb3JtIGFsbCBET00gLyBDU1MgbW9kaWZpY2F0aW9uc1xuICAgICAgc2V0dXAoKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogUGVyZm9ybXMgYWxsIERPTSBhbmQgQ1NTIG1vZGlmaWNhdGlvbnNcbiAgICAgKi9cbiAgICB2YXIgc2V0dXAgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBwcmVsb2FkU2VsZWN0b3IgPSBzbGlkZXIuY2hpbGRyZW4uZXEoc2xpZGVyLnNldHRpbmdzLnN0YXJ0U2xpZGUpOyAvLyBzZXQgdGhlIGRlZmF1bHQgcHJlbG9hZCBzZWxlY3RvciAodmlzaWJsZSlcblxuICAgICAgLy8gd3JhcCBlbCBpbiBhIHdyYXBwZXJcbiAgICAgIGVsLndyYXAoJzxkaXYgY2xhc3M9XCInICsgc2xpZGVyLnNldHRpbmdzLndyYXBwZXJDbGFzcyArICdcIj48ZGl2IGNsYXNzPVwiYngtdmlld3BvcnRcIj48L2Rpdj48L2Rpdj4nKTtcbiAgICAgIC8vIHN0b3JlIGEgbmFtZXNwYWNlIHJlZmVyZW5jZSB0byAuYngtdmlld3BvcnRcbiAgICAgIHNsaWRlci52aWV3cG9ydCA9IGVsLnBhcmVudCgpO1xuXG4gICAgICAvLyBhZGQgYXJpYS1saXZlIGlmIHRoZSBzZXR0aW5nIGlzIGVuYWJsZWQgYW5kIHRpY2tlciBtb2RlIGlzIGRpc2FibGVkXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmFyaWFMaXZlICYmICFzbGlkZXIuc2V0dGluZ3MudGlja2VyKSB7XG4gICAgICAgIHNsaWRlci52aWV3cG9ydC5hdHRyKCdhcmlhLWxpdmUnLCAncG9saXRlJyk7XG4gICAgICB9XG4gICAgICAvLyBhZGQgYSBsb2FkaW5nIGRpdiB0byBkaXNwbGF5IHdoaWxlIGltYWdlcyBhcmUgbG9hZGluZ1xuICAgICAgc2xpZGVyLmxvYWRlciA9ICQoJzxkaXYgY2xhc3M9XCJieC1sb2FkaW5nXCIgLz4nKTtcbiAgICAgIHNsaWRlci52aWV3cG9ydC5wcmVwZW5kKHNsaWRlci5sb2FkZXIpO1xuICAgICAgLy8gc2V0IGVsIHRvIGEgbWFzc2l2ZSB3aWR0aCwgdG8gaG9sZCBhbnkgbmVlZGVkIHNsaWRlc1xuICAgICAgLy8gYWxzbyBzdHJpcCBhbnkgbWFyZ2luIGFuZCBwYWRkaW5nIGZyb20gZWxcbiAgICAgIGVsLmNzcyh7XG4gICAgICAgIHdpZHRoOiBzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2hvcml6b250YWwnID8gKHNsaWRlci5jaGlsZHJlbi5sZW5ndGggKiAxMDAwICsgMjE1KSArICclJyA6ICdhdXRvJyxcbiAgICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZSdcbiAgICAgIH0pO1xuICAgICAgLy8gaWYgdXNpbmcgQ1NTLCBhZGQgdGhlIGVhc2luZyBwcm9wZXJ0eVxuICAgICAgaWYgKHNsaWRlci51c2luZ0NTUyAmJiBzbGlkZXIuc2V0dGluZ3MuZWFzaW5nKSB7XG4gICAgICAgIGVsLmNzcygnLScgKyBzbGlkZXIuY3NzUHJlZml4ICsgJy10cmFuc2l0aW9uLXRpbWluZy1mdW5jdGlvbicsIHNsaWRlci5zZXR0aW5ncy5lYXNpbmcpO1xuICAgICAgLy8gaWYgbm90IHVzaW5nIENTUyBhbmQgbm8gZWFzaW5nIHZhbHVlIHdhcyBzdXBwbGllZCwgdXNlIHRoZSBkZWZhdWx0IEpTIGFuaW1hdGlvbiBlYXNpbmcgKHN3aW5nKVxuICAgICAgfSBlbHNlIGlmICghc2xpZGVyLnNldHRpbmdzLmVhc2luZykge1xuICAgICAgICBzbGlkZXIuc2V0dGluZ3MuZWFzaW5nID0gJ3N3aW5nJztcbiAgICAgIH1cbiAgICAgIC8vIG1ha2UgbW9kaWZpY2F0aW9ucyB0byB0aGUgdmlld3BvcnQgKC5ieC12aWV3cG9ydClcbiAgICAgIHNsaWRlci52aWV3cG9ydC5jc3Moe1xuICAgICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnXG4gICAgICB9KTtcbiAgICAgIHNsaWRlci52aWV3cG9ydC5wYXJlbnQoKS5jc3Moe1xuICAgICAgICBtYXhXaWR0aDogZ2V0Vmlld3BvcnRNYXhXaWR0aCgpXG4gICAgICB9KTtcbiAgICAgIC8vIGFwcGx5IGNzcyB0byBhbGwgc2xpZGVyIGNoaWxkcmVuXG4gICAgICBzbGlkZXIuY2hpbGRyZW4uY3NzKHtcbiAgICAgICAgLy8gdGhlIGZsb2F0IGF0dHJpYnV0ZSBpcyBhIHJlc2VydmVkIHdvcmQgaW4gY29tcHJlc3NvcnMgbGlrZSBZVUkgY29tcHJlc3NvciBhbmQgbmVlZCB0byBiZSBxdW90ZWQgIzQ4XG4gICAgICAgICdmbG9hdCc6IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgPyAnbGVmdCcgOiAnbm9uZScsXG4gICAgICAgIGxpc3RTdHlsZTogJ25vbmUnLFxuICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJ1xuICAgICAgfSk7XG4gICAgICAvLyBhcHBseSB0aGUgY2FsY3VsYXRlZCB3aWR0aCBhZnRlciB0aGUgZmxvYXQgaXMgYXBwbGllZCB0byBwcmV2ZW50IHNjcm9sbGJhciBpbnRlcmZlcmVuY2VcbiAgICAgIHNsaWRlci5jaGlsZHJlbi5jc3MoJ3dpZHRoJywgZ2V0U2xpZGVXaWR0aCgpKTtcbiAgICAgIC8vIGlmIHNsaWRlTWFyZ2luIGlzIHN1cHBsaWVkLCBhZGQgdGhlIGNzc1xuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgJiYgc2xpZGVyLnNldHRpbmdzLnNsaWRlTWFyZ2luID4gMCkgeyBzbGlkZXIuY2hpbGRyZW4uY3NzKCdtYXJnaW5SaWdodCcsIHNsaWRlci5zZXR0aW5ncy5zbGlkZU1hcmdpbik7IH1cbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJyAmJiBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4gPiAwKSB7IHNsaWRlci5jaGlsZHJlbi5jc3MoJ21hcmdpbkJvdHRvbScsIHNsaWRlci5zZXR0aW5ncy5zbGlkZU1hcmdpbik7IH1cbiAgICAgIC8vIGlmIFwiZmFkZVwiIG1vZGUsIGFkZCBwb3NpdGlvbmluZyBhbmQgei1pbmRleCBDU1NcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2ZhZGUnKSB7XG4gICAgICAgIHNsaWRlci5jaGlsZHJlbi5jc3Moe1xuICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgICAgIHpJbmRleDogMCxcbiAgICAgICAgICBkaXNwbGF5OiAnbm9uZSdcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIHByZXBhcmUgdGhlIHotaW5kZXggb24gdGhlIHNob3dpbmcgZWxlbWVudFxuICAgICAgICBzbGlkZXIuY2hpbGRyZW4uZXEoc2xpZGVyLnNldHRpbmdzLnN0YXJ0U2xpZGUpLmNzcyh7ekluZGV4OiBzbGlkZXIuc2V0dGluZ3Muc2xpZGVaSW5kZXgsIGRpc3BsYXk6ICdibG9jayd9KTtcbiAgICAgIH1cbiAgICAgIC8vIGNyZWF0ZSBhbiBlbGVtZW50IHRvIGNvbnRhaW4gYWxsIHNsaWRlciBjb250cm9scyAocGFnZXIsIHN0YXJ0IC8gc3RvcCwgZXRjKVxuICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsID0gJCgnPGRpdiBjbGFzcz1cImJ4LWNvbnRyb2xzXCIgLz4nKTtcbiAgICAgIC8vIGlmIGNhcHRpb25zIGFyZSByZXF1ZXN0ZWQsIGFkZCB0aGVtXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmNhcHRpb25zKSB7IGFwcGVuZENhcHRpb25zKCk7IH1cbiAgICAgIC8vIGNoZWNrIGlmIHN0YXJ0U2xpZGUgaXMgbGFzdCBzbGlkZVxuICAgICAgc2xpZGVyLmFjdGl2ZS5sYXN0ID0gc2xpZGVyLnNldHRpbmdzLnN0YXJ0U2xpZGUgPT09IGdldFBhZ2VyUXR5KCkgLSAxO1xuICAgICAgLy8gaWYgdmlkZW8gaXMgdHJ1ZSwgc2V0IHVwIHRoZSBmaXRWaWRzIHBsdWdpblxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy52aWRlbykgeyBlbC5maXRWaWRzKCk7IH1cbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MucHJlbG9hZEltYWdlcyA9PT0gJ2FsbCcgfHwgc2xpZGVyLnNldHRpbmdzLnRpY2tlcikgeyBwcmVsb2FkU2VsZWN0b3IgPSBzbGlkZXIuY2hpbGRyZW47IH1cbiAgICAgIC8vIG9ubHkgY2hlY2sgZm9yIGNvbnRyb2wgYWRkaXRpb24gaWYgbm90IGluIFwidGlja2VyXCIgbW9kZVxuICAgICAgaWYgKCFzbGlkZXIuc2V0dGluZ3MudGlja2VyKSB7XG4gICAgICAgIC8vIGlmIGNvbnRyb2xzIGFyZSByZXF1ZXN0ZWQsIGFkZCB0aGVtXG4gICAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuY29udHJvbHMpIHsgYXBwZW5kQ29udHJvbHMoKTsgfVxuICAgICAgICAvLyBpZiBhdXRvIGlzIHRydWUsIGFuZCBhdXRvIGNvbnRyb2xzIGFyZSByZXF1ZXN0ZWQsIGFkZCB0aGVtXG4gICAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYXV0byAmJiBzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzKSB7IGFwcGVuZENvbnRyb2xzQXV0bygpOyB9XG4gICAgICAgIC8vIGlmIHBhZ2VyIGlzIHJlcXVlc3RlZCwgYWRkIGl0XG4gICAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MucGFnZXIpIHsgYXBwZW5kUGFnZXIoKTsgfVxuICAgICAgICAvLyBpZiBhbnkgY29udHJvbCBvcHRpb24gaXMgcmVxdWVzdGVkLCBhZGQgdGhlIGNvbnRyb2xzIHdyYXBwZXJcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5jb250cm9scyB8fCBzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzIHx8IHNsaWRlci5zZXR0aW5ncy5wYWdlcikgeyBzbGlkZXIudmlld3BvcnQuYWZ0ZXIoc2xpZGVyLmNvbnRyb2xzLmVsKTsgfVxuICAgICAgLy8gaWYgdGlja2VyIG1vZGUsIGRvIG5vdCBhbGxvdyBhIHBhZ2VyXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzbGlkZXIuc2V0dGluZ3MucGFnZXIgPSBmYWxzZTtcbiAgICAgIH1cbiAgICAgIGxvYWRFbGVtZW50cyhwcmVsb2FkU2VsZWN0b3IsIHN0YXJ0KTtcbiAgICB9O1xuXG4gICAgdmFyIGxvYWRFbGVtZW50cyA9IGZ1bmN0aW9uKHNlbGVjdG9yLCBjYWxsYmFjaykge1xuICAgICAgdmFyIHRvdGFsID0gc2VsZWN0b3IuZmluZCgnaW1nOm5vdChbc3JjPVwiXCJdKSwgaWZyYW1lJykubGVuZ3RoLFxuICAgICAgY291bnQgPSAwO1xuICAgICAgaWYgKHRvdGFsID09PSAwKSB7XG4gICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHNlbGVjdG9yLmZpbmQoJ2ltZzpub3QoW3NyYz1cIlwiXSksIGlmcmFtZScpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICQodGhpcykub25lKCdsb2FkIGVycm9yJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgaWYgKCsrY291bnQgPT09IHRvdGFsKSB7IGNhbGxiYWNrKCk7IH1cbiAgICAgICAgfSkuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICBpZiAodGhpcy5jb21wbGV0ZSB8fCB0aGlzLnNyYyA9PSAnJykgeyAkKHRoaXMpLnRyaWdnZXIoJ2xvYWQnKTsgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBTdGFydCB0aGUgc2xpZGVyXG4gICAgICovXG4gICAgdmFyIHN0YXJ0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAvLyBpZiBpbmZpbml0ZSBsb29wLCBwcmVwYXJlIGFkZGl0aW9uYWwgc2xpZGVzXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmluZmluaXRlTG9vcCAmJiBzbGlkZXIuc2V0dGluZ3MubW9kZSAhPT0gJ2ZhZGUnICYmICFzbGlkZXIuc2V0dGluZ3MudGlja2VyKSB7XG4gICAgICAgIHZhciBzbGljZSAgICA9IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAndmVydGljYWwnID8gc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcyA6IHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXMsXG4gICAgICAgIHNsaWNlQXBwZW5kICA9IHNsaWRlci5jaGlsZHJlbi5zbGljZSgwLCBzbGljZSkuY2xvbmUodHJ1ZSkuYWRkQ2xhc3MoJ2J4LWNsb25lJyksXG4gICAgICAgIHNsaWNlUHJlcGVuZCA9IHNsaWRlci5jaGlsZHJlbi5zbGljZSgtc2xpY2UpLmNsb25lKHRydWUpLmFkZENsYXNzKCdieC1jbG9uZScpO1xuICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmFyaWFIaWRkZW4pIHtcbiAgICAgICAgICBzbGljZUFwcGVuZC5hdHRyKCdhcmlhLWhpZGRlbicsIHRydWUpO1xuICAgICAgICAgIHNsaWNlUHJlcGVuZC5hdHRyKCdhcmlhLWhpZGRlbicsIHRydWUpO1xuICAgICAgICB9XG4gICAgICAgIGVsLmFwcGVuZChzbGljZUFwcGVuZCkucHJlcGVuZChzbGljZVByZXBlbmQpO1xuICAgICAgfVxuICAgICAgLy8gcmVtb3ZlIHRoZSBsb2FkaW5nIERPTSBlbGVtZW50XG4gICAgICBzbGlkZXIubG9hZGVyLnJlbW92ZSgpO1xuICAgICAgLy8gc2V0IHRoZSBsZWZ0IC8gdG9wIHBvc2l0aW9uIG9mIFwiZWxcIlxuICAgICAgc2V0U2xpZGVQb3NpdGlvbigpO1xuICAgICAgLy8gaWYgXCJ2ZXJ0aWNhbFwiIG1vZGUsIGFsd2F5cyB1c2UgYWRhcHRpdmVIZWlnaHQgdG8gcHJldmVudCBvZGQgYmVoYXZpb3JcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJykgeyBzbGlkZXIuc2V0dGluZ3MuYWRhcHRpdmVIZWlnaHQgPSB0cnVlOyB9XG4gICAgICAvLyBzZXQgdGhlIHZpZXdwb3J0IGhlaWdodFxuICAgICAgc2xpZGVyLnZpZXdwb3J0LmhlaWdodChnZXRWaWV3cG9ydEhlaWdodCgpKTtcbiAgICAgIC8vIG1ha2Ugc3VyZSBldmVyeXRoaW5nIGlzIHBvc2l0aW9uZWQganVzdCByaWdodCAoc2FtZSBhcyBhIHdpbmRvdyByZXNpemUpXG4gICAgICBlbC5yZWRyYXdTbGlkZXIoKTtcbiAgICAgIC8vIG9uU2xpZGVyTG9hZCBjYWxsYmFja1xuICAgICAgc2xpZGVyLnNldHRpbmdzLm9uU2xpZGVyTG9hZC5jYWxsKGVsLCBzbGlkZXIuYWN0aXZlLmluZGV4KTtcbiAgICAgIC8vIHNsaWRlciBoYXMgYmVlbiBmdWxseSBpbml0aWFsaXplZFxuICAgICAgc2xpZGVyLmluaXRpYWxpemVkID0gdHJ1ZTtcbiAgICAgIC8vIGJpbmQgdGhlIHJlc2l6ZSBjYWxsIHRvIHRoZSB3aW5kb3dcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MucmVzcG9uc2l2ZSkgeyAkKHdpbmRvdykuYmluZCgncmVzaXplJywgcmVzaXplV2luZG93KTsgfVxuICAgICAgLy8gaWYgYXV0byBpcyB0cnVlIGFuZCBoYXMgbW9yZSB0aGFuIDEgcGFnZSwgc3RhcnQgdGhlIHNob3dcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYXV0byAmJiBzbGlkZXIuc2V0dGluZ3MuYXV0b1N0YXJ0ICYmIChnZXRQYWdlclF0eSgpID4gMSB8fCBzbGlkZXIuc2V0dGluZ3MuYXV0b1NsaWRlRm9yT25lUGFnZSkpIHsgaW5pdEF1dG8oKTsgfVxuICAgICAgLy8gaWYgdGlja2VyIGlzIHRydWUsIHN0YXJ0IHRoZSB0aWNrZXJcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MudGlja2VyKSB7IGluaXRUaWNrZXIoKTsgfVxuICAgICAgLy8gaWYgcGFnZXIgaXMgcmVxdWVzdGVkLCBtYWtlIHRoZSBhcHByb3ByaWF0ZSBwYWdlciBsaW5rIGFjdGl2ZVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5wYWdlcikgeyB1cGRhdGVQYWdlckFjdGl2ZShzbGlkZXIuc2V0dGluZ3Muc3RhcnRTbGlkZSk7IH1cbiAgICAgIC8vIGNoZWNrIGZvciBhbnkgdXBkYXRlcyB0byB0aGUgY29udHJvbHMgKGxpa2UgaGlkZUNvbnRyb2xPbkVuZCB1cGRhdGVzKVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5jb250cm9scykgeyB1cGRhdGVEaXJlY3Rpb25Db250cm9scygpOyB9XG4gICAgICAvLyBpZiB0b3VjaEVuYWJsZWQgaXMgdHJ1ZSwgc2V0dXAgdGhlIHRvdWNoIGV2ZW50c1xuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy50b3VjaEVuYWJsZWQgJiYgIXNsaWRlci5zZXR0aW5ncy50aWNrZXIpIHsgaW5pdFRvdWNoKCk7IH1cbiAgICAgIC8vIGlmIGtleWJvYXJkRW5hYmxlZCBpcyB0cnVlLCBzZXR1cCB0aGUga2V5Ym9hcmQgZXZlbnRzXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmtleWJvYXJkRW5hYmxlZCAmJiAhc2xpZGVyLnNldHRpbmdzLnRpY2tlcikge1xuICAgICAgICAkKGRvY3VtZW50KS5rZXlkb3duKGtleVByZXNzKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY2FsY3VsYXRlZCBoZWlnaHQgb2YgdGhlIHZpZXdwb3J0LCB1c2VkIHRvIGRldGVybWluZSBlaXRoZXIgYWRhcHRpdmVIZWlnaHQgb3IgdGhlIG1heEhlaWdodCB2YWx1ZVxuICAgICAqL1xuICAgIHZhciBnZXRWaWV3cG9ydEhlaWdodCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGhlaWdodCA9IDA7XG4gICAgICAvLyBmaXJzdCBkZXRlcm1pbmUgd2hpY2ggY2hpbGRyZW4gKHNsaWRlcykgc2hvdWxkIGJlIHVzZWQgaW4gb3VyIGhlaWdodCBjYWxjdWxhdGlvblxuICAgICAgdmFyIGNoaWxkcmVuID0gJCgpO1xuICAgICAgLy8gaWYgbW9kZSBpcyBub3QgXCJ2ZXJ0aWNhbFwiIGFuZCBhZGFwdGl2ZUhlaWdodCBpcyBmYWxzZSwgaW5jbHVkZSBhbGwgY2hpbGRyZW5cbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSAhPT0gJ3ZlcnRpY2FsJyAmJiAhc2xpZGVyLnNldHRpbmdzLmFkYXB0aXZlSGVpZ2h0KSB7XG4gICAgICAgIGNoaWxkcmVuID0gc2xpZGVyLmNoaWxkcmVuO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gaWYgbm90IGNhcm91c2VsLCByZXR1cm4gdGhlIHNpbmdsZSBhY3RpdmUgY2hpbGRcbiAgICAgICAgaWYgKCFzbGlkZXIuY2Fyb3VzZWwpIHtcbiAgICAgICAgICBjaGlsZHJlbiA9IHNsaWRlci5jaGlsZHJlbi5lcShzbGlkZXIuYWN0aXZlLmluZGV4KTtcbiAgICAgICAgLy8gaWYgY2Fyb3VzZWwsIHJldHVybiBhIHNsaWNlIG9mIGNoaWxkcmVuXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gZ2V0IHRoZSBpbmRpdmlkdWFsIHNsaWRlIGluZGV4XG4gICAgICAgICAgdmFyIGN1cnJlbnRJbmRleCA9IHNsaWRlci5zZXR0aW5ncy5tb3ZlU2xpZGVzID09PSAxID8gc2xpZGVyLmFjdGl2ZS5pbmRleCA6IHNsaWRlci5hY3RpdmUuaW5kZXggKiBnZXRNb3ZlQnkoKTtcbiAgICAgICAgICAvLyBhZGQgdGhlIGN1cnJlbnQgc2xpZGUgdG8gdGhlIGNoaWxkcmVuXG4gICAgICAgICAgY2hpbGRyZW4gPSBzbGlkZXIuY2hpbGRyZW4uZXEoY3VycmVudEluZGV4KTtcbiAgICAgICAgICAvLyBjeWNsZSB0aHJvdWdoIHRoZSByZW1haW5pbmcgXCJzaG93aW5nXCIgc2xpZGVzXG4gICAgICAgICAgZm9yIChpID0gMTsgaSA8PSBzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzIC0gMTsgaSsrKSB7XG4gICAgICAgICAgICAvLyBpZiBsb29wZWQgYmFjayB0byB0aGUgc3RhcnRcbiAgICAgICAgICAgIGlmIChjdXJyZW50SW5kZXggKyBpID49IHNsaWRlci5jaGlsZHJlbi5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlbi5hZGQoc2xpZGVyLmNoaWxkcmVuLmVxKGkgLSAxKSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBjaGlsZHJlbiA9IGNoaWxkcmVuLmFkZChzbGlkZXIuY2hpbGRyZW4uZXEoY3VycmVudEluZGV4ICsgaSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLy8gaWYgXCJ2ZXJ0aWNhbFwiIG1vZGUsIGNhbGN1bGF0ZSB0aGUgc3VtIG9mIHRoZSBoZWlnaHRzIG9mIHRoZSBjaGlsZHJlblxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAndmVydGljYWwnKSB7XG4gICAgICAgIGNoaWxkcmVuLmVhY2goZnVuY3Rpb24oaW5kZXgpIHtcbiAgICAgICAgICBoZWlnaHQgKz0gJCh0aGlzKS5vdXRlckhlaWdodCgpO1xuICAgICAgICB9KTtcbiAgICAgICAgLy8gYWRkIHVzZXItc3VwcGxpZWQgbWFyZ2luc1xuICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLnNsaWRlTWFyZ2luID4gMCkge1xuICAgICAgICAgIGhlaWdodCArPSBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4gKiAoc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcyAtIDEpO1xuICAgICAgICB9XG4gICAgICAvLyBpZiBub3QgXCJ2ZXJ0aWNhbFwiIG1vZGUsIGNhbGN1bGF0ZSB0aGUgbWF4IGhlaWdodCBvZiB0aGUgY2hpbGRyZW5cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGhlaWdodCA9IE1hdGgubWF4LmFwcGx5KE1hdGgsIGNoaWxkcmVuLm1hcChmdW5jdGlvbigpIHtcbiAgICAgICAgICByZXR1cm4gJCh0aGlzKS5vdXRlckhlaWdodChmYWxzZSk7XG4gICAgICAgIH0pLmdldCgpKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHNsaWRlci52aWV3cG9ydC5jc3MoJ2JveC1zaXppbmcnKSA9PT0gJ2JvcmRlci1ib3gnKSB7XG4gICAgICAgIGhlaWdodCArPSBwYXJzZUZsb2F0KHNsaWRlci52aWV3cG9ydC5jc3MoJ3BhZGRpbmctdG9wJykpICsgcGFyc2VGbG9hdChzbGlkZXIudmlld3BvcnQuY3NzKCdwYWRkaW5nLWJvdHRvbScpKSArXG4gICAgICAgICAgICAgIHBhcnNlRmxvYXQoc2xpZGVyLnZpZXdwb3J0LmNzcygnYm9yZGVyLXRvcC13aWR0aCcpKSArIHBhcnNlRmxvYXQoc2xpZGVyLnZpZXdwb3J0LmNzcygnYm9yZGVyLWJvdHRvbS13aWR0aCcpKTtcbiAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLnZpZXdwb3J0LmNzcygnYm94LXNpemluZycpID09PSAncGFkZGluZy1ib3gnKSB7XG4gICAgICAgIGhlaWdodCArPSBwYXJzZUZsb2F0KHNsaWRlci52aWV3cG9ydC5jc3MoJ3BhZGRpbmctdG9wJykpICsgcGFyc2VGbG9hdChzbGlkZXIudmlld3BvcnQuY3NzKCdwYWRkaW5nLWJvdHRvbScpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGhlaWdodDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY2FsY3VsYXRlZCB3aWR0aCB0byBiZSB1c2VkIGZvciB0aGUgb3V0ZXIgd3JhcHBlciAvIHZpZXdwb3J0XG4gICAgICovXG4gICAgdmFyIGdldFZpZXdwb3J0TWF4V2lkdGggPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciB3aWR0aCA9ICcxMDAlJztcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3Muc2xpZGVXaWR0aCA+IDApIHtcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcpIHtcbiAgICAgICAgICB3aWR0aCA9IChzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzICogc2xpZGVyLnNldHRpbmdzLnNsaWRlV2lkdGgpICsgKChzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzIC0gMSkgKiBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHdpZHRoID0gc2xpZGVyLnNldHRpbmdzLnNsaWRlV2lkdGg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiB3aWR0aDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY2FsY3VsYXRlZCB3aWR0aCB0byBiZSBhcHBsaWVkIHRvIGVhY2ggc2xpZGVcbiAgICAgKi9cbiAgICB2YXIgZ2V0U2xpZGVXaWR0aCA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIG5ld0VsV2lkdGggPSBzbGlkZXIuc2V0dGluZ3Muc2xpZGVXaWR0aCwgLy8gc3RhcnQgd2l0aCBhbnkgdXNlci1zdXBwbGllZCBzbGlkZSB3aWR0aFxuICAgICAgd3JhcFdpZHRoICAgICAgPSBzbGlkZXIudmlld3BvcnQud2lkdGgoKTsgICAgLy8gZ2V0IHRoZSBjdXJyZW50IHZpZXdwb3J0IHdpZHRoXG4gICAgICAvLyBpZiBzbGlkZSB3aWR0aCB3YXMgbm90IHN1cHBsaWVkLCBvciBpcyBsYXJnZXIgdGhhbiB0aGUgdmlld3BvcnQgdXNlIHRoZSB2aWV3cG9ydCB3aWR0aFxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5zbGlkZVdpZHRoID09PSAwIHx8XG4gICAgICAgIChzbGlkZXIuc2V0dGluZ3Muc2xpZGVXaWR0aCA+IHdyYXBXaWR0aCAmJiAhc2xpZGVyLmNhcm91c2VsKSB8fFxuICAgICAgICBzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJykge1xuICAgICAgICBuZXdFbFdpZHRoID0gd3JhcFdpZHRoO1xuICAgICAgLy8gaWYgY2Fyb3VzZWwsIHVzZSB0aGUgdGhyZXNob2xkcyB0byBkZXRlcm1pbmUgdGhlIHdpZHRoXG4gICAgICB9IGVsc2UgaWYgKHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXMgPiAxICYmIHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcpIHtcbiAgICAgICAgaWYgKHdyYXBXaWR0aCA+IHNsaWRlci5tYXhUaHJlc2hvbGQpIHtcbiAgICAgICAgICByZXR1cm4gbmV3RWxXaWR0aDtcbiAgICAgICAgfSBlbHNlIGlmICh3cmFwV2lkdGggPCBzbGlkZXIubWluVGhyZXNob2xkKSB7XG4gICAgICAgICAgbmV3RWxXaWR0aCA9ICh3cmFwV2lkdGggLSAoc2xpZGVyLnNldHRpbmdzLnNsaWRlTWFyZ2luICogKHNsaWRlci5zZXR0aW5ncy5taW5TbGlkZXMgLSAxKSkpIC8gc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcztcbiAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuc2V0dGluZ3Muc2hyaW5rSXRlbXMpIHtcbiAgICAgICAgICBuZXdFbFdpZHRoID0gTWF0aC5mbG9vcigod3JhcFdpZHRoICsgc2xpZGVyLnNldHRpbmdzLnNsaWRlTWFyZ2luKSAvIChNYXRoLmNlaWwoKHdyYXBXaWR0aCArIHNsaWRlci5zZXR0aW5ncy5zbGlkZU1hcmdpbikgLyAobmV3RWxXaWR0aCArIHNsaWRlci5zZXR0aW5ncy5zbGlkZU1hcmdpbikpKSAtIHNsaWRlci5zZXR0aW5ncy5zbGlkZU1hcmdpbik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBuZXdFbFdpZHRoO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBudW1iZXIgb2Ygc2xpZGVzIGN1cnJlbnRseSB2aXNpYmxlIGluIHRoZSB2aWV3cG9ydCAoaW5jbHVkZXMgcGFydGlhbGx5IHZpc2libGUgc2xpZGVzKVxuICAgICAqL1xuICAgIHZhciBnZXROdW1iZXJTbGlkZXNTaG93aW5nID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc2xpZGVzU2hvd2luZyA9IDEsXG4gICAgICBjaGlsZFdpZHRoID0gbnVsbDtcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2hvcml6b250YWwnICYmIHNsaWRlci5zZXR0aW5ncy5zbGlkZVdpZHRoID4gMCkge1xuICAgICAgICAvLyBpZiB2aWV3cG9ydCBpcyBzbWFsbGVyIHRoYW4gbWluVGhyZXNob2xkLCByZXR1cm4gbWluU2xpZGVzXG4gICAgICAgIGlmIChzbGlkZXIudmlld3BvcnQud2lkdGgoKSA8IHNsaWRlci5taW5UaHJlc2hvbGQpIHtcbiAgICAgICAgICBzbGlkZXNTaG93aW5nID0gc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcztcbiAgICAgICAgLy8gaWYgdmlld3BvcnQgaXMgbGFyZ2VyIHRoYW4gbWF4VGhyZXNob2xkLCByZXR1cm4gbWF4U2xpZGVzXG4gICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLnZpZXdwb3J0LndpZHRoKCkgPiBzbGlkZXIubWF4VGhyZXNob2xkKSB7XG4gICAgICAgICAgc2xpZGVzU2hvd2luZyA9IHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXM7XG4gICAgICAgIC8vIGlmIHZpZXdwb3J0IGlzIGJldHdlZW4gbWluIC8gbWF4IHRocmVzaG9sZHMsIGRpdmlkZSB2aWV3cG9ydCB3aWR0aCBieSBmaXJzdCBjaGlsZCB3aWR0aFxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoaWxkV2lkdGggPSBzbGlkZXIuY2hpbGRyZW4uZmlyc3QoKS53aWR0aCgpICsgc2xpZGVyLnNldHRpbmdzLnNsaWRlTWFyZ2luO1xuICAgICAgICAgIHNsaWRlc1Nob3dpbmcgPSBNYXRoLmZsb29yKChzbGlkZXIudmlld3BvcnQud2lkdGgoKSArXG4gICAgICAgICAgICBzbGlkZXIuc2V0dGluZ3Muc2xpZGVNYXJnaW4pIC8gY2hpbGRXaWR0aCkgfHwgMTtcbiAgICAgICAgfVxuICAgICAgLy8gaWYgXCJ2ZXJ0aWNhbFwiIG1vZGUsIHNsaWRlcyBzaG93aW5nIHdpbGwgYWx3YXlzIGJlIG1pblNsaWRlc1xuICAgICAgfSBlbHNlIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJykge1xuICAgICAgICBzbGlkZXNTaG93aW5nID0gc2xpZGVyLnNldHRpbmdzLm1pblNsaWRlcztcbiAgICAgIH1cbiAgICAgIHJldHVybiBzbGlkZXNTaG93aW5nO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBudW1iZXIgb2YgcGFnZXMgKG9uZSBmdWxsIHZpZXdwb3J0IG9mIHNsaWRlcyBpcyBvbmUgXCJwYWdlXCIpXG4gICAgICovXG4gICAgdmFyIGdldFBhZ2VyUXR5ID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcGFnZXJRdHkgPSAwLFxuICAgICAgYnJlYWtQb2ludCA9IDAsXG4gICAgICBjb3VudGVyID0gMDtcbiAgICAgIC8vIGlmIG1vdmVTbGlkZXMgaXMgc3BlY2lmaWVkIGJ5IHRoZSB1c2VyXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1vdmVTbGlkZXMgPiAwKSB7XG4gICAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuaW5maW5pdGVMb29wKSB7XG4gICAgICAgICAgcGFnZXJRdHkgPSBNYXRoLmNlaWwoc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAvIGdldE1vdmVCeSgpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyB3aGVuIGJyZWFrcG9pbnQgZ29lcyBhYm92ZSBjaGlsZHJlbiBsZW5ndGgsIGNvdW50ZXIgaXMgdGhlIG51bWJlciBvZiBwYWdlc1xuICAgICAgICAgIHdoaWxlIChicmVha1BvaW50IDwgc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCkge1xuICAgICAgICAgICAgKytwYWdlclF0eTtcbiAgICAgICAgICAgIGJyZWFrUG9pbnQgPSBjb3VudGVyICsgZ2V0TnVtYmVyU2xpZGVzU2hvd2luZygpO1xuICAgICAgICAgICAgY291bnRlciArPSBzbGlkZXIuc2V0dGluZ3MubW92ZVNsaWRlcyA8PSBnZXROdW1iZXJTbGlkZXNTaG93aW5nKCkgPyBzbGlkZXIuc2V0dGluZ3MubW92ZVNsaWRlcyA6IGdldE51bWJlclNsaWRlc1Nob3dpbmcoKTtcbiAgICAgICAgICB9XG5cdFx0ICByZXR1cm4gY291bnRlcjtcbiAgICAgICAgfVxuICAgICAgLy8gaWYgbW92ZVNsaWRlcyBpcyAwIChhdXRvKSBkaXZpZGUgY2hpbGRyZW4gbGVuZ3RoIGJ5IHNpZGVzIHNob3dpbmcsIHRoZW4gcm91bmQgdXBcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBhZ2VyUXR5ID0gTWF0aC5jZWlsKHNsaWRlci5jaGlsZHJlbi5sZW5ndGggLyBnZXROdW1iZXJTbGlkZXNTaG93aW5nKCkpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHBhZ2VyUXR5O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBudW1iZXIgb2YgaW5kaXZpZHVhbCBzbGlkZXMgYnkgd2hpY2ggdG8gc2hpZnQgdGhlIHNsaWRlclxuICAgICAqL1xuICAgIHZhciBnZXRNb3ZlQnkgPSBmdW5jdGlvbigpIHtcbiAgICAgIC8vIGlmIG1vdmVTbGlkZXMgd2FzIHNldCBieSB0aGUgdXNlciBhbmQgbW92ZVNsaWRlcyBpcyBsZXNzIHRoYW4gbnVtYmVyIG9mIHNsaWRlcyBzaG93aW5nXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1vdmVTbGlkZXMgPiAwICYmIHNsaWRlci5zZXR0aW5ncy5tb3ZlU2xpZGVzIDw9IGdldE51bWJlclNsaWRlc1Nob3dpbmcoKSkge1xuICAgICAgICByZXR1cm4gc2xpZGVyLnNldHRpbmdzLm1vdmVTbGlkZXM7XG4gICAgICB9XG4gICAgICAvLyBpZiBtb3ZlU2xpZGVzIGlzIDAgKGF1dG8pXG4gICAgICByZXR1cm4gZ2V0TnVtYmVyU2xpZGVzU2hvd2luZygpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSBzbGlkZXIncyAoZWwpIGxlZnQgb3IgdG9wIHBvc2l0aW9uXG4gICAgICovXG4gICAgdmFyIHNldFNsaWRlUG9zaXRpb24gPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBwb3NpdGlvbiwgbGFzdENoaWxkLCBsYXN0U2hvd2luZ0luZGV4O1xuICAgICAgLy8gaWYgbGFzdCBzbGlkZSwgbm90IGluZmluaXRlIGxvb3AsIGFuZCBudW1iZXIgb2YgY2hpbGRyZW4gaXMgbGFyZ2VyIHRoYW4gc3BlY2lmaWVkIG1heFNsaWRlc1xuICAgICAgaWYgKHNsaWRlci5jaGlsZHJlbi5sZW5ndGggPiBzbGlkZXIuc2V0dGluZ3MubWF4U2xpZGVzICYmIHNsaWRlci5hY3RpdmUubGFzdCAmJiAhc2xpZGVyLnNldHRpbmdzLmluZmluaXRlTG9vcCkge1xuICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICdob3Jpem9udGFsJykge1xuICAgICAgICAgIC8vIGdldCB0aGUgbGFzdCBjaGlsZCdzIHBvc2l0aW9uXG4gICAgICAgICAgbGFzdENoaWxkID0gc2xpZGVyLmNoaWxkcmVuLmxhc3QoKTtcbiAgICAgICAgICBwb3NpdGlvbiA9IGxhc3RDaGlsZC5wb3NpdGlvbigpO1xuICAgICAgICAgIC8vIHNldCB0aGUgbGVmdCBwb3NpdGlvblxuICAgICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkoLShwb3NpdGlvbi5sZWZ0IC0gKHNsaWRlci52aWV3cG9ydC53aWR0aCgpIC0gbGFzdENoaWxkLm91dGVyV2lkdGgoKSkpLCAncmVzZXQnLCAwKTtcbiAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJykge1xuICAgICAgICAgIC8vIGdldCB0aGUgbGFzdCBzaG93aW5nIGluZGV4J3MgcG9zaXRpb25cbiAgICAgICAgICBsYXN0U2hvd2luZ0luZGV4ID0gc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAtIHNsaWRlci5zZXR0aW5ncy5taW5TbGlkZXM7XG4gICAgICAgICAgcG9zaXRpb24gPSBzbGlkZXIuY2hpbGRyZW4uZXEobGFzdFNob3dpbmdJbmRleCkucG9zaXRpb24oKTtcbiAgICAgICAgICAvLyBzZXQgdGhlIHRvcCBwb3NpdGlvblxuICAgICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkoLXBvc2l0aW9uLnRvcCwgJ3Jlc2V0JywgMCk7XG4gICAgICAgIH1cbiAgICAgIC8vIGlmIG5vdCBsYXN0IHNsaWRlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBnZXQgdGhlIHBvc2l0aW9uIG9mIHRoZSBmaXJzdCBzaG93aW5nIHNsaWRlXG4gICAgICAgIHBvc2l0aW9uID0gc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXggKiBnZXRNb3ZlQnkoKSkucG9zaXRpb24oKTtcbiAgICAgICAgLy8gY2hlY2sgZm9yIGxhc3Qgc2xpZGVcbiAgICAgICAgaWYgKHNsaWRlci5hY3RpdmUuaW5kZXggPT09IGdldFBhZ2VyUXR5KCkgLSAxKSB7IHNsaWRlci5hY3RpdmUubGFzdCA9IHRydWU7IH1cbiAgICAgICAgLy8gc2V0IHRoZSByZXNwZWN0aXZlIHBvc2l0aW9uXG4gICAgICAgIGlmIChwb3NpdGlvbiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcpIHsgc2V0UG9zaXRpb25Qcm9wZXJ0eSgtcG9zaXRpb24ubGVmdCwgJ3Jlc2V0JywgMCk7IH1cbiAgICAgICAgICBlbHNlIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ3ZlcnRpY2FsJykgeyBzZXRQb3NpdGlvblByb3BlcnR5KC1wb3NpdGlvbi50b3AsICdyZXNldCcsIDApOyB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgZWwncyBhbmltYXRpbmcgcHJvcGVydHkgcG9zaXRpb24gKHdoaWNoIGluIHR1cm4gd2lsbCBzb21ldGltZXMgYW5pbWF0ZSBlbCkuXG4gICAgICogSWYgdXNpbmcgQ1NTLCBzZXRzIHRoZSB0cmFuc2Zvcm0gcHJvcGVydHkuIElmIG5vdCB1c2luZyBDU1MsIHNldHMgdGhlIHRvcCAvIGxlZnQgcHJvcGVydHkuXG4gICAgICpcbiAgICAgKiBAcGFyYW0gdmFsdWUgKGludClcbiAgICAgKiAgLSB0aGUgYW5pbWF0aW5nIHByb3BlcnR5J3MgdmFsdWVcbiAgICAgKlxuICAgICAqIEBwYXJhbSB0eXBlIChzdHJpbmcpICdzbGlkZScsICdyZXNldCcsICd0aWNrZXInXG4gICAgICogIC0gdGhlIHR5cGUgb2YgaW5zdGFuY2UgZm9yIHdoaWNoIHRoZSBmdW5jdGlvbiBpcyBiZWluZ1xuICAgICAqXG4gICAgICogQHBhcmFtIGR1cmF0aW9uIChpbnQpXG4gICAgICogIC0gdGhlIGFtb3VudCBvZiB0aW1lIChpbiBtcykgdGhlIHRyYW5zaXRpb24gc2hvdWxkIG9jY3VweVxuICAgICAqXG4gICAgICogQHBhcmFtIHBhcmFtcyAoYXJyYXkpIG9wdGlvbmFsXG4gICAgICogIC0gYW4gb3B0aW9uYWwgcGFyYW1ldGVyIGNvbnRhaW5pbmcgYW55IHZhcmlhYmxlcyB0aGF0IG5lZWQgdG8gYmUgcGFzc2VkIGluXG4gICAgICovXG4gICAgdmFyIHNldFBvc2l0aW9uUHJvcGVydHkgPSBmdW5jdGlvbih2YWx1ZSwgdHlwZSwgZHVyYXRpb24sIHBhcmFtcykge1xuICAgICAgdmFyIGFuaW1hdGVPYmosIHByb3BWYWx1ZTtcbiAgICAgIC8vIHVzZSBDU1MgdHJhbnNmb3JtXG4gICAgICBpZiAoc2xpZGVyLnVzaW5nQ1NTKSB7XG4gICAgICAgIC8vIGRldGVybWluZSB0aGUgdHJhbnNsYXRlM2QgdmFsdWVcbiAgICAgICAgcHJvcFZhbHVlID0gc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICd2ZXJ0aWNhbCcgPyAndHJhbnNsYXRlM2QoMCwgJyArIHZhbHVlICsgJ3B4LCAwKScgOiAndHJhbnNsYXRlM2QoJyArIHZhbHVlICsgJ3B4LCAwLCAwKSc7XG4gICAgICAgIC8vIGFkZCB0aGUgQ1NTIHRyYW5zaXRpb24tZHVyYXRpb25cbiAgICAgICAgZWwuY3NzKCctJyArIHNsaWRlci5jc3NQcmVmaXggKyAnLXRyYW5zaXRpb24tZHVyYXRpb24nLCBkdXJhdGlvbiAvIDEwMDAgKyAncycpO1xuICAgICAgICBpZiAodHlwZSA9PT0gJ3NsaWRlJykge1xuICAgICAgICAgIC8vIHNldCB0aGUgcHJvcGVydHkgdmFsdWVcbiAgICAgICAgICBlbC5jc3Moc2xpZGVyLmFuaW1Qcm9wLCBwcm9wVmFsdWUpO1xuICAgICAgICAgIGlmIChkdXJhdGlvbiAhPT0gMCkge1xuICAgICAgICAgICAgLy8gYmluZCBhIGNhbGxiYWNrIG1ldGhvZCAtIGV4ZWN1dGVzIHdoZW4gQ1NTIHRyYW5zaXRpb24gY29tcGxldGVzXG4gICAgICAgICAgICBlbC5iaW5kKCd0cmFuc2l0aW9uZW5kIHdlYmtpdFRyYW5zaXRpb25FbmQgb1RyYW5zaXRpb25FbmQgTVNUcmFuc2l0aW9uRW5kJywgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAvL21ha2Ugc3VyZSBpdCdzIHRoZSBjb3JyZWN0IG9uZVxuICAgICAgICAgICAgICBpZiAoISQoZS50YXJnZXQpLmlzKGVsKSkgeyByZXR1cm47IH1cbiAgICAgICAgICAgICAgLy8gdW5iaW5kIHRoZSBjYWxsYmFja1xuICAgICAgICAgICAgICBlbC51bmJpbmQoJ3RyYW5zaXRpb25lbmQgd2Via2l0VHJhbnNpdGlvbkVuZCBvVHJhbnNpdGlvbkVuZCBNU1RyYW5zaXRpb25FbmQnKTtcbiAgICAgICAgICAgICAgdXBkYXRlQWZ0ZXJTbGlkZVRyYW5zaXRpb24oKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0gZWxzZSB7IC8vZHVyYXRpb24gPSAwXG4gICAgICAgICAgICB1cGRhdGVBZnRlclNsaWRlVHJhbnNpdGlvbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAncmVzZXQnKSB7XG4gICAgICAgICAgZWwuY3NzKHNsaWRlci5hbmltUHJvcCwgcHJvcFZhbHVlKTtcbiAgICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAndGlja2VyJykge1xuICAgICAgICAgIC8vIG1ha2UgdGhlIHRyYW5zaXRpb24gdXNlICdsaW5lYXInXG4gICAgICAgICAgZWwuY3NzKCctJyArIHNsaWRlci5jc3NQcmVmaXggKyAnLXRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uJywgJ2xpbmVhcicpO1xuICAgICAgICAgIGVsLmNzcyhzbGlkZXIuYW5pbVByb3AsIHByb3BWYWx1ZSk7XG4gICAgICAgICAgaWYgKGR1cmF0aW9uICE9PSAwKSB7XG4gICAgICAgICAgICBlbC5iaW5kKCd0cmFuc2l0aW9uZW5kIHdlYmtpdFRyYW5zaXRpb25FbmQgb1RyYW5zaXRpb25FbmQgTVNUcmFuc2l0aW9uRW5kJywgZnVuY3Rpb24oZSkge1xuICAgICAgICAgICAgICAvL21ha2Ugc3VyZSBpdCdzIHRoZSBjb3JyZWN0IG9uZVxuICAgICAgICAgICAgICBpZiAoISQoZS50YXJnZXQpLmlzKGVsKSkgeyByZXR1cm47IH1cbiAgICAgICAgICAgICAgLy8gdW5iaW5kIHRoZSBjYWxsYmFja1xuICAgICAgICAgICAgICBlbC51bmJpbmQoJ3RyYW5zaXRpb25lbmQgd2Via2l0VHJhbnNpdGlvbkVuZCBvVHJhbnNpdGlvbkVuZCBNU1RyYW5zaXRpb25FbmQnKTtcbiAgICAgICAgICAgICAgLy8gcmVzZXQgdGhlIHBvc2l0aW9uXG4gICAgICAgICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkocGFyYW1zLnJlc2V0VmFsdWUsICdyZXNldCcsIDApO1xuICAgICAgICAgICAgICAvLyBzdGFydCB0aGUgbG9vcCBhZ2FpblxuICAgICAgICAgICAgICB0aWNrZXJMb29wKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9IGVsc2UgeyAvL2R1cmF0aW9uID0gMFxuICAgICAgICAgICAgc2V0UG9zaXRpb25Qcm9wZXJ0eShwYXJhbXMucmVzZXRWYWx1ZSwgJ3Jlc2V0JywgMCk7XG4gICAgICAgICAgICB0aWNrZXJMb29wKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAvLyB1c2UgSlMgYW5pbWF0ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYW5pbWF0ZU9iaiA9IHt9O1xuICAgICAgICBhbmltYXRlT2JqW3NsaWRlci5hbmltUHJvcF0gPSB2YWx1ZTtcbiAgICAgICAgaWYgKHR5cGUgPT09ICdzbGlkZScpIHtcbiAgICAgICAgICBlbC5hbmltYXRlKGFuaW1hdGVPYmosIGR1cmF0aW9uLCBzbGlkZXIuc2V0dGluZ3MuZWFzaW5nLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHVwZGF0ZUFmdGVyU2xpZGVUcmFuc2l0aW9uKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ3Jlc2V0Jykge1xuICAgICAgICAgIGVsLmNzcyhzbGlkZXIuYW5pbVByb3AsIHZhbHVlKTtcbiAgICAgICAgfSBlbHNlIGlmICh0eXBlID09PSAndGlja2VyJykge1xuICAgICAgICAgIGVsLmFuaW1hdGUoYW5pbWF0ZU9iaiwgZHVyYXRpb24sICdsaW5lYXInLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkocGFyYW1zLnJlc2V0VmFsdWUsICdyZXNldCcsIDApO1xuICAgICAgICAgICAgLy8gcnVuIHRoZSByZWN1cnNpdmUgbG9vcCBhZnRlciBhbmltYXRpb25cbiAgICAgICAgICAgIHRpY2tlckxvb3AoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBQb3B1bGF0ZXMgdGhlIHBhZ2VyIHdpdGggcHJvcGVyIGFtb3VudCBvZiBwYWdlc1xuICAgICAqL1xuICAgIHZhciBwb3B1bGF0ZVBhZ2VyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcGFnZXJIdG1sID0gJycsXG4gICAgICBsaW5rQ29udGVudCA9ICcnLFxuICAgICAgcGFnZXJRdHkgPSBnZXRQYWdlclF0eSgpO1xuICAgICAgLy8gbG9vcCB0aHJvdWdoIGVhY2ggcGFnZXIgaXRlbVxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYWdlclF0eTsgaSsrKSB7XG4gICAgICAgIGxpbmtDb250ZW50ID0gJyc7XG4gICAgICAgIC8vIGlmIGEgYnVpbGRQYWdlciBmdW5jdGlvbiBpcyBzdXBwbGllZCwgdXNlIGl0IHRvIGdldCBwYWdlciBsaW5rIHZhbHVlLCBlbHNlIHVzZSBpbmRleCArIDFcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5idWlsZFBhZ2VyICYmICQuaXNGdW5jdGlvbihzbGlkZXIuc2V0dGluZ3MuYnVpbGRQYWdlcikgfHwgc2xpZGVyLnNldHRpbmdzLnBhZ2VyQ3VzdG9tKSB7XG4gICAgICAgICAgbGlua0NvbnRlbnQgPSBzbGlkZXIuc2V0dGluZ3MuYnVpbGRQYWdlcihpKTtcbiAgICAgICAgICBzbGlkZXIucGFnZXJFbC5hZGRDbGFzcygnYngtY3VzdG9tLXBhZ2VyJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbGlua0NvbnRlbnQgPSBpICsgMTtcbiAgICAgICAgICBzbGlkZXIucGFnZXJFbC5hZGRDbGFzcygnYngtZGVmYXVsdC1wYWdlcicpO1xuICAgICAgICB9XG4gICAgICAgIC8vIHZhciBsaW5rQ29udGVudCA9IHNsaWRlci5zZXR0aW5ncy5idWlsZFBhZ2VyICYmICQuaXNGdW5jdGlvbihzbGlkZXIuc2V0dGluZ3MuYnVpbGRQYWdlcikgPyBzbGlkZXIuc2V0dGluZ3MuYnVpbGRQYWdlcihpKSA6IGkgKyAxO1xuICAgICAgICAvLyBhZGQgdGhlIG1hcmt1cCB0byB0aGUgc3RyaW5nXG4gICAgICAgIHBhZ2VySHRtbCArPSAnPGRpdiBjbGFzcz1cImJ4LXBhZ2VyLWl0ZW1cIj48YSBocmVmPVwiXCIgZGF0YS1zbGlkZS1pbmRleD1cIicgKyBpICsgJ1wiIGNsYXNzPVwiYngtcGFnZXItbGlua1wiPicgKyBsaW5rQ29udGVudCArICc8L2E+PC9kaXY+JztcbiAgICAgIH1cbiAgICAgIC8vIHBvcHVsYXRlIHRoZSBwYWdlciBlbGVtZW50IHdpdGggcGFnZXIgbGlua3NcbiAgICAgIHNsaWRlci5wYWdlckVsLmh0bWwocGFnZXJIdG1sKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQXBwZW5kcyB0aGUgcGFnZXIgdG8gdGhlIGNvbnRyb2xzIGVsZW1lbnRcbiAgICAgKi9cbiAgICB2YXIgYXBwZW5kUGFnZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmICghc2xpZGVyLnNldHRpbmdzLnBhZ2VyQ3VzdG9tKSB7XG4gICAgICAgIC8vIGNyZWF0ZSB0aGUgcGFnZXIgRE9NIGVsZW1lbnRcbiAgICAgICAgc2xpZGVyLnBhZ2VyRWwgPSAkKCc8ZGl2IGNsYXNzPVwiYngtcGFnZXJcIiAvPicpO1xuICAgICAgICAvLyBpZiBhIHBhZ2VyIHNlbGVjdG9yIHdhcyBzdXBwbGllZCwgcG9wdWxhdGUgaXQgd2l0aCB0aGUgcGFnZXJcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5wYWdlclNlbGVjdG9yKSB7XG4gICAgICAgICAgJChzbGlkZXIuc2V0dGluZ3MucGFnZXJTZWxlY3RvcikuaHRtbChzbGlkZXIucGFnZXJFbCk7XG4gICAgICAgIC8vIGlmIG5vIHBhZ2VyIHNlbGVjdG9yIHdhcyBzdXBwbGllZCwgYWRkIGl0IGFmdGVyIHRoZSB3cmFwcGVyXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLmFkZENsYXNzKCdieC1oYXMtcGFnZXInKS5hcHBlbmQoc2xpZGVyLnBhZ2VyRWwpO1xuICAgICAgICB9XG4gICAgICAgIC8vIHBvcHVsYXRlIHRoZSBwYWdlclxuICAgICAgICBwb3B1bGF0ZVBhZ2VyKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzbGlkZXIucGFnZXJFbCA9ICQoc2xpZGVyLnNldHRpbmdzLnBhZ2VyQ3VzdG9tKTtcbiAgICAgIH1cbiAgICAgIC8vIGFzc2lnbiB0aGUgcGFnZXIgY2xpY2sgYmluZGluZ1xuICAgICAgc2xpZGVyLnBhZ2VyRWwub24oJ2NsaWNrIHRvdWNoZW5kJywgJ2EnLCBjbGlja1BhZ2VyQmluZCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEFwcGVuZHMgcHJldiAvIG5leHQgY29udHJvbHMgdG8gdGhlIGNvbnRyb2xzIGVsZW1lbnRcbiAgICAgKi9cbiAgICB2YXIgYXBwZW5kQ29udHJvbHMgPSBmdW5jdGlvbigpIHtcbiAgICAgIHNsaWRlci5jb250cm9scy5uZXh0ID0gJCgnPGEgY2xhc3M9XCJieC1uZXh0XCIgaHJlZj1cIlwiPicgKyBzbGlkZXIuc2V0dGluZ3MubmV4dFRleHQgKyAnPC9hPicpO1xuICAgICAgc2xpZGVyLmNvbnRyb2xzLnByZXYgPSAkKCc8YSBjbGFzcz1cImJ4LXByZXZcIiBocmVmPVwiXCI+JyArIHNsaWRlci5zZXR0aW5ncy5wcmV2VGV4dCArICc8L2E+Jyk7XG4gICAgICAvLyBiaW5kIGNsaWNrIGFjdGlvbnMgdG8gdGhlIGNvbnRyb2xzXG4gICAgICBzbGlkZXIuY29udHJvbHMubmV4dC5iaW5kKCdjbGljayB0b3VjaGVuZCcsIGNsaWNrTmV4dEJpbmQpO1xuICAgICAgc2xpZGVyLmNvbnRyb2xzLnByZXYuYmluZCgnY2xpY2sgdG91Y2hlbmQnLCBjbGlja1ByZXZCaW5kKTtcbiAgICAgIC8vIGlmIG5leHRTZWxlY3RvciB3YXMgc3VwcGxpZWQsIHBvcHVsYXRlIGl0XG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm5leHRTZWxlY3Rvcikge1xuICAgICAgICAkKHNsaWRlci5zZXR0aW5ncy5uZXh0U2VsZWN0b3IpLmFwcGVuZChzbGlkZXIuY29udHJvbHMubmV4dCk7XG4gICAgICB9XG4gICAgICAvLyBpZiBwcmV2U2VsZWN0b3Igd2FzIHN1cHBsaWVkLCBwb3B1bGF0ZSBpdFxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5wcmV2U2VsZWN0b3IpIHtcbiAgICAgICAgJChzbGlkZXIuc2V0dGluZ3MucHJldlNlbGVjdG9yKS5hcHBlbmQoc2xpZGVyLmNvbnRyb2xzLnByZXYpO1xuICAgICAgfVxuICAgICAgLy8gaWYgbm8gY3VzdG9tIHNlbGVjdG9ycyB3ZXJlIHN1cHBsaWVkXG4gICAgICBpZiAoIXNsaWRlci5zZXR0aW5ncy5uZXh0U2VsZWN0b3IgJiYgIXNsaWRlci5zZXR0aW5ncy5wcmV2U2VsZWN0b3IpIHtcbiAgICAgICAgLy8gYWRkIHRoZSBjb250cm9scyB0byB0aGUgRE9NXG4gICAgICAgIHNsaWRlci5jb250cm9scy5kaXJlY3Rpb25FbCA9ICQoJzxkaXYgY2xhc3M9XCJieC1jb250cm9scy1kaXJlY3Rpb25cIiAvPicpO1xuICAgICAgICAvLyBhZGQgdGhlIGNvbnRyb2wgZWxlbWVudHMgdG8gdGhlIGRpcmVjdGlvbkVsXG4gICAgICAgIHNsaWRlci5jb250cm9scy5kaXJlY3Rpb25FbC5hcHBlbmQoc2xpZGVyLmNvbnRyb2xzLnByZXYpLmFwcGVuZChzbGlkZXIuY29udHJvbHMubmV4dCk7XG4gICAgICAgIC8vIHNsaWRlci52aWV3cG9ydC5hcHBlbmQoc2xpZGVyLmNvbnRyb2xzLmRpcmVjdGlvbkVsKTtcbiAgICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLmFkZENsYXNzKCdieC1oYXMtY29udHJvbHMtZGlyZWN0aW9uJykuYXBwZW5kKHNsaWRlci5jb250cm9scy5kaXJlY3Rpb25FbCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEFwcGVuZHMgc3RhcnQgLyBzdG9wIGF1dG8gY29udHJvbHMgdG8gdGhlIGNvbnRyb2xzIGVsZW1lbnRcbiAgICAgKi9cbiAgICB2YXIgYXBwZW5kQ29udHJvbHNBdXRvID0gZnVuY3Rpb24oKSB7XG4gICAgICBzbGlkZXIuY29udHJvbHMuc3RhcnQgPSAkKCc8ZGl2IGNsYXNzPVwiYngtY29udHJvbHMtYXV0by1pdGVtXCI+PGEgY2xhc3M9XCJieC1zdGFydFwiIGhyZWY9XCJcIj4nICsgc2xpZGVyLnNldHRpbmdzLnN0YXJ0VGV4dCArICc8L2E+PC9kaXY+Jyk7XG4gICAgICBzbGlkZXIuY29udHJvbHMuc3RvcCA9ICQoJzxkaXYgY2xhc3M9XCJieC1jb250cm9scy1hdXRvLWl0ZW1cIj48YSBjbGFzcz1cImJ4LXN0b3BcIiBocmVmPVwiXCI+JyArIHNsaWRlci5zZXR0aW5ncy5zdG9wVGV4dCArICc8L2E+PC9kaXY+Jyk7XG4gICAgICAvLyBhZGQgdGhlIGNvbnRyb2xzIHRvIHRoZSBET01cbiAgICAgIHNsaWRlci5jb250cm9scy5hdXRvRWwgPSAkKCc8ZGl2IGNsYXNzPVwiYngtY29udHJvbHMtYXV0b1wiIC8+Jyk7XG4gICAgICAvLyBiaW5kIGNsaWNrIGFjdGlvbnMgdG8gdGhlIGNvbnRyb2xzXG4gICAgICBzbGlkZXIuY29udHJvbHMuYXV0b0VsLm9uKCdjbGljaycsICcuYngtc3RhcnQnLCBjbGlja1N0YXJ0QmluZCk7XG4gICAgICBzbGlkZXIuY29udHJvbHMuYXV0b0VsLm9uKCdjbGljaycsICcuYngtc3RvcCcsIGNsaWNrU3RvcEJpbmQpO1xuICAgICAgLy8gaWYgYXV0b0NvbnRyb2xzQ29tYmluZSwgaW5zZXJ0IG9ubHkgdGhlIFwic3RhcnRcIiBjb250cm9sXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmF1dG9Db250cm9sc0NvbWJpbmUpIHtcbiAgICAgICAgc2xpZGVyLmNvbnRyb2xzLmF1dG9FbC5hcHBlbmQoc2xpZGVyLmNvbnRyb2xzLnN0YXJ0KTtcbiAgICAgIC8vIGlmIGF1dG9Db250cm9sc0NvbWJpbmUgaXMgZmFsc2UsIGluc2VydCBib3RoIGNvbnRyb2xzXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzbGlkZXIuY29udHJvbHMuYXV0b0VsLmFwcGVuZChzbGlkZXIuY29udHJvbHMuc3RhcnQpLmFwcGVuZChzbGlkZXIuY29udHJvbHMuc3RvcCk7XG4gICAgICB9XG4gICAgICAvLyBpZiBhdXRvIGNvbnRyb2xzIHNlbGVjdG9yIHdhcyBzdXBwbGllZCwgcG9wdWxhdGUgaXQgd2l0aCB0aGUgY29udHJvbHNcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzU2VsZWN0b3IpIHtcbiAgICAgICAgJChzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzU2VsZWN0b3IpLmh0bWwoc2xpZGVyLmNvbnRyb2xzLmF1dG9FbCk7XG4gICAgICAvLyBpZiBhdXRvIGNvbnRyb2xzIHNlbGVjdG9yIHdhcyBub3Qgc3VwcGxpZWQsIGFkZCBpdCBhZnRlciB0aGUgd3JhcHBlclxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLmFkZENsYXNzKCdieC1oYXMtY29udHJvbHMtYXV0bycpLmFwcGVuZChzbGlkZXIuY29udHJvbHMuYXV0b0VsKTtcbiAgICAgIH1cbiAgICAgIC8vIHVwZGF0ZSB0aGUgYXV0byBjb250cm9sc1xuICAgICAgdXBkYXRlQXV0b0NvbnRyb2xzKHNsaWRlci5zZXR0aW5ncy5hdXRvU3RhcnQgPyAnc3RvcCcgOiAnc3RhcnQnKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQXBwZW5kcyBpbWFnZSBjYXB0aW9ucyB0byB0aGUgRE9NXG4gICAgICovXG4gICAgdmFyIGFwcGVuZENhcHRpb25zID0gZnVuY3Rpb24oKSB7XG4gICAgICAvLyBjeWNsZSB0aHJvdWdoIGVhY2ggY2hpbGRcbiAgICAgIHNsaWRlci5jaGlsZHJlbi5lYWNoKGZ1bmN0aW9uKGluZGV4KSB7XG4gICAgICAgIC8vIGdldCB0aGUgaW1hZ2UgdGl0bGUgYXR0cmlidXRlXG4gICAgICAgIHZhciB0aXRsZSA9ICQodGhpcykuZmluZCgnaW1nOmZpcnN0JykuYXR0cigndGl0bGUnKTtcbiAgICAgICAgLy8gYXBwZW5kIHRoZSBjYXB0aW9uXG4gICAgICAgIGlmICh0aXRsZSAhPT0gdW5kZWZpbmVkICYmICgnJyArIHRpdGxlKS5sZW5ndGgpIHtcbiAgICAgICAgICAkKHRoaXMpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImJ4LWNhcHRpb25cIj48c3Bhbj4nICsgdGl0bGUgKyAnPC9zcGFuPjwvZGl2PicpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQ2xpY2sgbmV4dCBiaW5kaW5nXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZSAoZXZlbnQpXG4gICAgICogIC0gRE9NIGV2ZW50IG9iamVjdFxuICAgICAqL1xuICAgIHZhciBjbGlja05leHRCaW5kID0gZnVuY3Rpb24oZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgaWYgKHNsaWRlci5jb250cm9scy5lbC5oYXNDbGFzcygnZGlzYWJsZWQnKSkgeyByZXR1cm47IH1cbiAgICAgIC8vIGlmIGF1dG8gc2hvdyBpcyBydW5uaW5nLCBzdG9wIGl0XG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmF1dG8gJiYgc2xpZGVyLnNldHRpbmdzLnN0b3BBdXRvT25DbGljaykgeyBlbC5zdG9wQXV0bygpOyB9XG4gICAgICBlbC5nb1RvTmV4dFNsaWRlKCk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENsaWNrIHByZXYgYmluZGluZ1xuICAgICAqXG4gICAgICogQHBhcmFtIGUgKGV2ZW50KVxuICAgICAqICAtIERPTSBldmVudCBvYmplY3RcbiAgICAgKi9cbiAgICB2YXIgY2xpY2tQcmV2QmluZCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGlmIChzbGlkZXIuY29udHJvbHMuZWwuaGFzQ2xhc3MoJ2Rpc2FibGVkJykpIHsgcmV0dXJuOyB9XG4gICAgICAvLyBpZiBhdXRvIHNob3cgaXMgcnVubmluZywgc3RvcCBpdFxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hdXRvICYmIHNsaWRlci5zZXR0aW5ncy5zdG9wQXV0b09uQ2xpY2spIHsgZWwuc3RvcEF1dG8oKTsgfVxuICAgICAgZWwuZ29Ub1ByZXZTbGlkZSgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDbGljayBzdGFydCBiaW5kaW5nXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZSAoZXZlbnQpXG4gICAgICogIC0gRE9NIGV2ZW50IG9iamVjdFxuICAgICAqL1xuICAgIHZhciBjbGlja1N0YXJ0QmluZCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgIGVsLnN0YXJ0QXV0bygpO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDbGljayBzdG9wIGJpbmRpbmdcbiAgICAgKlxuICAgICAqIEBwYXJhbSBlIChldmVudClcbiAgICAgKiAgLSBET00gZXZlbnQgb2JqZWN0XG4gICAgICovXG4gICAgdmFyIGNsaWNrU3RvcEJpbmQgPSBmdW5jdGlvbihlKSB7XG4gICAgICBlbC5zdG9wQXV0bygpO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDbGljayBwYWdlciBiaW5kaW5nXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZSAoZXZlbnQpXG4gICAgICogIC0gRE9NIGV2ZW50IG9iamVjdFxuICAgICAqL1xuICAgIHZhciBjbGlja1BhZ2VyQmluZCA9IGZ1bmN0aW9uKGUpIHtcbiAgICAgIHZhciBwYWdlckxpbmssIHBhZ2VySW5kZXg7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBpZiAoc2xpZGVyLmNvbnRyb2xzLmVsLmhhc0NsYXNzKCdkaXNhYmxlZCcpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIC8vIGlmIGF1dG8gc2hvdyBpcyBydW5uaW5nLCBzdG9wIGl0XG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmF1dG8gICYmIHNsaWRlci5zZXR0aW5ncy5zdG9wQXV0b09uQ2xpY2spIHsgZWwuc3RvcEF1dG8oKTsgfVxuICAgICAgcGFnZXJMaW5rID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgaWYgKHBhZ2VyTGluay5hdHRyKCdkYXRhLXNsaWRlLWluZGV4JykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBwYWdlckluZGV4ID0gcGFyc2VJbnQocGFnZXJMaW5rLmF0dHIoJ2RhdGEtc2xpZGUtaW5kZXgnKSk7XG4gICAgICAgIC8vIGlmIGNsaWNrZWQgcGFnZXIgbGluayBpcyBub3QgYWN0aXZlLCBjb250aW51ZSB3aXRoIHRoZSBnb1RvU2xpZGUgY2FsbFxuICAgICAgICBpZiAocGFnZXJJbmRleCAhPT0gc2xpZGVyLmFjdGl2ZS5pbmRleCkgeyBlbC5nb1RvU2xpZGUocGFnZXJJbmRleCk7IH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVXBkYXRlcyB0aGUgcGFnZXIgbGlua3Mgd2l0aCBhbiBhY3RpdmUgY2xhc3NcbiAgICAgKlxuICAgICAqIEBwYXJhbSBzbGlkZUluZGV4IChpbnQpXG4gICAgICogIC0gaW5kZXggb2Ygc2xpZGUgdG8gbWFrZSBhY3RpdmVcbiAgICAgKi9cbiAgICB2YXIgdXBkYXRlUGFnZXJBY3RpdmUgPSBmdW5jdGlvbihzbGlkZUluZGV4KSB7XG4gICAgICAvLyBpZiBcInNob3J0XCIgcGFnZXIgdHlwZVxuICAgICAgdmFyIGxlbiA9IHNsaWRlci5jaGlsZHJlbi5sZW5ndGg7IC8vIG5iIG9mIGNoaWxkcmVuXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLnBhZ2VyVHlwZSA9PT0gJ3Nob3J0Jykge1xuICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1heFNsaWRlcyA+IDEpIHtcbiAgICAgICAgICBsZW4gPSBNYXRoLmNlaWwoc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAvIHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXMpO1xuICAgICAgICB9XG4gICAgICAgIHNsaWRlci5wYWdlckVsLmh0bWwoKHNsaWRlSW5kZXggKyAxKSArIHNsaWRlci5zZXR0aW5ncy5wYWdlclNob3J0U2VwYXJhdG9yICsgbGVuKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgLy8gcmVtb3ZlIGFsbCBwYWdlciBhY3RpdmUgY2xhc3Nlc1xuICAgICAgc2xpZGVyLnBhZ2VyRWwuZmluZCgnYScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgIC8vIGFwcGx5IHRoZSBhY3RpdmUgY2xhc3MgZm9yIGFsbCBwYWdlcnNcbiAgICAgIHNsaWRlci5wYWdlckVsLmVhY2goZnVuY3Rpb24oaSwgZWwpIHsgJChlbCkuZmluZCgnYScpLmVxKHNsaWRlSW5kZXgpLmFkZENsYXNzKCdhY3RpdmUnKTsgfSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFBlcmZvcm1zIG5lZWRlZCBhY3Rpb25zIGFmdGVyIGEgc2xpZGUgdHJhbnNpdGlvblxuICAgICAqL1xuICAgIHZhciB1cGRhdGVBZnRlclNsaWRlVHJhbnNpdGlvbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gaWYgaW5maW5pdGUgbG9vcCBpcyB0cnVlXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmluZmluaXRlTG9vcCkge1xuICAgICAgICB2YXIgcG9zaXRpb24gPSAnJztcbiAgICAgICAgLy8gZmlyc3Qgc2xpZGVcbiAgICAgICAgaWYgKHNsaWRlci5hY3RpdmUuaW5kZXggPT09IDApIHtcbiAgICAgICAgICAvLyBzZXQgdGhlIG5ldyBwb3NpdGlvblxuICAgICAgICAgIHBvc2l0aW9uID0gc2xpZGVyLmNoaWxkcmVuLmVxKDApLnBvc2l0aW9uKCk7XG4gICAgICAgIC8vIGNhcm91c2VsLCBsYXN0IHNsaWRlXG4gICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLmFjdGl2ZS5pbmRleCA9PT0gZ2V0UGFnZXJRdHkoKSAtIDEgJiYgc2xpZGVyLmNhcm91c2VsKSB7XG4gICAgICAgICAgcG9zaXRpb24gPSBzbGlkZXIuY2hpbGRyZW4uZXEoKGdldFBhZ2VyUXR5KCkgLSAxKSAqIGdldE1vdmVCeSgpKS5wb3NpdGlvbigpO1xuICAgICAgICAvLyBsYXN0IHNsaWRlXG4gICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLmFjdGl2ZS5pbmRleCA9PT0gc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgICBwb3NpdGlvbiA9IHNsaWRlci5jaGlsZHJlbi5lcShzbGlkZXIuY2hpbGRyZW4ubGVuZ3RoIC0gMSkucG9zaXRpb24oKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocG9zaXRpb24pIHtcbiAgICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICdob3Jpem9udGFsJykgeyBzZXRQb3NpdGlvblByb3BlcnR5KC1wb3NpdGlvbi5sZWZ0LCAncmVzZXQnLCAwKTsgfVxuICAgICAgICAgIGVsc2UgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAndmVydGljYWwnKSB7IHNldFBvc2l0aW9uUHJvcGVydHkoLXBvc2l0aW9uLnRvcCwgJ3Jlc2V0JywgMCk7IH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLy8gZGVjbGFyZSB0aGF0IHRoZSB0cmFuc2l0aW9uIGlzIGNvbXBsZXRlXG4gICAgICBzbGlkZXIud29ya2luZyA9IGZhbHNlO1xuICAgICAgLy8gb25TbGlkZUFmdGVyIGNhbGxiYWNrXG4gICAgICBzbGlkZXIuc2V0dGluZ3Mub25TbGlkZUFmdGVyLmNhbGwoZWwsIHNsaWRlci5jaGlsZHJlbi5lcShzbGlkZXIuYWN0aXZlLmluZGV4KSwgc2xpZGVyLm9sZEluZGV4LCBzbGlkZXIuYWN0aXZlLmluZGV4KTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVXBkYXRlcyB0aGUgYXV0byBjb250cm9scyBzdGF0ZSAoZWl0aGVyIGFjdGl2ZSwgb3IgY29tYmluZWQgc3dpdGNoKVxuICAgICAqXG4gICAgICogQHBhcmFtIHN0YXRlIChzdHJpbmcpIFwic3RhcnRcIiwgXCJzdG9wXCJcbiAgICAgKiAgLSB0aGUgbmV3IHN0YXRlIG9mIHRoZSBhdXRvIHNob3dcbiAgICAgKi9cbiAgICB2YXIgdXBkYXRlQXV0b0NvbnRyb2xzID0gZnVuY3Rpb24oc3RhdGUpIHtcbiAgICAgIC8vIGlmIGF1dG9Db250cm9sc0NvbWJpbmUgaXMgdHJ1ZSwgcmVwbGFjZSB0aGUgY3VycmVudCBjb250cm9sIHdpdGggdGhlIG5ldyBzdGF0ZVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hdXRvQ29udHJvbHNDb21iaW5lKSB7XG4gICAgICAgIHNsaWRlci5jb250cm9scy5hdXRvRWwuaHRtbChzbGlkZXIuY29udHJvbHNbc3RhdGVdKTtcbiAgICAgIC8vIGlmIGF1dG9Db250cm9sc0NvbWJpbmUgaXMgZmFsc2UsIGFwcGx5IHRoZSBcImFjdGl2ZVwiIGNsYXNzIHRvIHRoZSBhcHByb3ByaWF0ZSBjb250cm9sXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzbGlkZXIuY29udHJvbHMuYXV0b0VsLmZpbmQoJ2EnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgIHNsaWRlci5jb250cm9scy5hdXRvRWwuZmluZCgnYTpub3QoLmJ4LScgKyBzdGF0ZSArICcpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBVcGRhdGVzIHRoZSBkaXJlY3Rpb24gY29udHJvbHMgKGNoZWNrcyBpZiBlaXRoZXIgc2hvdWxkIGJlIGhpZGRlbilcbiAgICAgKi9cbiAgICB2YXIgdXBkYXRlRGlyZWN0aW9uQ29udHJvbHMgPSBmdW5jdGlvbigpIHtcbiAgICAgIGlmIChnZXRQYWdlclF0eSgpID09PSAxKSB7XG4gICAgICAgIHNsaWRlci5jb250cm9scy5wcmV2LmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgICBzbGlkZXIuY29udHJvbHMubmV4dC5hZGRDbGFzcygnZGlzYWJsZWQnKTtcbiAgICAgIH0gZWxzZSBpZiAoIXNsaWRlci5zZXR0aW5ncy5pbmZpbml0ZUxvb3AgJiYgc2xpZGVyLnNldHRpbmdzLmhpZGVDb250cm9sT25FbmQpIHtcbiAgICAgICAgLy8gaWYgZmlyc3Qgc2xpZGVcbiAgICAgICAgaWYgKHNsaWRlci5hY3RpdmUuaW5kZXggPT09IDApIHtcbiAgICAgICAgICBzbGlkZXIuY29udHJvbHMucHJldi5hZGRDbGFzcygnZGlzYWJsZWQnKTtcbiAgICAgICAgICBzbGlkZXIuY29udHJvbHMubmV4dC5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcbiAgICAgICAgLy8gaWYgbGFzdCBzbGlkZVxuICAgICAgICB9IGVsc2UgaWYgKHNsaWRlci5hY3RpdmUuaW5kZXggPT09IGdldFBhZ2VyUXR5KCkgLSAxKSB7XG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xzLm5leHQuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xzLnByZXYucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgIC8vIGlmIGFueSBzbGlkZSBpbiB0aGUgbWlkZGxlXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xzLnByZXYucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xzLm5leHQucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXHQvKiBhdXRvIHN0YXJ0IGFuZCBzdG9wIGZ1bmN0aW9ucyAqL1xuXHR2YXIgd2luZG93Rm9jdXNIYW5kbGVyID0gZnVuY3Rpb24oKSB7IGVsLnN0YXJ0QXV0bygpOyB9O1xuXHR2YXIgd2luZG93Qmx1ckhhbmRsZXIgPSBmdW5jdGlvbigpIHsgZWwuc3RvcEF1dG8oKTsgfTtcbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyB0aGUgYXV0byBwcm9jZXNzXG4gICAgICovXG4gICAgdmFyIGluaXRBdXRvID0gZnVuY3Rpb24oKSB7XG4gICAgICAvLyBpZiBhdXRvRGVsYXkgd2FzIHN1cHBsaWVkLCBsYXVuY2ggdGhlIGF1dG8gc2hvdyB1c2luZyBhIHNldFRpbWVvdXQoKSBjYWxsXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmF1dG9EZWxheSA+IDApIHtcbiAgICAgICAgdmFyIHRpbWVvdXQgPSBzZXRUaW1lb3V0KGVsLnN0YXJ0QXV0bywgc2xpZGVyLnNldHRpbmdzLmF1dG9EZWxheSk7XG4gICAgICAvLyBpZiBhdXRvRGVsYXkgd2FzIG5vdCBzdXBwbGllZCwgc3RhcnQgdGhlIGF1dG8gc2hvdyBub3JtYWxseVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWwuc3RhcnRBdXRvKCk7XG5cbiAgICAgICAgLy9hZGQgZm9jdXMgYW5kIGJsdXIgZXZlbnRzIHRvIGVuc3VyZSBpdHMgcnVubmluZyBpZiB0aW1lb3V0IGdldHMgcGF1c2VkXG4gICAgICAgICQod2luZG93KS5mb2N1cyh3aW5kb3dGb2N1c0hhbmRsZXIpLmJsdXIod2luZG93Qmx1ckhhbmRsZXIpO1xuICAgICAgfVxuICAgICAgLy8gaWYgYXV0b0hvdmVyIGlzIHJlcXVlc3RlZFxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hdXRvSG92ZXIpIHtcbiAgICAgICAgLy8gb24gZWwgaG92ZXJcbiAgICAgICAgZWwuaG92ZXIoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgLy8gaWYgdGhlIGF1dG8gc2hvdyBpcyBjdXJyZW50bHkgcGxheWluZyAoaGFzIGFuIGFjdGl2ZSBpbnRlcnZhbClcbiAgICAgICAgICBpZiAoc2xpZGVyLmludGVydmFsKSB7XG4gICAgICAgICAgICAvLyBzdG9wIHRoZSBhdXRvIHNob3cgYW5kIHBhc3MgdHJ1ZSBhcmd1bWVudCB3aGljaCB3aWxsIHByZXZlbnQgY29udHJvbCB1cGRhdGVcbiAgICAgICAgICAgIGVsLnN0b3BBdXRvKHRydWUpO1xuICAgICAgICAgICAgLy8gY3JlYXRlIGEgbmV3IGF1dG9QYXVzZWQgdmFsdWUgd2hpY2ggd2lsbCBiZSB1c2VkIGJ5IHRoZSByZWxhdGl2ZSBcIm1vdXNlb3V0XCIgZXZlbnRcbiAgICAgICAgICAgIHNsaWRlci5hdXRvUGF1c2VkID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIGZ1bmN0aW9uKCkge1xuICAgICAgICAgIC8vIGlmIHRoZSBhdXRvUGF1c2VkIHZhbHVlIHdhcyBjcmVhdGVkIGJlIHRoZSBwcmlvciBcIm1vdXNlb3ZlclwiIGV2ZW50XG4gICAgICAgICAgaWYgKHNsaWRlci5hdXRvUGF1c2VkKSB7XG4gICAgICAgICAgICAvLyBzdGFydCB0aGUgYXV0byBzaG93IGFuZCBwYXNzIHRydWUgYXJndW1lbnQgd2hpY2ggd2lsbCBwcmV2ZW50IGNvbnRyb2wgdXBkYXRlXG4gICAgICAgICAgICBlbC5zdGFydEF1dG8odHJ1ZSk7XG4gICAgICAgICAgICAvLyByZXNldCB0aGUgYXV0b1BhdXNlZCB2YWx1ZVxuICAgICAgICAgICAgc2xpZGVyLmF1dG9QYXVzZWQgPSBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemVzIHRoZSB0aWNrZXIgcHJvY2Vzc1xuICAgICAqL1xuICAgIHZhciBpbml0VGlja2VyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgc3RhcnRQb3NpdGlvbiA9IDAsXG4gICAgICBwb3NpdGlvbiwgdHJhbnNmb3JtLCB2YWx1ZSwgaWR4LCByYXRpbywgcHJvcGVydHksIG5ld1NwZWVkLCB0b3RhbERpbWVucztcbiAgICAgIC8vIGlmIGF1dG9EaXJlY3Rpb24gaXMgXCJuZXh0XCIsIGFwcGVuZCBhIGNsb25lIG9mIHRoZSBlbnRpcmUgc2xpZGVyXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmF1dG9EaXJlY3Rpb24gPT09ICduZXh0Jykge1xuICAgICAgICBlbC5hcHBlbmQoc2xpZGVyLmNoaWxkcmVuLmNsb25lKCkuYWRkQ2xhc3MoJ2J4LWNsb25lJykpO1xuICAgICAgLy8gaWYgYXV0b0RpcmVjdGlvbiBpcyBcInByZXZcIiwgcHJlcGVuZCBhIGNsb25lIG9mIHRoZSBlbnRpcmUgc2xpZGVyLCBhbmQgc2V0IHRoZSBsZWZ0IHBvc2l0aW9uXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbC5wcmVwZW5kKHNsaWRlci5jaGlsZHJlbi5jbG9uZSgpLmFkZENsYXNzKCdieC1jbG9uZScpKTtcbiAgICAgICAgcG9zaXRpb24gPSBzbGlkZXIuY2hpbGRyZW4uZmlyc3QoKS5wb3NpdGlvbigpO1xuICAgICAgICBzdGFydFBvc2l0aW9uID0gc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICdob3Jpem9udGFsJyA/IC1wb3NpdGlvbi5sZWZ0IDogLXBvc2l0aW9uLnRvcDtcbiAgICAgIH1cbiAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkoc3RhcnRQb3NpdGlvbiwgJ3Jlc2V0JywgMCk7XG4gICAgICAvLyBkbyBub3QgYWxsb3cgY29udHJvbHMgaW4gdGlja2VyIG1vZGVcbiAgICAgIHNsaWRlci5zZXR0aW5ncy5wYWdlciA9IGZhbHNlO1xuICAgICAgc2xpZGVyLnNldHRpbmdzLmNvbnRyb2xzID0gZmFsc2U7XG4gICAgICBzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzID0gZmFsc2U7XG4gICAgICAvLyBpZiBhdXRvSG92ZXIgaXMgcmVxdWVzdGVkXG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLnRpY2tlckhvdmVyKSB7XG4gICAgICAgIGlmIChzbGlkZXIudXNpbmdDU1MpIHtcbiAgICAgICAgICBpZHggPSBzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2hvcml6b250YWwnID8gNCA6IDU7XG4gICAgICAgICAgc2xpZGVyLnZpZXdwb3J0LmhvdmVyKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdHJhbnNmb3JtID0gZWwuY3NzKCctJyArIHNsaWRlci5jc3NQcmVmaXggKyAnLXRyYW5zZm9ybScpO1xuICAgICAgICAgICAgdmFsdWUgPSBwYXJzZUZsb2F0KHRyYW5zZm9ybS5zcGxpdCgnLCcpW2lkeF0pO1xuICAgICAgICAgICAgc2V0UG9zaXRpb25Qcm9wZXJ0eSh2YWx1ZSwgJ3Jlc2V0JywgMCk7XG4gICAgICAgICAgfSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB0b3RhbERpbWVucyA9IDA7XG4gICAgICAgICAgICBzbGlkZXIuY2hpbGRyZW4uZWFjaChmdW5jdGlvbihpbmRleCkge1xuICAgICAgICAgICAgICB0b3RhbERpbWVucyArPSBzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2hvcml6b250YWwnID8gJCh0aGlzKS5vdXRlcldpZHRoKHRydWUpIDogJCh0aGlzKS5vdXRlckhlaWdodCh0cnVlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gY2FsY3VsYXRlIHRoZSBzcGVlZCByYXRpbyAodXNlZCB0byBkZXRlcm1pbmUgdGhlIG5ldyBzcGVlZCB0byBmaW5pc2ggdGhlIHBhdXNlZCBhbmltYXRpb24pXG4gICAgICAgICAgICByYXRpbyA9IHNsaWRlci5zZXR0aW5ncy5zcGVlZCAvIHRvdGFsRGltZW5zO1xuICAgICAgICAgICAgLy8gZGV0ZXJtaW5lIHdoaWNoIHByb3BlcnR5IHRvIHVzZVxuICAgICAgICAgICAgcHJvcGVydHkgPSBzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2hvcml6b250YWwnID8gJ2xlZnQnIDogJ3RvcCc7XG4gICAgICAgICAgICAvLyBjYWxjdWxhdGUgdGhlIG5ldyBzcGVlZFxuICAgICAgICAgICAgbmV3U3BlZWQgPSByYXRpbyAqICh0b3RhbERpbWVucyAtIChNYXRoLmFicyhwYXJzZUludCh2YWx1ZSkpKSk7XG4gICAgICAgICAgICB0aWNrZXJMb29wKG5ld1NwZWVkKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBvbiBlbCBob3ZlclxuICAgICAgICAgIHNsaWRlci52aWV3cG9ydC5ob3ZlcihmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGVsLnN0b3AoKTtcbiAgICAgICAgICB9LCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSB0aGUgdG90YWwgd2lkdGggb2YgY2hpbGRyZW4gKHVzZWQgdG8gY2FsY3VsYXRlIHRoZSBzcGVlZCByYXRpbylcbiAgICAgICAgICAgIHRvdGFsRGltZW5zID0gMDtcbiAgICAgICAgICAgIHNsaWRlci5jaGlsZHJlbi5lYWNoKGZ1bmN0aW9uKGluZGV4KSB7XG4gICAgICAgICAgICAgIHRvdGFsRGltZW5zICs9IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgPyAkKHRoaXMpLm91dGVyV2lkdGgodHJ1ZSkgOiAkKHRoaXMpLm91dGVySGVpZ2h0KHRydWUpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvLyBjYWxjdWxhdGUgdGhlIHNwZWVkIHJhdGlvICh1c2VkIHRvIGRldGVybWluZSB0aGUgbmV3IHNwZWVkIHRvIGZpbmlzaCB0aGUgcGF1c2VkIGFuaW1hdGlvbilcbiAgICAgICAgICAgIHJhdGlvID0gc2xpZGVyLnNldHRpbmdzLnNwZWVkIC8gdG90YWxEaW1lbnM7XG4gICAgICAgICAgICAvLyBkZXRlcm1pbmUgd2hpY2ggcHJvcGVydHkgdG8gdXNlXG4gICAgICAgICAgICBwcm9wZXJ0eSA9IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgPyAnbGVmdCcgOiAndG9wJztcbiAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSB0aGUgbmV3IHNwZWVkXG4gICAgICAgICAgICBuZXdTcGVlZCA9IHJhdGlvICogKHRvdGFsRGltZW5zIC0gKE1hdGguYWJzKHBhcnNlSW50KGVsLmNzcyhwcm9wZXJ0eSkpKSkpO1xuICAgICAgICAgICAgdGlja2VyTG9vcChuZXdTcGVlZCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8vIHN0YXJ0IHRoZSB0aWNrZXIgbG9vcFxuICAgICAgdGlja2VyTG9vcCgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSdW5zIGEgY29udGludW91cyBsb29wLCBuZXdzIHRpY2tlci1zdHlsZVxuICAgICAqL1xuICAgIHZhciB0aWNrZXJMb29wID0gZnVuY3Rpb24ocmVzdW1lU3BlZWQpIHtcbiAgICAgIHZhciBzcGVlZCA9IHJlc3VtZVNwZWVkID8gcmVzdW1lU3BlZWQgOiBzbGlkZXIuc2V0dGluZ3Muc3BlZWQsXG4gICAgICBwb3NpdGlvbiA9IHtsZWZ0OiAwLCB0b3A6IDB9LFxuICAgICAgcmVzZXQgPSB7bGVmdDogMCwgdG9wOiAwfSxcbiAgICAgIGFuaW1hdGVQcm9wZXJ0eSwgcmVzZXRWYWx1ZSwgcGFyYW1zO1xuXG4gICAgICAvLyBpZiBcIm5leHRcIiBhbmltYXRlIGxlZnQgcG9zaXRpb24gdG8gbGFzdCBjaGlsZCwgdGhlbiByZXNldCBsZWZ0IHRvIDBcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYXV0b0RpcmVjdGlvbiA9PT0gJ25leHQnKSB7XG4gICAgICAgIHBvc2l0aW9uID0gZWwuZmluZCgnLmJ4LWNsb25lJykuZmlyc3QoKS5wb3NpdGlvbigpO1xuICAgICAgLy8gaWYgXCJwcmV2XCIgYW5pbWF0ZSBsZWZ0IHBvc2l0aW9uIHRvIDAsIHRoZW4gcmVzZXQgbGVmdCB0byBmaXJzdCBub24tY2xvbmUgY2hpbGRcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc2V0ID0gc2xpZGVyLmNoaWxkcmVuLmZpcnN0KCkucG9zaXRpb24oKTtcbiAgICAgIH1cbiAgICAgIGFuaW1hdGVQcm9wZXJ0eSA9IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgPyAtcG9zaXRpb24ubGVmdCA6IC1wb3NpdGlvbi50b3A7XG4gICAgICByZXNldFZhbHVlID0gc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICdob3Jpem9udGFsJyA/IC1yZXNldC5sZWZ0IDogLXJlc2V0LnRvcDtcbiAgICAgIHBhcmFtcyA9IHtyZXNldFZhbHVlOiByZXNldFZhbHVlfTtcbiAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkoYW5pbWF0ZVByb3BlcnR5LCAndGlja2VyJywgc3BlZWQsIHBhcmFtcyk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENoZWNrIGlmIGVsIGlzIG9uIHNjcmVlblxuICAgICAqL1xuICAgIHZhciBpc09uU2NyZWVuID0gZnVuY3Rpb24oZWwpIHtcbiAgICAgIHZhciB3aW4gPSAkKHdpbmRvdyksXG4gICAgICB2aWV3cG9ydCA9IHtcbiAgICAgICAgdG9wOiB3aW4uc2Nyb2xsVG9wKCksXG4gICAgICAgIGxlZnQ6IHdpbi5zY3JvbGxMZWZ0KClcbiAgICAgIH0sXG4gICAgICBib3VuZHMgPSBlbC5vZmZzZXQoKTtcblxuICAgICAgdmlld3BvcnQucmlnaHQgPSB2aWV3cG9ydC5sZWZ0ICsgd2luLndpZHRoKCk7XG4gICAgICB2aWV3cG9ydC5ib3R0b20gPSB2aWV3cG9ydC50b3AgKyB3aW4uaGVpZ2h0KCk7XG4gICAgICBib3VuZHMucmlnaHQgPSBib3VuZHMubGVmdCArIGVsLm91dGVyV2lkdGgoKTtcbiAgICAgIGJvdW5kcy5ib3R0b20gPSBib3VuZHMudG9wICsgZWwub3V0ZXJIZWlnaHQoKTtcblxuICAgICAgcmV0dXJuICghKHZpZXdwb3J0LnJpZ2h0IDwgYm91bmRzLmxlZnQgfHwgdmlld3BvcnQubGVmdCA+IGJvdW5kcy5yaWdodCB8fCB2aWV3cG9ydC5ib3R0b20gPCBib3VuZHMudG9wIHx8IHZpZXdwb3J0LnRvcCA+IGJvdW5kcy5ib3R0b20pKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZXMga2V5Ym9hcmQgZXZlbnRzXG4gICAgICovXG4gICAgdmFyIGtleVByZXNzID0gZnVuY3Rpb24oZSkge1xuICAgICAgdmFyIGFjdGl2ZUVsZW1lbnRUYWcgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50LnRhZ05hbWUudG9Mb3dlckNhc2UoKSxcbiAgICAgIHRhZ0ZpbHRlcnMgPSAnaW5wdXR8dGV4dGFyZWEnLFxuICAgICAgcCA9IG5ldyBSZWdFeHAoYWN0aXZlRWxlbWVudFRhZyxbJ2knXSksXG4gICAgICByZXN1bHQgPSBwLmV4ZWModGFnRmlsdGVycyk7XG5cbiAgICAgIGlmIChyZXN1bHQgPT0gbnVsbCAmJiBpc09uU2NyZWVuKGVsKSkge1xuICAgICAgICBpZiAoZS5rZXlDb2RlID09PSAzOSkge1xuICAgICAgICAgIGNsaWNrTmV4dEJpbmQoZSk7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9IGVsc2UgaWYgKGUua2V5Q29kZSA9PT0gMzcpIHtcbiAgICAgICAgICBjbGlja1ByZXZCaW5kKGUpO1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyB0b3VjaCBldmVudHNcbiAgICAgKi9cbiAgICB2YXIgaW5pdFRvdWNoID0gZnVuY3Rpb24oKSB7XG4gICAgICAvLyBpbml0aWFsaXplIG9iamVjdCB0byBjb250YWluIGFsbCB0b3VjaCB2YWx1ZXNcbiAgICAgIHNsaWRlci50b3VjaCA9IHtcbiAgICAgICAgc3RhcnQ6IHt4OiAwLCB5OiAwfSxcbiAgICAgICAgZW5kOiB7eDogMCwgeTogMH1cbiAgICAgIH07XG4gICAgICBzbGlkZXIudmlld3BvcnQuYmluZCgndG91Y2hzdGFydCBNU1BvaW50ZXJEb3duIHBvaW50ZXJkb3duJywgb25Ub3VjaFN0YXJ0KTtcblxuICAgICAgLy9mb3IgYnJvd3NlcnMgdGhhdCBoYXZlIGltcGxlbWVudGVkIHBvaW50ZXIgZXZlbnRzIGFuZCBmaXJlIGEgY2xpY2sgYWZ0ZXJcbiAgICAgIC8vZXZlcnkgcG9pbnRlcnVwIHJlZ2FyZGxlc3Mgb2Ygd2hldGhlciBwb2ludGVydXAgaXMgb24gc2FtZSBzY3JlZW4gbG9jYXRpb24gYXMgcG9pbnRlcmRvd24gb3Igbm90XG4gICAgICBzbGlkZXIudmlld3BvcnQub24oJ2NsaWNrJywgJy5ieHNsaWRlciBhJywgZnVuY3Rpb24oZSkge1xuICAgICAgICBpZiAoc2xpZGVyLnZpZXdwb3J0Lmhhc0NsYXNzKCdjbGljay1kaXNhYmxlZCcpKSB7XG4gICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIHNsaWRlci52aWV3cG9ydC5yZW1vdmVDbGFzcygnY2xpY2stZGlzYWJsZWQnKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEV2ZW50IGhhbmRsZXIgZm9yIFwidG91Y2hzdGFydFwiXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZSAoZXZlbnQpXG4gICAgICogIC0gRE9NIGV2ZW50IG9iamVjdFxuICAgICAqL1xuICAgIHZhciBvblRvdWNoU3RhcnQgPSBmdW5jdGlvbihlKSB7XG4gICAgICAvL2Rpc2FibGUgc2xpZGVyIGNvbnRyb2xzIHdoaWxlIHVzZXIgaXMgaW50ZXJhY3Rpbmcgd2l0aCBzbGlkZXMgdG8gYXZvaWQgc2xpZGVyIGZyZWV6ZSB0aGF0IGhhcHBlbnMgb24gdG91Y2ggZGV2aWNlcyB3aGVuIGEgc2xpZGUgc3dpcGUgaGFwcGVucyBpbW1lZGlhdGVseSBhZnRlciBpbnRlcmFjdGluZyB3aXRoIHNsaWRlciBjb250cm9sc1xuICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuXG4gICAgICBpZiAoc2xpZGVyLndvcmtpbmcpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBzbGlkZXIuY29udHJvbHMuZWwucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyByZWNvcmQgdGhlIG9yaWdpbmFsIHBvc2l0aW9uIHdoZW4gdG91Y2ggc3RhcnRzXG4gICAgICAgIHNsaWRlci50b3VjaC5vcmlnaW5hbFBvcyA9IGVsLnBvc2l0aW9uKCk7XG4gICAgICAgIHZhciBvcmlnID0gZS5vcmlnaW5hbEV2ZW50LFxuICAgICAgICB0b3VjaFBvaW50cyA9ICh0eXBlb2Ygb3JpZy5jaGFuZ2VkVG91Y2hlcyAhPT0gJ3VuZGVmaW5lZCcpID8gb3JpZy5jaGFuZ2VkVG91Y2hlcyA6IFtvcmlnXTtcbiAgICAgICAgLy8gcmVjb3JkIHRoZSBzdGFydGluZyB0b3VjaCB4LCB5IGNvb3JkaW5hdGVzXG4gICAgICAgIHNsaWRlci50b3VjaC5zdGFydC54ID0gdG91Y2hQb2ludHNbMF0ucGFnZVg7XG4gICAgICAgIHNsaWRlci50b3VjaC5zdGFydC55ID0gdG91Y2hQb2ludHNbMF0ucGFnZVk7XG5cbiAgICAgICAgaWYgKHNsaWRlci52aWV3cG9ydC5nZXQoMCkuc2V0UG9pbnRlckNhcHR1cmUpIHtcbiAgICAgICAgICBzbGlkZXIucG9pbnRlcklkID0gb3JpZy5wb2ludGVySWQ7XG4gICAgICAgICAgc2xpZGVyLnZpZXdwb3J0LmdldCgwKS5zZXRQb2ludGVyQ2FwdHVyZShzbGlkZXIucG9pbnRlcklkKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBiaW5kIGEgXCJ0b3VjaG1vdmVcIiBldmVudCB0byB0aGUgdmlld3BvcnRcbiAgICAgICAgc2xpZGVyLnZpZXdwb3J0LmJpbmQoJ3RvdWNobW92ZSBNU1BvaW50ZXJNb3ZlIHBvaW50ZXJtb3ZlJywgb25Ub3VjaE1vdmUpO1xuICAgICAgICAvLyBiaW5kIGEgXCJ0b3VjaGVuZFwiIGV2ZW50IHRvIHRoZSB2aWV3cG9ydFxuICAgICAgICBzbGlkZXIudmlld3BvcnQuYmluZCgndG91Y2hlbmQgTVNQb2ludGVyVXAgcG9pbnRlcnVwJywgb25Ub3VjaEVuZCk7XG4gICAgICAgIHNsaWRlci52aWV3cG9ydC5iaW5kKCdNU1BvaW50ZXJDYW5jZWwgcG9pbnRlcmNhbmNlbCcsIG9uUG9pbnRlckNhbmNlbCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIENhbmNlbCBQb2ludGVyIGZvciBXaW5kb3dzIFBob25lXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZSAoZXZlbnQpXG4gICAgICogIC0gRE9NIGV2ZW50IG9iamVjdFxuICAgICAqL1xuICAgIHZhciBvblBvaW50ZXJDYW5jZWwgPSBmdW5jdGlvbihlKSB7XG4gICAgICAvKiBvblBvaW50ZXJDYW5jZWwgaGFuZGxlciBpcyBuZWVkZWQgdG8gZGVhbCB3aXRoIHNpdHVhdGlvbnMgd2hlbiBhIHRvdWNoZW5kXG4gICAgICBkb2Vzbid0IGZpcmUgYWZ0ZXIgYSB0b3VjaHN0YXJ0ICh0aGlzIGhhcHBlbnMgb24gd2luZG93cyBwaG9uZXMgb25seSkgKi9cbiAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkoc2xpZGVyLnRvdWNoLm9yaWdpbmFsUG9zLmxlZnQsICdyZXNldCcsIDApO1xuXG4gICAgICAvL3JlbW92ZSBoYW5kbGVyc1xuICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgc2xpZGVyLnZpZXdwb3J0LnVuYmluZCgnTVNQb2ludGVyQ2FuY2VsIHBvaW50ZXJjYW5jZWwnLCBvblBvaW50ZXJDYW5jZWwpO1xuICAgICAgc2xpZGVyLnZpZXdwb3J0LnVuYmluZCgndG91Y2htb3ZlIE1TUG9pbnRlck1vdmUgcG9pbnRlcm1vdmUnLCBvblRvdWNoTW92ZSk7XG4gICAgICBzbGlkZXIudmlld3BvcnQudW5iaW5kKCd0b3VjaGVuZCBNU1BvaW50ZXJVcCBwb2ludGVydXAnLCBvblRvdWNoRW5kKTtcbiAgICAgIGlmIChzbGlkZXIudmlld3BvcnQuZ2V0KDApLnJlbGVhc2VQb2ludGVyQ2FwdHVyZSkge1xuICAgICAgICBzbGlkZXIudmlld3BvcnQuZ2V0KDApLnJlbGVhc2VQb2ludGVyQ2FwdHVyZShzbGlkZXIucG9pbnRlcklkKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogRXZlbnQgaGFuZGxlciBmb3IgXCJ0b3VjaG1vdmVcIlxuICAgICAqXG4gICAgICogQHBhcmFtIGUgKGV2ZW50KVxuICAgICAqICAtIERPTSBldmVudCBvYmplY3RcbiAgICAgKi9cbiAgICB2YXIgb25Ub3VjaE1vdmUgPSBmdW5jdGlvbihlKSB7XG4gICAgICB2YXIgb3JpZyA9IGUub3JpZ2luYWxFdmVudCxcbiAgICAgIHRvdWNoUG9pbnRzID0gKHR5cGVvZiBvcmlnLmNoYW5nZWRUb3VjaGVzICE9PSAndW5kZWZpbmVkJykgPyBvcmlnLmNoYW5nZWRUb3VjaGVzIDogW29yaWddLFxuICAgICAgLy8gaWYgc2Nyb2xsaW5nIG9uIHkgYXhpcywgZG8gbm90IHByZXZlbnQgZGVmYXVsdFxuICAgICAgeE1vdmVtZW50ID0gTWF0aC5hYnModG91Y2hQb2ludHNbMF0ucGFnZVggLSBzbGlkZXIudG91Y2guc3RhcnQueCksXG4gICAgICB5TW92ZW1lbnQgPSBNYXRoLmFicyh0b3VjaFBvaW50c1swXS5wYWdlWSAtIHNsaWRlci50b3VjaC5zdGFydC55KSxcbiAgICAgIHZhbHVlID0gMCxcbiAgICAgIGNoYW5nZSA9IDA7XG5cbiAgICAgIC8vIHggYXhpcyBzd2lwZVxuICAgICAgaWYgKCh4TW92ZW1lbnQgKiAzKSA+IHlNb3ZlbWVudCAmJiBzbGlkZXIuc2V0dGluZ3MucHJldmVudERlZmF1bHRTd2lwZVgpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgLy8geSBheGlzIHN3aXBlXG4gICAgICB9IGVsc2UgaWYgKCh5TW92ZW1lbnQgKiAzKSA+IHhNb3ZlbWVudCAmJiBzbGlkZXIuc2V0dGluZ3MucHJldmVudERlZmF1bHRTd2lwZVkpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlICE9PSAnZmFkZScgJiYgc2xpZGVyLnNldHRpbmdzLm9uZVRvT25lVG91Y2gpIHtcbiAgICAgICAgLy8gaWYgaG9yaXpvbnRhbCwgZHJhZyBhbG9uZyB4IGF4aXNcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcpIHtcbiAgICAgICAgICBjaGFuZ2UgPSB0b3VjaFBvaW50c1swXS5wYWdlWCAtIHNsaWRlci50b3VjaC5zdGFydC54O1xuICAgICAgICAgIHZhbHVlID0gc2xpZGVyLnRvdWNoLm9yaWdpbmFsUG9zLmxlZnQgKyBjaGFuZ2U7XG4gICAgICAgIC8vIGlmIHZlcnRpY2FsLCBkcmFnIGFsb25nIHkgYXhpc1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoYW5nZSA9IHRvdWNoUG9pbnRzWzBdLnBhZ2VZIC0gc2xpZGVyLnRvdWNoLnN0YXJ0Lnk7XG4gICAgICAgICAgdmFsdWUgPSBzbGlkZXIudG91Y2gub3JpZ2luYWxQb3MudG9wICsgY2hhbmdlO1xuICAgICAgICB9XG4gICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkodmFsdWUsICdyZXNldCcsIDApO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBFdmVudCBoYW5kbGVyIGZvciBcInRvdWNoZW5kXCJcbiAgICAgKlxuICAgICAqIEBwYXJhbSBlIChldmVudClcbiAgICAgKiAgLSBET00gZXZlbnQgb2JqZWN0XG4gICAgICovXG4gICAgdmFyIG9uVG91Y2hFbmQgPSBmdW5jdGlvbihlKSB7XG4gICAgICBzbGlkZXIudmlld3BvcnQudW5iaW5kKCd0b3VjaG1vdmUgTVNQb2ludGVyTW92ZSBwb2ludGVybW92ZScsIG9uVG91Y2hNb3ZlKTtcbiAgICAgIC8vZW5hYmxlIHNsaWRlciBjb250cm9scyBhcyBzb29uIGFzIHVzZXIgc3RvcHMgaW50ZXJhY2luZyB3aXRoIHNsaWRlc1xuICAgICAgc2xpZGVyLmNvbnRyb2xzLmVsLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgdmFyIG9yaWcgICAgPSBlLm9yaWdpbmFsRXZlbnQsXG4gICAgICB0b3VjaFBvaW50cyA9ICh0eXBlb2Ygb3JpZy5jaGFuZ2VkVG91Y2hlcyAhPT0gJ3VuZGVmaW5lZCcpID8gb3JpZy5jaGFuZ2VkVG91Y2hlcyA6IFtvcmlnXSxcbiAgICAgIHZhbHVlICAgICAgID0gMCxcbiAgICAgIGRpc3RhbmNlICAgID0gMDtcbiAgICAgIC8vIHJlY29yZCBlbmQgeCwgeSBwb3NpdGlvbnNcbiAgICAgIHNsaWRlci50b3VjaC5lbmQueCA9IHRvdWNoUG9pbnRzWzBdLnBhZ2VYO1xuICAgICAgc2xpZGVyLnRvdWNoLmVuZC55ID0gdG91Y2hQb2ludHNbMF0ucGFnZVk7XG4gICAgICAvLyBpZiBmYWRlIG1vZGUsIGNoZWNrIGlmIGFic29sdXRlIHggZGlzdGFuY2UgY2xlYXJzIHRoZSB0aHJlc2hvbGRcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MubW9kZSA9PT0gJ2ZhZGUnKSB7XG4gICAgICAgIGRpc3RhbmNlID0gTWF0aC5hYnMoc2xpZGVyLnRvdWNoLnN0YXJ0LnggLSBzbGlkZXIudG91Y2guZW5kLngpO1xuICAgICAgICBpZiAoZGlzdGFuY2UgPj0gc2xpZGVyLnNldHRpbmdzLnN3aXBlVGhyZXNob2xkKSB7XG4gICAgICAgICAgaWYgKHNsaWRlci50b3VjaC5zdGFydC54ID4gc2xpZGVyLnRvdWNoLmVuZC54KSB7XG4gICAgICAgICAgICBlbC5nb1RvTmV4dFNsaWRlKCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVsLmdvVG9QcmV2U2xpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWwuc3RvcEF1dG8oKTtcbiAgICAgICAgfVxuICAgICAgLy8gbm90IGZhZGUgbW9kZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gY2FsY3VsYXRlIGRpc3RhbmNlIGFuZCBlbCdzIGFuaW1hdGUgcHJvcGVydHlcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcpIHtcbiAgICAgICAgICBkaXN0YW5jZSA9IHNsaWRlci50b3VjaC5lbmQueCAtIHNsaWRlci50b3VjaC5zdGFydC54O1xuICAgICAgICAgIHZhbHVlID0gc2xpZGVyLnRvdWNoLm9yaWdpbmFsUG9zLmxlZnQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZGlzdGFuY2UgPSBzbGlkZXIudG91Y2guZW5kLnkgLSBzbGlkZXIudG91Y2guc3RhcnQueTtcbiAgICAgICAgICB2YWx1ZSA9IHNsaWRlci50b3VjaC5vcmlnaW5hbFBvcy50b3A7XG4gICAgICAgIH1cbiAgICAgICAgLy8gaWYgbm90IGluZmluaXRlIGxvb3AgYW5kIGZpcnN0IC8gbGFzdCBzbGlkZSwgZG8gbm90IGF0dGVtcHQgYSBzbGlkZSB0cmFuc2l0aW9uXG4gICAgICAgIGlmICghc2xpZGVyLnNldHRpbmdzLmluZmluaXRlTG9vcCAmJiAoKHNsaWRlci5hY3RpdmUuaW5kZXggPT09IDAgJiYgZGlzdGFuY2UgPiAwKSB8fCAoc2xpZGVyLmFjdGl2ZS5sYXN0ICYmIGRpc3RhbmNlIDwgMCkpKSB7XG4gICAgICAgICAgc2V0UG9zaXRpb25Qcm9wZXJ0eSh2YWx1ZSwgJ3Jlc2V0JywgMjAwKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBjaGVjayBpZiBkaXN0YW5jZSBjbGVhcnMgdGhyZXNob2xkXG4gICAgICAgICAgaWYgKE1hdGguYWJzKGRpc3RhbmNlKSA+PSBzbGlkZXIuc2V0dGluZ3Muc3dpcGVUaHJlc2hvbGQpIHtcbiAgICAgICAgICAgIGlmIChkaXN0YW5jZSA8IDApIHtcbiAgICAgICAgICAgICAgZWwuZ29Ub05leHRTbGlkZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgZWwuZ29Ub1ByZXZTbGlkZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWwuc3RvcEF1dG8oKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gZWwuYW5pbWF0ZShwcm9wZXJ0eSwgMjAwKTtcbiAgICAgICAgICAgIHNldFBvc2l0aW9uUHJvcGVydHkodmFsdWUsICdyZXNldCcsIDIwMCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBzbGlkZXIudmlld3BvcnQudW5iaW5kKCd0b3VjaGVuZCBNU1BvaW50ZXJVcCBwb2ludGVydXAnLCBvblRvdWNoRW5kKTtcbiAgICAgIGlmIChzbGlkZXIudmlld3BvcnQuZ2V0KDApLnJlbGVhc2VQb2ludGVyQ2FwdHVyZSkge1xuICAgICAgICBzbGlkZXIudmlld3BvcnQuZ2V0KDApLnJlbGVhc2VQb2ludGVyQ2FwdHVyZShzbGlkZXIucG9pbnRlcklkKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogV2luZG93IHJlc2l6ZSBldmVudCBjYWxsYmFja1xuICAgICAqL1xuICAgIHZhciByZXNpemVXaW5kb3cgPSBmdW5jdGlvbihlKSB7XG4gICAgICAvLyBkb24ndCBkbyBhbnl0aGluZyBpZiBzbGlkZXIgaXNuJ3QgaW5pdGlhbGl6ZWQuXG4gICAgICBpZiAoIXNsaWRlci5pbml0aWFsaXplZCkgeyByZXR1cm47IH1cbiAgICAgIC8vIERlbGF5IGlmIHNsaWRlciB3b3JraW5nLlxuICAgICAgaWYgKHNsaWRlci53b3JraW5nKSB7XG4gICAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KHJlc2l6ZVdpbmRvdywgMTApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gZ2V0IHRoZSBuZXcgd2luZG93IGRpbWVucyAoYWdhaW4sIHRoYW5rIHlvdSBJRSlcbiAgICAgICAgdmFyIHdpbmRvd1dpZHRoTmV3ID0gJCh3aW5kb3cpLndpZHRoKCksXG4gICAgICAgIHdpbmRvd0hlaWdodE5ldyA9ICQod2luZG93KS5oZWlnaHQoKTtcbiAgICAgICAgLy8gbWFrZSBzdXJlIHRoYXQgaXQgaXMgYSB0cnVlIHdpbmRvdyByZXNpemVcbiAgICAgICAgLy8gKndlIG11c3QgY2hlY2sgdGhpcyBiZWNhdXNlIG91ciBkaW5vc2F1ciBmcmllbmQgSUUgZmlyZXMgYSB3aW5kb3cgcmVzaXplIGV2ZW50IHdoZW4gY2VydGFpbiBET00gZWxlbWVudHNcbiAgICAgICAgLy8gYXJlIHJlc2l6ZWQuIENhbiB5b3UganVzdCBkaWUgYWxyZWFkeT8qXG4gICAgICAgIGlmICh3aW5kb3dXaWR0aCAhPT0gd2luZG93V2lkdGhOZXcgfHwgd2luZG93SGVpZ2h0ICE9PSB3aW5kb3dIZWlnaHROZXcpIHtcbiAgICAgICAgICAvLyBzZXQgdGhlIG5ldyB3aW5kb3cgZGltZW5zXG4gICAgICAgICAgd2luZG93V2lkdGggPSB3aW5kb3dXaWR0aE5ldztcbiAgICAgICAgICB3aW5kb3dIZWlnaHQgPSB3aW5kb3dIZWlnaHROZXc7XG4gICAgICAgICAgLy8gdXBkYXRlIGFsbCBkeW5hbWljIGVsZW1lbnRzXG4gICAgICAgICAgZWwucmVkcmF3U2xpZGVyKCk7XG4gICAgICAgICAgLy8gQ2FsbCB1c2VyIHJlc2l6ZSBoYW5kbGVyXG4gICAgICAgICAgc2xpZGVyLnNldHRpbmdzLm9uU2xpZGVyUmVzaXplLmNhbGwoZWwsIHNsaWRlci5hY3RpdmUuaW5kZXgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEFkZHMgYW4gYXJpYS1oaWRkZW49dHJ1ZSBhdHRyaWJ1dGUgdG8gZWFjaCBlbGVtZW50XG4gICAgICpcbiAgICAgKiBAcGFyYW0gc3RhcnRWaXNpYmxlSW5kZXggKGludClcbiAgICAgKiAgLSB0aGUgZmlyc3QgdmlzaWJsZSBlbGVtZW50J3MgaW5kZXhcbiAgICAgKi9cbiAgICB2YXIgYXBwbHlBcmlhSGlkZGVuQXR0cmlidXRlcyA9IGZ1bmN0aW9uKHN0YXJ0VmlzaWJsZUluZGV4KSB7XG4gICAgICB2YXIgbnVtYmVyT2ZTbGlkZXNTaG93aW5nID0gZ2V0TnVtYmVyU2xpZGVzU2hvd2luZygpO1xuICAgICAgLy8gb25seSBhcHBseSBhdHRyaWJ1dGVzIGlmIHRoZSBzZXR0aW5nIGlzIGVuYWJsZWQgYW5kIG5vdCBpbiB0aWNrZXIgbW9kZVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hcmlhSGlkZGVuICYmICFzbGlkZXIuc2V0dGluZ3MudGlja2VyKSB7XG4gICAgICAgIC8vIGFkZCBhcmlhLWhpZGRlbj10cnVlIHRvIGFsbCBlbGVtZW50c1xuICAgICAgICBzbGlkZXIuY2hpbGRyZW4uYXR0cignYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xuICAgICAgICAvLyBnZXQgdGhlIHZpc2libGUgZWxlbWVudHMgYW5kIGNoYW5nZSB0byBhcmlhLWhpZGRlbj1mYWxzZVxuICAgICAgICBzbGlkZXIuY2hpbGRyZW4uc2xpY2Uoc3RhcnRWaXNpYmxlSW5kZXgsIHN0YXJ0VmlzaWJsZUluZGV4ICsgbnVtYmVyT2ZTbGlkZXNTaG93aW5nKS5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGluZGV4IGFjY29yZGluZyB0byBwcmVzZW50IHBhZ2UgcmFuZ2VcbiAgICAgKlxuICAgICAqIEBwYXJhbSBzbGlkZU9uZGV4IChpbnQpXG4gICAgICogIC0gdGhlIGRlc2lyZWQgc2xpZGUgaW5kZXhcbiAgICAgKi9cbiAgICB2YXIgc2V0U2xpZGVJbmRleCA9IGZ1bmN0aW9uKHNsaWRlSW5kZXgpIHtcbiAgICAgIGlmIChzbGlkZUluZGV4IDwgMCkge1xuICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLmluZmluaXRlTG9vcCkge1xuICAgICAgICAgIHJldHVybiBnZXRQYWdlclF0eSgpIC0gMTtcbiAgICAgICAgfWVsc2Uge1xuICAgICAgICAgIC8vd2UgZG9uJ3QgZ28gdG8gdW5kZWZpbmVkIHNsaWRlc1xuICAgICAgICAgIHJldHVybiBzbGlkZXIuYWN0aXZlLmluZGV4O1xuICAgICAgICB9XG4gICAgICAvLyBpZiBzbGlkZUluZGV4IGlzIGdyZWF0ZXIgdGhhbiBjaGlsZHJlbiBsZW5ndGgsIHNldCBhY3RpdmUgaW5kZXggdG8gMCAodGhpcyBoYXBwZW5zIGR1cmluZyBpbmZpbml0ZSBsb29wKVxuICAgICAgfSBlbHNlIGlmIChzbGlkZUluZGV4ID49IGdldFBhZ2VyUXR5KCkpIHtcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5pbmZpbml0ZUxvb3ApIHtcbiAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvL3dlIGRvbid0IG1vdmUgdG8gdW5kZWZpbmVkIHBhZ2VzXG4gICAgICAgICAgcmV0dXJuIHNsaWRlci5hY3RpdmUuaW5kZXg7XG4gICAgICAgIH1cbiAgICAgIC8vIHNldCBhY3RpdmUgaW5kZXggdG8gcmVxdWVzdGVkIHNsaWRlXG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gc2xpZGVJbmRleDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAgKiA9IFBVQkxJQyBGVU5DVElPTlNcbiAgICAgKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogUGVyZm9ybXMgc2xpZGUgdHJhbnNpdGlvbiB0byB0aGUgc3BlY2lmaWVkIHNsaWRlXG4gICAgICpcbiAgICAgKiBAcGFyYW0gc2xpZGVJbmRleCAoaW50KVxuICAgICAqICAtIHRoZSBkZXN0aW5hdGlvbiBzbGlkZSdzIGluZGV4ICh6ZXJvLWJhc2VkKVxuICAgICAqXG4gICAgICogQHBhcmFtIGRpcmVjdGlvbiAoc3RyaW5nKVxuICAgICAqICAtIElOVEVSTkFMIFVTRSBPTkxZIC0gdGhlIGRpcmVjdGlvbiBvZiB0cmF2ZWwgKFwicHJldlwiIC8gXCJuZXh0XCIpXG4gICAgICovXG4gICAgZWwuZ29Ub1NsaWRlID0gZnVuY3Rpb24oc2xpZGVJbmRleCwgZGlyZWN0aW9uKSB7XG4gICAgICAvLyBvblNsaWRlQmVmb3JlLCBvblNsaWRlTmV4dCwgb25TbGlkZVByZXYgY2FsbGJhY2tzXG4gICAgICAvLyBBbGxvdyB0cmFuc2l0aW9uIGNhbmNlbGluZyBiYXNlZCBvbiByZXR1cm5lZCB2YWx1ZVxuICAgICAgdmFyIHBlcmZvcm1UcmFuc2l0aW9uID0gdHJ1ZSxcbiAgICAgIG1vdmVCeSA9IDAsXG4gICAgICBwb3NpdGlvbiA9IHtsZWZ0OiAwLCB0b3A6IDB9LFxuICAgICAgbGFzdENoaWxkID0gbnVsbCxcbiAgICAgIGxhc3RTaG93aW5nSW5kZXgsIGVxLCB2YWx1ZSwgcmVxdWVzdEVsO1xuICAgICAgLy8gc3RvcmUgdGhlIG9sZCBpbmRleFxuICAgICAgc2xpZGVyLm9sZEluZGV4ID0gc2xpZGVyLmFjdGl2ZS5pbmRleDtcbiAgICAgIC8vc2V0IG5ldyBpbmRleFxuICAgICAgc2xpZGVyLmFjdGl2ZS5pbmRleCA9IHNldFNsaWRlSW5kZXgoc2xpZGVJbmRleCk7XG5cbiAgICAgIC8vIGlmIHBsdWdpbiBpcyBjdXJyZW50bHkgaW4gbW90aW9uLCBpZ25vcmUgcmVxdWVzdFxuICAgICAgaWYgKHNsaWRlci53b3JraW5nIHx8IHNsaWRlci5hY3RpdmUuaW5kZXggPT09IHNsaWRlci5vbGRJbmRleCkgeyByZXR1cm47IH1cbiAgICAgIC8vIGRlY2xhcmUgdGhhdCBwbHVnaW4gaXMgaW4gbW90aW9uXG4gICAgICBzbGlkZXIud29ya2luZyA9IHRydWU7XG5cbiAgICAgIHBlcmZvcm1UcmFuc2l0aW9uID0gc2xpZGVyLnNldHRpbmdzLm9uU2xpZGVCZWZvcmUuY2FsbChlbCwgc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXgpLCBzbGlkZXIub2xkSW5kZXgsIHNsaWRlci5hY3RpdmUuaW5kZXgpO1xuXG4gICAgICAvLyBJZiB0cmFuc2l0aW9ucyBjYW5jZWxlZCwgcmVzZXQgYW5kIHJldHVyblxuICAgICAgaWYgKHR5cGVvZiAocGVyZm9ybVRyYW5zaXRpb24pICE9PSAndW5kZWZpbmVkJyAmJiAhcGVyZm9ybVRyYW5zaXRpb24pIHtcbiAgICAgICAgc2xpZGVyLmFjdGl2ZS5pbmRleCA9IHNsaWRlci5vbGRJbmRleDsgLy8gcmVzdG9yZSBvbGQgaW5kZXhcbiAgICAgICAgc2xpZGVyLndvcmtpbmcgPSBmYWxzZTsgLy8gaXMgbm90IGluIG1vdGlvblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChkaXJlY3Rpb24gPT09ICduZXh0Jykge1xuICAgICAgICAvLyBQcmV2ZW50IGNhbmNlbGluZyBpbiBmdXR1cmUgZnVuY3Rpb25zIG9yIGxhY2sgdGhlcmUtb2YgZnJvbSBuZWdhdGluZyBwcmV2aW91cyBjb21tYW5kcyB0byBjYW5jZWxcbiAgICAgICAgaWYgKCFzbGlkZXIuc2V0dGluZ3Mub25TbGlkZU5leHQuY2FsbChlbCwgc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXgpLCBzbGlkZXIub2xkSW5kZXgsIHNsaWRlci5hY3RpdmUuaW5kZXgpKSB7XG4gICAgICAgICAgcGVyZm9ybVRyYW5zaXRpb24gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChkaXJlY3Rpb24gPT09ICdwcmV2Jykge1xuICAgICAgICAvLyBQcmV2ZW50IGNhbmNlbGluZyBpbiBmdXR1cmUgZnVuY3Rpb25zIG9yIGxhY2sgdGhlcmUtb2YgZnJvbSBuZWdhdGluZyBwcmV2aW91cyBjb21tYW5kcyB0byBjYW5jZWxcbiAgICAgICAgaWYgKCFzbGlkZXIuc2V0dGluZ3Mub25TbGlkZVByZXYuY2FsbChlbCwgc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXgpLCBzbGlkZXIub2xkSW5kZXgsIHNsaWRlci5hY3RpdmUuaW5kZXgpKSB7XG4gICAgICAgICAgcGVyZm9ybVRyYW5zaXRpb24gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBjaGVjayBpZiBsYXN0IHNsaWRlXG4gICAgICBzbGlkZXIuYWN0aXZlLmxhc3QgPSBzbGlkZXIuYWN0aXZlLmluZGV4ID49IGdldFBhZ2VyUXR5KCkgLSAxO1xuICAgICAgLy8gdXBkYXRlIHRoZSBwYWdlciB3aXRoIGFjdGl2ZSBjbGFzc1xuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5wYWdlciB8fCBzbGlkZXIuc2V0dGluZ3MucGFnZXJDdXN0b20pIHsgdXBkYXRlUGFnZXJBY3RpdmUoc2xpZGVyLmFjdGl2ZS5pbmRleCk7IH1cbiAgICAgIC8vIC8vIGNoZWNrIGZvciBkaXJlY3Rpb24gY29udHJvbCB1cGRhdGVcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuY29udHJvbHMpIHsgdXBkYXRlRGlyZWN0aW9uQ29udHJvbHMoKTsgfVxuICAgICAgLy8gaWYgc2xpZGVyIGlzIHNldCB0byBtb2RlOiBcImZhZGVcIlxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnZmFkZScpIHtcbiAgICAgICAgLy8gaWYgYWRhcHRpdmVIZWlnaHQgaXMgdHJ1ZSBhbmQgbmV4dCBoZWlnaHQgaXMgZGlmZmVyZW50IGZyb20gY3VycmVudCBoZWlnaHQsIGFuaW1hdGUgdG8gdGhlIG5ldyBoZWlnaHRcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hZGFwdGl2ZUhlaWdodCAmJiBzbGlkZXIudmlld3BvcnQuaGVpZ2h0KCkgIT09IGdldFZpZXdwb3J0SGVpZ2h0KCkpIHtcbiAgICAgICAgICBzbGlkZXIudmlld3BvcnQuYW5pbWF0ZSh7aGVpZ2h0OiBnZXRWaWV3cG9ydEhlaWdodCgpfSwgc2xpZGVyLnNldHRpbmdzLmFkYXB0aXZlSGVpZ2h0U3BlZWQpO1xuICAgICAgICB9XG4gICAgICAgIC8vIGZhZGUgb3V0IHRoZSB2aXNpYmxlIGNoaWxkIGFuZCByZXNldCBpdHMgei1pbmRleCB2YWx1ZVxuICAgICAgICBzbGlkZXIuY2hpbGRyZW4uZmlsdGVyKCc6dmlzaWJsZScpLmZhZGVPdXQoc2xpZGVyLnNldHRpbmdzLnNwZWVkKS5jc3Moe3pJbmRleDogMH0pO1xuICAgICAgICAvLyBmYWRlIGluIHRoZSBuZXdseSByZXF1ZXN0ZWQgc2xpZGVcbiAgICAgICAgc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXgpLmNzcygnekluZGV4Jywgc2xpZGVyLnNldHRpbmdzLnNsaWRlWkluZGV4ICsgMSkuZmFkZUluKHNsaWRlci5zZXR0aW5ncy5zcGVlZCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJCh0aGlzKS5jc3MoJ3pJbmRleCcsIHNsaWRlci5zZXR0aW5ncy5zbGlkZVpJbmRleCk7XG4gICAgICAgICAgdXBkYXRlQWZ0ZXJTbGlkZVRyYW5zaXRpb24oKTtcbiAgICAgICAgfSk7XG4gICAgICAvLyBzbGlkZXIgbW9kZSBpcyBub3QgXCJmYWRlXCJcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIGlmIGFkYXB0aXZlSGVpZ2h0IGlzIHRydWUgYW5kIG5leHQgaGVpZ2h0IGlzIGRpZmZlcmVudCBmcm9tIGN1cnJlbnQgaGVpZ2h0LCBhbmltYXRlIHRvIHRoZSBuZXcgaGVpZ2h0XG4gICAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYWRhcHRpdmVIZWlnaHQgJiYgc2xpZGVyLnZpZXdwb3J0LmhlaWdodCgpICE9PSBnZXRWaWV3cG9ydEhlaWdodCgpKSB7XG4gICAgICAgICAgc2xpZGVyLnZpZXdwb3J0LmFuaW1hdGUoe2hlaWdodDogZ2V0Vmlld3BvcnRIZWlnaHQoKX0sIHNsaWRlci5zZXR0aW5ncy5hZGFwdGl2ZUhlaWdodFNwZWVkKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBpZiBjYXJvdXNlbCBhbmQgbm90IGluZmluaXRlIGxvb3BcbiAgICAgICAgaWYgKCFzbGlkZXIuc2V0dGluZ3MuaW5maW5pdGVMb29wICYmIHNsaWRlci5jYXJvdXNlbCAmJiBzbGlkZXIuYWN0aXZlLmxhc3QpIHtcbiAgICAgICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLm1vZGUgPT09ICdob3Jpem9udGFsJykge1xuICAgICAgICAgICAgLy8gZ2V0IHRoZSBsYXN0IGNoaWxkIHBvc2l0aW9uXG4gICAgICAgICAgICBsYXN0Q2hpbGQgPSBzbGlkZXIuY2hpbGRyZW4uZXEoc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAtIDEpO1xuICAgICAgICAgICAgcG9zaXRpb24gPSBsYXN0Q2hpbGQucG9zaXRpb24oKTtcbiAgICAgICAgICAgIC8vIGNhbGN1bGF0ZSB0aGUgcG9zaXRpb24gb2YgdGhlIGxhc3Qgc2xpZGVcbiAgICAgICAgICAgIG1vdmVCeSA9IHNsaWRlci52aWV3cG9ydC53aWR0aCgpIC0gbGFzdENoaWxkLm91dGVyV2lkdGgoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gZ2V0IGxhc3Qgc2hvd2luZyBpbmRleCBwb3NpdGlvblxuICAgICAgICAgICAgbGFzdFNob3dpbmdJbmRleCA9IHNsaWRlci5jaGlsZHJlbi5sZW5ndGggLSBzbGlkZXIuc2V0dGluZ3MubWluU2xpZGVzO1xuICAgICAgICAgICAgcG9zaXRpb24gPSBzbGlkZXIuY2hpbGRyZW4uZXEobGFzdFNob3dpbmdJbmRleCkucG9zaXRpb24oKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gaG9yaXpvbnRhbCBjYXJvdXNlbCwgZ29pbmcgcHJldmlvdXMgd2hpbGUgb24gZmlyc3Qgc2xpZGUgKGluZmluaXRlTG9vcCBtb2RlKVxuICAgICAgICB9IGVsc2UgaWYgKHNsaWRlci5jYXJvdXNlbCAmJiBzbGlkZXIuYWN0aXZlLmxhc3QgJiYgZGlyZWN0aW9uID09PSAncHJldicpIHtcbiAgICAgICAgICAvLyBnZXQgdGhlIGxhc3QgY2hpbGQgcG9zaXRpb25cbiAgICAgICAgICBlcSA9IHNsaWRlci5zZXR0aW5ncy5tb3ZlU2xpZGVzID09PSAxID8gc2xpZGVyLnNldHRpbmdzLm1heFNsaWRlcyAtIGdldE1vdmVCeSgpIDogKChnZXRQYWdlclF0eSgpIC0gMSkgKiBnZXRNb3ZlQnkoKSkgLSAoc2xpZGVyLmNoaWxkcmVuLmxlbmd0aCAtIHNsaWRlci5zZXR0aW5ncy5tYXhTbGlkZXMpO1xuICAgICAgICAgIGxhc3RDaGlsZCA9IGVsLmNoaWxkcmVuKCcuYngtY2xvbmUnKS5lcShlcSk7XG4gICAgICAgICAgcG9zaXRpb24gPSBsYXN0Q2hpbGQucG9zaXRpb24oKTtcbiAgICAgICAgLy8gaWYgaW5maW5pdGUgbG9vcCBhbmQgXCJOZXh0XCIgaXMgY2xpY2tlZCBvbiB0aGUgbGFzdCBzbGlkZVxuICAgICAgICB9IGVsc2UgaWYgKGRpcmVjdGlvbiA9PT0gJ25leHQnICYmIHNsaWRlci5hY3RpdmUuaW5kZXggPT09IDApIHtcbiAgICAgICAgICAvLyBnZXQgdGhlIGxhc3QgY2xvbmUgcG9zaXRpb25cbiAgICAgICAgICBwb3NpdGlvbiA9IGVsLmZpbmQoJz4gLmJ4LWNsb25lJykuZXEoc2xpZGVyLnNldHRpbmdzLm1heFNsaWRlcykucG9zaXRpb24oKTtcbiAgICAgICAgICBzbGlkZXIuYWN0aXZlLmxhc3QgPSBmYWxzZTtcbiAgICAgICAgLy8gbm9ybWFsIG5vbi16ZXJvIHJlcXVlc3RzXG4gICAgICAgIH0gZWxzZSBpZiAoc2xpZGVJbmRleCA+PSAwKSB7XG4gICAgICAgICAgLy9wYXJzZUludCBpcyBhcHBsaWVkIHRvIGFsbG93IGZsb2F0cyBmb3Igc2xpZGVzL3BhZ2VcbiAgICAgICAgICByZXF1ZXN0RWwgPSBzbGlkZUluZGV4ICogcGFyc2VJbnQoZ2V0TW92ZUJ5KCkpO1xuICAgICAgICAgIHBvc2l0aW9uID0gc2xpZGVyLmNoaWxkcmVuLmVxKHJlcXVlc3RFbCkucG9zaXRpb24oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qIElmIHRoZSBwb3NpdGlvbiBkb2Vzbid0IGV4aXN0XG4gICAgICAgICAqIChlLmcuIGlmIHlvdSBkZXN0cm95IHRoZSBzbGlkZXIgb24gYSBuZXh0IGNsaWNrKSxcbiAgICAgICAgICogaXQgZG9lc24ndCB0aHJvdyBhbiBlcnJvci5cbiAgICAgICAgICovXG4gICAgICAgIGlmICh0eXBlb2YgKHBvc2l0aW9uKSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICB2YWx1ZSA9IHNsaWRlci5zZXR0aW5ncy5tb2RlID09PSAnaG9yaXpvbnRhbCcgPyAtKHBvc2l0aW9uLmxlZnQgLSBtb3ZlQnkpIDogLXBvc2l0aW9uLnRvcDtcbiAgICAgICAgICAvLyBwbHVnaW4gdmFsdWVzIHRvIGJlIGFuaW1hdGVkXG4gICAgICAgICAgc2V0UG9zaXRpb25Qcm9wZXJ0eSh2YWx1ZSwgJ3NsaWRlJywgc2xpZGVyLnNldHRpbmdzLnNwZWVkKTtcbiAgICAgICAgfVxuICAgICAgICBzbGlkZXIud29ya2luZyA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hcmlhSGlkZGVuKSB7IGFwcGx5QXJpYUhpZGRlbkF0dHJpYnV0ZXMoc2xpZGVyLmFjdGl2ZS5pbmRleCAqIGdldE1vdmVCeSgpKTsgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBUcmFuc2l0aW9ucyB0byB0aGUgbmV4dCBzbGlkZSBpbiB0aGUgc2hvd1xuICAgICAqL1xuICAgIGVsLmdvVG9OZXh0U2xpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIC8vIGlmIGluZmluaXRlTG9vcCBpcyBmYWxzZSBhbmQgbGFzdCBwYWdlIGlzIHNob3dpbmcsIGRpc3JlZ2FyZCBjYWxsXG4gICAgICBpZiAoIXNsaWRlci5zZXR0aW5ncy5pbmZpbml0ZUxvb3AgJiYgc2xpZGVyLmFjdGl2ZS5sYXN0KSB7IHJldHVybjsgfVxuXHQgIGlmIChzbGlkZXIud29ya2luZyA9PSB0cnVlKXsgcmV0dXJuIDt9XG4gICAgICB2YXIgcGFnZXJJbmRleCA9IHBhcnNlSW50KHNsaWRlci5hY3RpdmUuaW5kZXgpICsgMTtcbiAgICAgIGVsLmdvVG9TbGlkZShwYWdlckluZGV4LCAnbmV4dCcpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBUcmFuc2l0aW9ucyB0byB0aGUgcHJldiBzbGlkZSBpbiB0aGUgc2hvd1xuICAgICAqL1xuICAgIGVsLmdvVG9QcmV2U2xpZGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIC8vIGlmIGluZmluaXRlTG9vcCBpcyBmYWxzZSBhbmQgbGFzdCBwYWdlIGlzIHNob3dpbmcsIGRpc3JlZ2FyZCBjYWxsXG4gICAgICBpZiAoIXNsaWRlci5zZXR0aW5ncy5pbmZpbml0ZUxvb3AgJiYgc2xpZGVyLmFjdGl2ZS5pbmRleCA9PT0gMCkgeyByZXR1cm47IH1cblx0ICBpZiAoc2xpZGVyLndvcmtpbmcgPT0gdHJ1ZSl7IHJldHVybiA7fVxuICAgICAgdmFyIHBhZ2VySW5kZXggPSBwYXJzZUludChzbGlkZXIuYWN0aXZlLmluZGV4KSAtIDE7XG4gICAgICBlbC5nb1RvU2xpZGUocGFnZXJJbmRleCwgJ3ByZXYnKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogU3RhcnRzIHRoZSBhdXRvIHNob3dcbiAgICAgKlxuICAgICAqIEBwYXJhbSBwcmV2ZW50Q29udHJvbFVwZGF0ZSAoYm9vbGVhbilcbiAgICAgKiAgLSBpZiB0cnVlLCBhdXRvIGNvbnRyb2xzIHN0YXRlIHdpbGwgbm90IGJlIHVwZGF0ZWRcbiAgICAgKi9cbiAgICBlbC5zdGFydEF1dG8gPSBmdW5jdGlvbihwcmV2ZW50Q29udHJvbFVwZGF0ZSkge1xuICAgICAgLy8gaWYgYW4gaW50ZXJ2YWwgYWxyZWFkeSBleGlzdHMsIGRpc3JlZ2FyZCBjYWxsXG4gICAgICBpZiAoc2xpZGVyLmludGVydmFsKSB7IHJldHVybjsgfVxuICAgICAgLy8gY3JlYXRlIGFuIGludGVydmFsXG4gICAgICBzbGlkZXIuaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hdXRvRGlyZWN0aW9uID09PSAnbmV4dCcpIHtcbiAgICAgICAgICBlbC5nb1RvTmV4dFNsaWRlKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZWwuZ29Ub1ByZXZTbGlkZSgpO1xuICAgICAgICB9XG4gICAgICB9LCBzbGlkZXIuc2V0dGluZ3MucGF1c2UpO1xuXHQgIC8vYWxsYmFjayBmb3Igd2hlbiB0aGUgYXV0byByb3RhdGUgc3RhdHVzIGNoYW5nZXNcblx0ICBzbGlkZXIuc2V0dGluZ3Mub25BdXRvQ2hhbmdlLmNhbGwoZWwsIHRydWUpO1xuICAgICAgLy8gaWYgYXV0byBjb250cm9scyBhcmUgZGlzcGxheWVkIGFuZCBwcmV2ZW50Q29udHJvbFVwZGF0ZSBpcyBub3QgdHJ1ZVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hdXRvQ29udHJvbHMgJiYgcHJldmVudENvbnRyb2xVcGRhdGUgIT09IHRydWUpIHsgdXBkYXRlQXV0b0NvbnRyb2xzKCdzdG9wJyk7IH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogU3RvcHMgdGhlIGF1dG8gc2hvd1xuICAgICAqXG4gICAgICogQHBhcmFtIHByZXZlbnRDb250cm9sVXBkYXRlIChib29sZWFuKVxuICAgICAqICAtIGlmIHRydWUsIGF1dG8gY29udHJvbHMgc3RhdGUgd2lsbCBub3QgYmUgdXBkYXRlZFxuICAgICAqL1xuICAgIGVsLnN0b3BBdXRvID0gZnVuY3Rpb24ocHJldmVudENvbnRyb2xVcGRhdGUpIHtcbiAgICAgIC8vIGlmIG5vIGludGVydmFsIGV4aXN0cywgZGlzcmVnYXJkIGNhbGxcbiAgICAgIGlmICghc2xpZGVyLmludGVydmFsKSB7IHJldHVybjsgfVxuICAgICAgLy8gY2xlYXIgdGhlIGludGVydmFsXG4gICAgICBjbGVhckludGVydmFsKHNsaWRlci5pbnRlcnZhbCk7XG4gICAgICBzbGlkZXIuaW50ZXJ2YWwgPSBudWxsO1xuXHQgIC8vYWxsYmFjayBmb3Igd2hlbiB0aGUgYXV0byByb3RhdGUgc3RhdHVzIGNoYW5nZXNcblx0ICBzbGlkZXIuc2V0dGluZ3Mub25BdXRvQ2hhbmdlLmNhbGwoZWwsIGZhbHNlKTtcbiAgICAgIC8vIGlmIGF1dG8gY29udHJvbHMgYXJlIGRpc3BsYXllZCBhbmQgcHJldmVudENvbnRyb2xVcGRhdGUgaXMgbm90IHRydWVcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MuYXV0b0NvbnRyb2xzICYmIHByZXZlbnRDb250cm9sVXBkYXRlICE9PSB0cnVlKSB7IHVwZGF0ZUF1dG9Db250cm9scygnc3RhcnQnKTsgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGN1cnJlbnQgc2xpZGUgaW5kZXggKHplcm8tYmFzZWQpXG4gICAgICovXG4gICAgZWwuZ2V0Q3VycmVudFNsaWRlID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gc2xpZGVyLmFjdGl2ZS5pbmRleDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyBjdXJyZW50IHNsaWRlIGVsZW1lbnRcbiAgICAgKi9cbiAgICBlbC5nZXRDdXJyZW50U2xpZGVFbGVtZW50ID0gZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gc2xpZGVyLmNoaWxkcmVuLmVxKHNsaWRlci5hY3RpdmUuaW5kZXgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgc2xpZGUgZWxlbWVudFxuICAgICAqIEBwYXJhbSBpbmRleCAoaW50KVxuICAgICAqICAtIFRoZSBpbmRleCAoemVyby1iYXNlZCkgb2YgdGhlIGVsZW1lbnQgeW91IHdhbnQgcmV0dXJuZWQuXG4gICAgICovXG4gICAgZWwuZ2V0U2xpZGVFbGVtZW50ID0gZnVuY3Rpb24oaW5kZXgpIHtcbiAgICAgIHJldHVybiBzbGlkZXIuY2hpbGRyZW4uZXEoaW5kZXgpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIG51bWJlciBvZiBzbGlkZXMgaW4gc2hvd1xuICAgICAqL1xuICAgIGVsLmdldFNsaWRlQ291bnQgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBzbGlkZXIuY2hpbGRyZW4ubGVuZ3RoO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gc2xpZGVyLndvcmtpbmcgdmFyaWFibGVcbiAgICAgKi9cbiAgICBlbC5pc1dvcmtpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiBzbGlkZXIud29ya2luZztcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVXBkYXRlIGFsbCBkeW5hbWljIHNsaWRlciBlbGVtZW50c1xuICAgICAqL1xuICAgIGVsLnJlZHJhd1NsaWRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gcmVzaXplIGFsbCBjaGlsZHJlbiBpbiByYXRpbyB0byBuZXcgc2NyZWVuIHNpemVcbiAgICAgIHNsaWRlci5jaGlsZHJlbi5hZGQoZWwuZmluZCgnLmJ4LWNsb25lJykpLm91dGVyV2lkdGgoZ2V0U2xpZGVXaWR0aCgpKTtcbiAgICAgIC8vIGFkanVzdCB0aGUgaGVpZ2h0XG4gICAgICBzbGlkZXIudmlld3BvcnQuY3NzKCdoZWlnaHQnLCBnZXRWaWV3cG9ydEhlaWdodCgpKTtcbiAgICAgIC8vIHVwZGF0ZSB0aGUgc2xpZGUgcG9zaXRpb25cbiAgICAgIGlmICghc2xpZGVyLnNldHRpbmdzLnRpY2tlcikgeyBzZXRTbGlkZVBvc2l0aW9uKCk7IH1cbiAgICAgIC8vIGlmIGFjdGl2ZS5sYXN0IHdhcyB0cnVlIGJlZm9yZSB0aGUgc2NyZWVuIHJlc2l6ZSwgd2Ugd2FudFxuICAgICAgLy8gdG8ga2VlcCBpdCBsYXN0IG5vIG1hdHRlciB3aGF0IHNjcmVlbiBzaXplIHdlIGVuZCBvblxuICAgICAgaWYgKHNsaWRlci5hY3RpdmUubGFzdCkgeyBzbGlkZXIuYWN0aXZlLmluZGV4ID0gZ2V0UGFnZXJRdHkoKSAtIDE7IH1cbiAgICAgIC8vIGlmIHRoZSBhY3RpdmUgaW5kZXggKHBhZ2UpIG5vIGxvbmdlciBleGlzdHMgZHVlIHRvIHRoZSByZXNpemUsIHNpbXBseSBzZXQgdGhlIGluZGV4IGFzIGxhc3RcbiAgICAgIGlmIChzbGlkZXIuYWN0aXZlLmluZGV4ID49IGdldFBhZ2VyUXR5KCkpIHsgc2xpZGVyLmFjdGl2ZS5sYXN0ID0gdHJ1ZTsgfVxuICAgICAgLy8gaWYgYSBwYWdlciBpcyBiZWluZyBkaXNwbGF5ZWQgYW5kIGEgY3VzdG9tIHBhZ2VyIGlzIG5vdCBiZWluZyB1c2VkLCB1cGRhdGUgaXRcbiAgICAgIGlmIChzbGlkZXIuc2V0dGluZ3MucGFnZXIgJiYgIXNsaWRlci5zZXR0aW5ncy5wYWdlckN1c3RvbSkge1xuICAgICAgICBwb3B1bGF0ZVBhZ2VyKCk7XG4gICAgICAgIHVwZGF0ZVBhZ2VyQWN0aXZlKHNsaWRlci5hY3RpdmUuaW5kZXgpO1xuICAgICAgfVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5hcmlhSGlkZGVuKSB7IGFwcGx5QXJpYUhpZGRlbkF0dHJpYnV0ZXMoc2xpZGVyLmFjdGl2ZS5pbmRleCAqIGdldE1vdmVCeSgpKTsgfVxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBEZXN0cm95IHRoZSBjdXJyZW50IGluc3RhbmNlIG9mIHRoZSBzbGlkZXIgKHJldmVydCBldmVyeXRoaW5nIGJhY2sgdG8gb3JpZ2luYWwgc3RhdGUpXG4gICAgICovXG4gICAgZWwuZGVzdHJveVNsaWRlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gZG9uJ3QgZG8gYW55dGhpbmcgaWYgc2xpZGVyIGhhcyBhbHJlYWR5IGJlZW4gZGVzdHJveWVkXG4gICAgICBpZiAoIXNsaWRlci5pbml0aWFsaXplZCkgeyByZXR1cm47IH1cbiAgICAgIHNsaWRlci5pbml0aWFsaXplZCA9IGZhbHNlO1xuICAgICAgJCgnLmJ4LWNsb25lJywgdGhpcykucmVtb3ZlKCk7XG4gICAgICBzbGlkZXIuY2hpbGRyZW4uZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKCQodGhpcykuZGF0YSgnb3JpZ1N0eWxlJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICQodGhpcykuYXR0cignc3R5bGUnLCAkKHRoaXMpLmRhdGEoJ29yaWdTdHlsZScpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAkKHRoaXMpLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgaWYgKCQodGhpcykuZGF0YSgnb3JpZ1N0eWxlJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aGlzLmF0dHIoJ3N0eWxlJywgJCh0aGlzKS5kYXRhKCdvcmlnU3R5bGUnKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKHRoaXMpLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICB9XG4gICAgICAkKHRoaXMpLnVud3JhcCgpLnVud3JhcCgpO1xuICAgICAgaWYgKHNsaWRlci5jb250cm9scy5lbCkgeyBzbGlkZXIuY29udHJvbHMuZWwucmVtb3ZlKCk7IH1cbiAgICAgIGlmIChzbGlkZXIuY29udHJvbHMubmV4dCkgeyBzbGlkZXIuY29udHJvbHMubmV4dC5yZW1vdmUoKTsgfVxuICAgICAgaWYgKHNsaWRlci5jb250cm9scy5wcmV2KSB7IHNsaWRlci5jb250cm9scy5wcmV2LnJlbW92ZSgpOyB9XG4gICAgICBpZiAoc2xpZGVyLnBhZ2VyRWwgJiYgc2xpZGVyLnNldHRpbmdzLmNvbnRyb2xzICYmICFzbGlkZXIuc2V0dGluZ3MucGFnZXJDdXN0b20pIHsgc2xpZGVyLnBhZ2VyRWwucmVtb3ZlKCk7IH1cbiAgICAgICQoJy5ieC1jYXB0aW9uJywgdGhpcykucmVtb3ZlKCk7XG4gICAgICBpZiAoc2xpZGVyLmNvbnRyb2xzLmF1dG9FbCkgeyBzbGlkZXIuY29udHJvbHMuYXV0b0VsLnJlbW92ZSgpOyB9XG4gICAgICBjbGVhckludGVydmFsKHNsaWRlci5pbnRlcnZhbCk7XG4gICAgICBpZiAoc2xpZGVyLnNldHRpbmdzLnJlc3BvbnNpdmUpIHsgJCh3aW5kb3cpLnVuYmluZCgncmVzaXplJywgcmVzaXplV2luZG93KTsgfVxuICAgICAgaWYgKHNsaWRlci5zZXR0aW5ncy5rZXlib2FyZEVuYWJsZWQpIHsgJChkb2N1bWVudCkudW5iaW5kKCdrZXlkb3duJywga2V5UHJlc3MpOyB9XG4gICAgICAvL3JlbW92ZSBzZWxmIHJlZmVyZW5jZSBpbiBkYXRhXG4gICAgICAkKHRoaXMpLnJlbW92ZURhdGEoJ2J4U2xpZGVyJyk7XG5cdCAgLy8gcmVtb3ZlIGdsb2JhbCB3aW5kb3cgaGFuZGxlcnNcblx0ICAkKHdpbmRvdykub2ZmKCdibHVyJywgd2luZG93Qmx1ckhhbmRsZXIpLm9mZignZm9jdXMnLCB3aW5kb3dGb2N1c0hhbmRsZXIpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBSZWxvYWQgdGhlIHNsaWRlciAocmV2ZXJ0IGFsbCBET00gY2hhbmdlcywgYW5kIHJlLWluaXRpYWxpemUpXG4gICAgICovXG4gICAgZWwucmVsb2FkU2xpZGVyID0gZnVuY3Rpb24oc2V0dGluZ3MpIHtcbiAgICAgIGlmIChzZXR0aW5ncyAhPT0gdW5kZWZpbmVkKSB7IG9wdGlvbnMgPSBzZXR0aW5nczsgfVxuICAgICAgZWwuZGVzdHJveVNsaWRlcigpO1xuICAgICAgaW5pdCgpO1xuICAgICAgLy9zdG9yZSByZWZlcmVuY2UgdG8gc2VsZiBpbiBvcmRlciB0byBhY2Nlc3MgcHVibGljIGZ1bmN0aW9ucyBsYXRlclxuICAgICAgJChlbCkuZGF0YSgnYnhTbGlkZXInLCB0aGlzKTtcbiAgICB9O1xuXG4gICAgaW5pdCgpO1xuXG4gICAgJChlbCkuZGF0YSgnYnhTbGlkZXInLCB0aGlzKTtcblxuICAgIC8vIHJldHVybnMgdGhlIGN1cnJlbnQgalF1ZXJ5IG9iamVjdFxuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG59KShqUXVlcnkpO1xuIl0sImZpbGUiOiJqcXVlcnkuYnhzbGlkZXIuanMifQ==
